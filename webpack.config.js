const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
	mode: "production",
	entry: ["./src/index.js"],
	output: {
		filename: "sgGlobal.js",
		path: path.resolve(__dirname, "dist"),
		clean: true,
		library: {
			name: "sgGlobal",
			type: "var",
		},
	},
	devServer: {
		static: path.join(__dirname, "public"),
		hot: true,
		compress: true,
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "SG Template Script",
			filename: "index.html",
			template: "./src/index.html",
			libraryPath: `sgGlobal`,
			inject: false,
		}),
		new MiniCssExtractPlugin({
			filename: "sgGlobal.css",
		}),
	],
	performance: {
        // hints: false,
        maxAssetSize: 1250000,
        maxEntrypointSize: 1250000
	},
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: "babel-loader",
					options: {
						presets: ["@babel/preset-env"],
					},
				},
			},
			{
				test: /\.css$/i,
				use: [MiniCssExtractPlugin.loader, "css-loader"],
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: "css-loader",
					},
					"sass-loader",
				],
			},
			{
				test: /\.(png|svg|jpg|jpeg|gif)$/i,
				type: "asset/resource",
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/i,
				type: "asset/resource",
			},
		],
	},
	optimization: {
		// runtimeChunk: 'single',
		minimize: false
	},
};
