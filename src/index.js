import './style.scss'
import * as AgendaCarousel from './Javascripts/agendaCarousel'
import * as AgendaList from './Javascripts/agendaList'
import * as ShowHideSwitch from './Javascripts/showHideSwitch'
import * as CustomAgenda from './Javascripts/customAgenda'
import { testPing, getScript, forwardParams } from './Javascripts/global'
import { generateCssVars } from './Javascripts/themeGen'
import * as SocialWall from './Javascripts/socialWall'
import * as Header from './Javascripts/header';
import * as ContentChapters from './Javascripts/contentChapters';
import * as EventDetails from './Javascripts/eventDetails';
import * as PresenterBioModal from './Javascripts/presenterBioModal';
import * as Banner from './Javascripts/banner';
import * as UW from './Javascripts/unifiedWidget';
import * as Questions from './Javascripts/questions';
import * as EventAlert from './Javascripts/EventAlerts';
import * as Sentiments from './Javascripts/sentiments';
import * as TH from './Javascripts/treasureHunt';
import * as PosterFilters from './Javascripts/posterFilter';
import * as SponsorFilters from './Javascripts/sponsorFilters';
import * as AgendaListFilters from './Javascripts/agendaListFilters';
import * as SponsorsCarousel from './Javascripts/sponsorsCarousel';
import * as Select2Init from './Javascripts/select2Init';
import { streamGoCountdown } from './Javascripts/countdown';

// get env
const isStaging = window.location.hostname.split('.')[0] === 'staging';

// functions run before window loaded
const externalScripts = [
  {
    url: 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/agenda-web-push/index.js',
    id: 'agenda-web-push-script',
  },
  {
    url: isStaging ? 
      'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/discover-go-widget/discover-go-widget-dev.js'
      :'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/discover-go-widget/discover-go-widget.js',
    id: 'discover-go-script',
  },
  {
    url: `https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/sentiments/ratingAnimation.js`,
    id: 'rating-animation-script',
  },
  {
    url: `https://cdn-aws.s3-eu-west-1.amazonaws.com/cdn-sg-productions/scripts/showHide20seconds.js`,
    id: 'show-hide-40-script',
  },
]

externalScripts.forEach(item => getScript(item.url, item.id));

generateCssVars()

forwardParams()

// functions to run on page loaded
window.addEventListener('load', () => {
  ShowHideSwitch.init()
  AgendaCarousel.init()
  SponsorsCarousel.init()
  AgendaList.init()
  CustomAgenda.init(typeof caSettings !== 'undefined' && caSettings)
  SocialWall.init()
  Header.init()
  ContentChapters.init()
  EventDetails.init()
  PresenterBioModal.init()
  Banner.init()
  UW.init(typeof uwSettings !== 'undefined' && uwSettings)
  Questions.init()
  EventAlert.init()
  Sentiments.initAnimations()
  PosterFilters.init();
  SponsorFilters.init();
  AgendaListFilters.init();
  Select2Init.init();
});

window.th_init = (settings) => TH.th_init(settings);

// export fragments of functions from other components
export { AgendaCarousel, AgendaList, testPing, ShowHideSwitch, streamGoCountdown }
