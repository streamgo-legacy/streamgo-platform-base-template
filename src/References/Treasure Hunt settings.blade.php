<script>

    var uwSettings = {
        customAgenda: {
            enable: true
        },
        discoverGo: {
            enable: true
        },
        treasureHunt: {
            enable: true,
            // if you want to set up treasure too, otherwise, loots: null
            loots: [
                {
                    id: 'loot-1',
                    path: 'lobby',
                    desc: 'Lobby Treasure',
                    src: 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/chest.png',
                    name: 'Lobby Loot',
                    html: `<p>You've found Loot 1!</p>`,
                    videoGo: 0
                },
                {
                    id: 'loot-2',
                    path: 'speakers',
                    desc: 'Speakers Treasure',
                    src: 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/chest.png',
                    name: 'Speakers Loot',
                    html: `<p>You've found Loot 2!</p>`,
                    videoGo: 0
                },
                {
                    id: 'loot-3',
                    path: 'sponsors',
                    desc: 'Sponsors Treasure',
                    src: 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/chest.png',
                    name: 'Sponsors Loot',
                    html: `<p>You've found Loot 3!</p>`,
                    videoGo: 0
                },
            ]
        },
        chatGo: {
            enable: {{ is_array($project->chatgo_event) ? 'true' : 'false' }}
        },
        cpd: {
            enable: {{ !empty($project->cpdSetting) && $project->cpdSetting->enabled ? 'true' : 'false' }}
        },
        leaderboard: {
            enable: true
        },
        techSupport: {
            enable: true
        }
    }
</script>
