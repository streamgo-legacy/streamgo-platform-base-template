<!-- Unified Widget init -->
<script type="text/javascript"
    src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/unified-widget/unified-widget-dev.js">
</script>

<script>
    let driftUrl = `https://streamgo.drift.click/3321c23b-e920-4c7e-bac6-2db57215e323`;
    @if (request()->is($project->url . '/login'))
        @if ($project->passcode_required === 'no')
            driftUrl = `https://streamgo.drift.click/8b642cc0-8df3-496e-88b4-1fd628121927`;
        @else
            driftUrl = `https://streamgo.drift.click/c52d36ea-087b-414a-94a1-97f122f8d46d`;
        @endif
    @elseif (request()->is($project->url . '/register'))
        driftUrl = `https://streamgo.drift.click/ebbcf6d4-f709-4f36-9545-510e17fdea20`;
    @elseif (request()->is($project->url . '/thanks'))
        driftUrl = `https://streamgo.drift.click/cb094519-7ffc-4b57-81cc-5af6d78df7ad`;
    @else
        @if (isset($event))
            @if ($event->event_type === 'round-table')
                driftUrl = `https://streamgo.drift.click/c2e0edfa-63f8-43d2-b41e-08deecf1fd99`;
            @elseif ($event->od_id)
                driftUrl = `https://streamgo.drift.click/657ccdac-1b82-41ac-b8b9-9b1be2aac7fa`;
            @else
                driftUrl = `https://streamgo.drift.click/3321c23b-e920-4c7e-bac6-2db57215e323`;
            @endif
        @else
            driftUrl = `https://streamgo.drift.click/3321c23b-e920-4c7e-bac6-2db57215e323`;
        @endif
    @endif

    var uwSettings = {
        widgetSettings: {
            enable: true,
            position: 'right',
            tabIconColor: '#fff',
            tabColor: '#bbb',
            tabColorHover: '{{ $branding->accent_1 }}'
        },
        customAgenda: {
            enable: false,
        },
        treasureHunt: {
            enable: false,
        },
        chatGo: {
            enable: false,
        },
        techSupport: {
            enable: true,
            url: driftUrl
        },
        discoverGo: {
            enable: false,
        }
    };

    if (typeof uwSettings != 'undefined') {
        uw_init(uwSettings);
    }
</script>
<!-- End of Unified Widget init -->