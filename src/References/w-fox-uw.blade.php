
<div class="unified-widget" id="unified-widget" style='{!! $project->accent_1 !!}'>
    <div class="live">
      <span class="live-now">
        <svg style="width: 50px; height: 50px" viewBox="0 0 24 24">
          <path fill="currentColor"
            d="M12,10A2,2 0 0,0 10,12C10,13.11 10.9,14 12,14C13.11,14 14,13.11 14,12A2,2 0 0,0 12,10Z" />
        </svg>
      </span>
      <div class="live-text">
        <h2 id="live-now__heading"></h2>
        <p id='session-name'></p>
      </div>
    </div>
    <!-- code here for leader board and cpd in future -->
    <!-- this code wil need injecting into the js, push to active content on drop down -->
    <div class='leaderboard-blade-code' style='display: none;'>
      <!-- this did not work, see email from KEV -->
      <!-- {!! !empty($project->leaderboard_exclusions) !!} -->
      <ol>
        @foreach ($project->leaderboard(10) as $user)
          <li class='leaderboard-li'>
            <div class='leaderboard-li-content'>
              <p>{!! $user->name !!} {!! $user->surname !!}</p><span>{!! $user->score !!} points</span>
            </div>
          </li>
        @endforeach
      </ol>
    </div>
  </div>
  <!-- END unified widget -->
  
  
  
  {{-- --------------------------------------------------- --}}
  
  
  
  <script>
    const agendaEvents = [];
    @foreach ($project->agenda()->get() as $agendaEvent)
      agendaEvents.push({
        id: '{!! $agendaEvent->id !!}',
        name: '{!! $agendaEvent->name !!}',
        type: '{!! $agendaEvent->event_type !!}',
        url: '{!! $agendaEvent->full_url !!}',
        start_time: '{!! $agendaEvent->start_time->valueOf() !!}',
        end_time: '{!! $agendaEvent->end_time ? $agendaEvent->end_time->valueOf() : '4089260030000' !!}'
      });
    @endforeach
  
  
  
    const sessionName = document.getElementById('session-name')
    const liveNow = document.querySelector('.live-now')
    const liveNowHeading = document.getElementById('live-now__heading')
    const sessionStart = document.getElementById('session-start')
    const sessionEnd = document.getElementById('session-end')
  
    const currentEventTime = () => {
      const currentTime = Date.now()
      let stopChecking = false;
  
      if (!stopChecking) {
        liveNow.style = 'visibility: hidden;'
      }
  
      const sorted = [...agendaEvents].sort((a, b) => {
        if (parseInt(a.start_time) > parseInt(b.start_time)) {
          return 1
        }
        if (parseInt(a.start_time) < parseInt(b.start_time)) {
          return -1
        }
        return 0
      });
  
      // trying to find if more than one have same start time
      // needs thinking through as currently its showing goood for one event
  
  
      // using new sorted agendaEvents to print to screen updated info
      sorted.map((event) => {
        // console.log(event.start_time)
        if (stopChecking) {
          return
        };
  
  
        // find the live event
        // this is live currently
        if (currentTime > parseInt(event.start_time) && currentTime < parseInt(event.end_time)) {
          liveNow.style.visibility = 'visible'
          liveNowHeading.textContent = 'Live Now'
          // create link
          const link = document.createElement('a')
          link.setAttribute('href', event.url)
          link.textContent = event.name
          link.style.color = 'white'
          sessionName.appendChild(link)
  
  
          stopChecking = true;
        }
        // this is looking to see what will be live soon
        if (currentTime < parseInt(event.start_time)) {
          liveNow.style.visibility = 'hidden'
          liveNowHeading.textContent = 'Coming Soon'
          sessionName.textContent = event.name
          // create link and add url that is correct for event ( event.url )
  
  
          stopChecking = true;
        }
      }) // end map
    }
    currentEventTime()
    setInterval(() => {
      // check to see if there is data there, if there is then i wont keep adding name of event
      // CHECK THIS - this is currently not updating live for dom with new thing if changed since last time
      // CHECK THIS - maybe be nessasarry to kick onload and then have the settimeout do the rest
      if (sessionName === '') {
        currentEventTime()
      }
    }, 60000) // this is every minute
  
    // click on leaderboard icon
    function leaderboard() {
      const leaderboardIcon = document.querySelector('.leaderboard')
      const bodyCheck = document.querySelector('.uw_widget_open')
  
      leaderboardIcon.addEventListener('click', () => {
        const leaderboardDiv = document.querySelector('.uw_widget_leaderboard')
        const leaderboardBladeCode = document.querySelector('.leaderboard-blade-code')
        leaderboardBladeCode.style = 'display: block; color: black !important; padding: 5px; margin-top: 30px;'
        leaderboardDiv.appendChild(leaderboardBladeCode)
      })
    }
  
  
  
    setTimeout(() => {
        leaderboard()
        const menuBG = document.querySelector('div .uw_widget_content_container')
        const widgetOpen = document.querySelector('.uw_widget_content_container')
        const tabs = document.querySelectorAll('.uw_widget_tab')
        tabs.forEach(tab => {
          tab.addEventListener('click', () => {
            if (widgetOpen.classList.contains('uw_widget_open')) {
              menuBG.style = 'display: flex !important';
              console.log(menuBG, 'WAYNE')
            } else {
              return console.log('WAYNE you r in the else case here mate')
            }
  
          })
  
        })
  
  
      },
      1000);
  </script>
  
  {{-- if this div.uw_widget_content_container includes uw_widget_open, then when close btn it clicked, toggle display: none --}}
  
  
  {{-- event_available
  search
  star_outline
  chat
  support_agent
  leaderboard --}}