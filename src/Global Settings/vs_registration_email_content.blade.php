<table id="backgroundTable" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td style="padding: 0; margin: 0;" align="center" valign="top" bgcolor="#f9f9f9"><!-- /// Start Outer Container /// -->
            <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 700px;" border="0" width="700" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="padding: 0; margin: 0;" align="center" valign="top"><!-- /// Start Inner Container /// -->
                        <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 650px;" border="0" width="650" cellspacing="0" cellpadding="0"><!-- /// Android Gmail Zoom Fix Start /// -->
                            <tbody>
                            <tr class="vanish">
                                <td style="font-size: 1px; line-height: 0; min-width: 650px; width: 650px; height: 1px; max-height: 1px; mso-margin-top-alt: 1px; border: 0; margin: 0; padding: 0; display: block;" height="1">&nbsp;</td>
                            </tr>
                            <!-- /// Divider Start /// -->
                            <tr>
                                <td style="padding: 0;" bgcolor="#ffffff">
                                    <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 650px;" role="presentation" border="0" width="650" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="border-top: 6px solid {{ $branding->accent_1 }};">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- /// Divider End /// --> <!-- /// Android Gmail Zoom Fix End /// --> <!-- /// Logo Start /// -->
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 650px;" role="presentation" border="0" width="650" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 0px 15px;">
                                                <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 620px;" role="presentation" border="0" width="620" cellspacing="0" cellpadding="0" align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td style="padding: 35px 20px; border-bottom: 1px solid #e8e8e8;" align="center"><img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" src="{{ storage_url($branding->logo) }}" alt="{{ $project->name }} logo" width="300" border="0" /></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- /// Logo End /// --> <!-- /// Intro Start /// -->
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 650px;" role="presentation" border="0" width="650" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; mso-line-height-rule: exactly; line-height: 18px; color: #686868; padding: 50px 20px;" align="center">Hey {{ $user->name }},</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial, sans-serif; font-size: 40px; font-weight: normal; mso-line-height-rule: exactly; line-height: 42px; color: #333333; padding: 0px 20px 50px;" align="center">You&rsquo;ve just signed up to {{ $project->name }}!</td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; mso-line-height-rule: exactly; line-height: 42px; color: #333333; padding: 0px 20px 50px;" align="center">Watch it all here:</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- /// Intro End /// --><!-- /// Registration Content Start /// -->
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 650px;" role="presentation" border="0" width="650" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 0px 20px;">
                                                <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 540px;" role="presentation" border="0" width="540" cellspacing="0" cellpadding="0" align="center">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 250px;" role="presentation" border="0" width="250" cellspacing="0" cellpadding="0" align="center">
                                                                <tbody>
                                                                <tr>
                                                                    <td class="ctaPad" style="padding-bottom: 20px;">
                                                                        <div><!-- [if mso]>
	<v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{ $user->getPermMagicLink() }}" style="height:50px;v-text-anchor:middle;width:250px;" stroke="f" fillcolor="{{ $branding->accent_1 }}">
	<w:anchorlock/>
	<center>
    <![endif]--> <a href="{{ $user->getPermMagicLink() }}" class="fluid" style="background-color: {{ $branding->accent_1 }}; color: #ffffff; display: inline-block; font-family: Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 50px; text-align: center; text-decoration: none; width: 250px; -webkit-text-size-adjust: none;">Watch Here</a> <!-- [if mso]>
                                                                            </center>
                                                                            </v:rect>
                                                                            <![endif]--></div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- /// Registration Content End /// --> <!-- /// Footer Start /// -->
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table class="fluid" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 650px;" role="presentation" border="0" width="650" cellspacing="0" cellpadding="0" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; mso-line-height-rule: exactly; line-height: 18px; color: #686868; padding: 50px 20px 20px;" align="center">Thanks,</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 40px 20px; border-top: 1px solid #e8e8e8;" align="center"><img style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" src="https://videoscout.ai/wp-content/uploads/2021/10/VideoScout-Logo-Final.png" alt="VideoScout" width="150" border="0" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- /// Footer End /// --></tbody>
                        </table>
                        <!-- /// End Inner Container /// --></td>
                </tr>
                </tbody>
            </table>
            <!-- /// End Outer Container /// --></td>
    </tr>
    </tbody>
</table>