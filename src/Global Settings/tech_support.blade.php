<!-- This script determines if the user is a client or not. If a client (determined by a Block called CLIENTLIST), they will get a Tawk widget to be able to speak to streamGo Production, otherwise they will get the Tech Support Drift widget -->

@if(!empty($user->email) && stripos($project->getBlock('CLIENTLIST'), $user->email) !== false)

    <!-- Start of Tawk -->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src=`https://embed.tawk.to/5f9aa67916ea1756a6de919c/default`;
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <script>
        Tawk_API = Tawk_API || {};
        Tawk_API.visitor = {
            name  : '{{ $user->full_name }}',
            email : '{{ $user->email }}'
        };

    </script>
    <!-- End of Tawk -->

@else

    <!-- Drift V2 -->
    <script src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/drift-code/production/driftHelper.min.js" ></script>

    @if(!empty($project->getBlock('driftShowOnLoad')))
        {!! $project->getBlock('driftShowOnLoad') !!}
    @endif

    <script>
        window.addEventListener('load', () => {
            driftHelper.init({
                backgroundColor: `{{ $branding->accent_1 }}`,
                eventType: '{{ isset($event) ? $event->event_type() : "Unknown" }}',
                isLoggedIn: '{{ $project->isLoggedIn() ? $user->email : "false" }}',
                projectId: '{{ session("project_session_".$project->id)["id"] }}',
                showOnLoad: typeof driftShowOnLoad === 'boolean'
                    ? driftShowOnLoad
                    : {{ determine_base_template($project->template) === 1 ? "false" : "true" }},
                baseTemplateId: {{ determine_base_template($project->template) }}
            });
        });
    </script>
    <!-- end of Drift V2 -->

@endif