@isset($alert)
    <div class="ws-alert alert">
        <a class="close">Close</a>
        <p class="content">
            {!! $alert->payload !!}
        </p>
    </div>
@endisset