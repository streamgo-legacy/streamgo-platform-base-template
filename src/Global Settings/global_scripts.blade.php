<!-- Treasure hunt init from platform -->
@if ($project->hasTreasureHunt())
    <script>
        th_init({
            enable: true,
            huntId: 'sg_treasurehunt_{{ $project->id }}',
            blankLootSrc: '{{ storage_url($project->treasure_setting->blank_icon) }}',
            completeModalHtml: `{!! $project->treasure_setting->completion_text !!}`,
            loots: {!! $project->treasures()->with('event')->get()->map(fn($treasure) =>
                        [
                            'id'        => 'loot-'.$treasure->id,
                            'path'      => $treasure->event->url,
                            'src'       => storage_url($treasure->icon ?? $project->treasure_setting->icon),
                            'videoGo'   => $treasure->video_pos ?? 0,
                            'position'  => $treasure->treasure_pos,
                        ]
                ) !!},
			hideAllLoots: false,
			passportFloat: false,
			progressPassportId: "th-passport",
			progressPassportClass: "th-passport",
			completeModalClass: "th-complete",
        });
    </script>
@endif
<!-- end of treasure hunt init -->

<!--- IE Warning Start -->
@if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false))
<div id="overlayIE" onclick="this.style.display = 'none';">
    <div id="textIE"><img src="https://cdn4.streamgo.co.uk/fonts/alert.png" alt="" width="256" height="256" /><br><strong>You're using an outdated browser.</strong><br><br>
        We strongly recommend you use another browser to view this event - we recommend Google Chrome.<br><br>
        If you continue to use this browser some features and elements of the event may not function correctly.
    </div>
</div>
@endif
<!--- IE Warning End -->

<!--- Global Scripts Start -->

<!-- select2 JS and its CSS for dynamically generating select UI components -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
<!-- end of select2 JS -->

<script src="{{ vapor_asset('/js/main.js') }}"></script>
<script src="{{ vapor_asset('/js/app.js') }}"></script>

<script type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
<!--- Global Scripts End -->

<!-- PIP init codes -->
@if($project->isLoggedIn() && $project->project_type_id == 2)
<script src='https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/spa-dev/picture-in-picture.js'></script>
@endif
<!-- end of PIP init codes -->

<!-- No Russia Code -->
@if(empty($project->getBlock('ALLOWRUSSIA')))
<Script src='https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/notRus/notRus.js'></script>
@endif
<!-- No Russia Code End -->

<!-- Base Template 1.5 codes -->
<script id="unified-widget-events-items-script">
    var uwEvents = [];
    @foreach (request()->has('tags') ? $project->agenda()->with('tags')->withTagString(request()->get('tags'))->get() : $project->agenda()->with('tags')->get() as $uwAgendaEvent)
    uwEvents.push({
        id: '{{ $uwAgendaEvent->id }}',
        name: `{{ $uwAgendaEvent->name }}`,
        type: '{!! $uwAgendaEvent->event_type !!}',
        url: '{!! $uwAgendaEvent->full_url !!}',
        tags: {!! $uwAgendaEvent->tags->map(fn($tag) => Str::slug($tag->name) ) !!},
        start_time: {!! $uwAgendaEvent->start_time->valueOf() !!},
        end_time: {!! $uwAgendaEvent->end_time ? $uwAgendaEvent->end_time->valueOf() : '4089260030000' !!}
    });
    @endforeach
</script>

<script id="drift-playbook-id-script">
    var playbookIds = {
        authTypes: {
            'users.login' : {
                'passcoded' : 255213,
                'noPasscode' : 110438
            },
            'users.register' : 98771,
            'users.thanks' : 108953
        },
        eventTypes: {
            'on-demand': 161787,
            'round-table': 257409
        },
        default : 98329
    };

    @if(request()->routeIs('users.login'))
    var interactionId = playbookIds.authTypes["users.login"]["{{ $project->passcode_required === 'no' ? 'noPasscode' : 'passcoded' }}"];
    @elseif(request()->routeIs('users.register', 'users.thanks'))
    var interactionId = playbookIds.authTypes["{{ request()->route()->getName() }}"];
    @elseif(isset($event) && in_array($event->event_type, ['on-demand', 'round-table']))
    var interactionId = playbookIds.authTypes["{{ $event->event_type }}"];
    @else
    var interactionId = playbookIds.default;
    @endif

    console.log(`interactionId:`, interactionId);

</script>
<!-- End of Base Template 1.5 codes -->