@if(!$event->ticket_types_enabled || optional(auth()->user())->can('ask-questions', $event))

    @if ($event->hasQuestions() && $event->hasDynamicQuestions())
        <style>
            .hide {
                display: none;
            }

            #dynamic-questions-wrapper{
                display: flex;
                flex-direction: column;
                background: #eee;
                box-sizing: border-box;
                padding: 12px !important;
                max-height: 70vh;
                margin: 2rem 0rem;
                border-radius: 16px;
                box-shadow: inset 0px 0px 8px #ddd;
                font-size: 14px;
                overflow: auto;
                border: 1px solid #eee;
            }
            .dynamic-questions-wrapper button{
                border-radius: 99px;
            }
            .dynamic-questions-postbox{
                box-shadow: 0px 12px 24px rgba(0,0,0,0.07);
                background: #fff;
                border-radius: 8px;
                box-sizing: border-box;
                padding: 12px;
                transition: 500ms;
            }
            .dynamic-questions-postbox .form__group textarea{
                min-height: 0px;
                transition: 500ms;
                border: none;
                resize: none;
                height: 22px;
                margin: 0px;
                padding: 0px 12px;
            }
            .dynamic-questions-postbox .form__group button{
                display: none;
            }
            .dynamic-questions-postbox.opened .form__group textarea{
                height: 80px;
            }
            .dynamic-questions-postbox.opened .form__group button{
                display: block;
            }
            .dynamic-questions-list{
                /* margin-top: 24px; */
            }
            .dynamic-question{
                display: flex;
                flex-direction: column;
                background: #fff;
                box-sizing: border-box;
                padding: 12px;
                border-radius: 8px;
                box-shadow: 0px 6px 12px rgba(0,0,0,0.07);
                margin-bottom: 12px;
            }
            .dynamic-question .question-info,
            .dynamic-question-reply .question-info {
                display: flex;
                flex-direction: row;
            }
            .dynamic-question .question-avatar,
            .dynamic-question-reply .question-avatar{

                width: 36px;
                height: 36px;
                background: #ddd;
                border-radius: 99px;
                font-size: 18px;
                color: #fff;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .dynamic-question .question-info-text,
            .dynamic-question-reply .question-info-text{
                display: flex;
                flex-direction: column;
                align-items: start;
                justify-content: space-evenly;
                margin-left: 12px;
            }
            .question-name{
                font-weight: bold;
            }
            .question-time{
                font-size: 12px;
                color: #888;
                padding-right: .5rem;
            }
            .question-report {
                font-size: 12px;
                color: #888;
                cursor: pointer;
                text-decoration: underline;
            }
            .question-meta {
                display: flex;
                align-items: center;
            }
            .dynamic-question .question-info .vote-form,
            .dynamic-question-reply .question-info .vote-form{
                margin-left: auto;
            }
            .dynamic-question .vote-button, .dynamic-questions-postbox .form__group button,
            .dynamic-question-reply .vote-button, .dynamic-questions-postbox .form__group button{

                padding: 4px 16px;
                font-size: 14px;
            }
            .dynamic-question .vote-button > *,
            .dynamic-question-reply .vote-button > *{
                pointer-events: none;
            }
            .dynamic-question .vote-button:disabled, .dynamic-question .vote-button:disabled:hover,
            .dynamic-question-reply .vote-button:disabled, .dynamic-question .vote-button:disabled:hover{

                padding: 2px 16px;
                font-size: 14px;
                color: #135 !important;
                box-sizing: border-box;
                border: 1px solid #fff;
                box-shadow: 0px 0px 0px 2px #d0d0d0;
            }
            .dynamic-question .question-title{
                margin-top: 12px;
                font-size: 14px;
                padding: 4px;
            }
            .toggle-question-order, .toggle-question-order:focus{
                display: inline-block;
                width: auto;
                margin: 36px auto 12px 16px;
                font-size: 14px;
                padding: 6px 24px;
            }
            .dynamic-questions-postbox .form__group{
                margin: 0px;
            }
            .dynamic-questions-postbox .alert{
                font-size: 14px;
                border-radius: 8px;
                padding: 12px !important;
                margin: 12px;
                box-sizing: border-box;
                width: auto;
            }
            .dynamic-questions-postbox .word-count{
                text-align: right;
                margin: 12px;
                font-size: 11px;
                color: #999;
                display: none;
            }
            .dynamic-questions-postbox.opened .word-count{
                display: block;
            }
            .dynamic-questions-actions{
                display: flex;
                flex-direction: row;
                justify-content: start;
                align-items: baseline;
            }
            .dynamic-questions-actions.hide{
                display: none;
            }
            .dynamic-questions-count{
                color: #666;
                margin-right: 12px;
            }
            .dynamic-questions-list .noq{
                padding: 48px;
                display: block;
                font-size: 16px;
                text-align: center;
            }

            .report-modal-overlay {
                background: rgba(0,0,0,0.25);
                width: 100vw;
                height: 100vh;
                position: fixed;
                top: 0;
                left: 0;
                display: none;
                align-items: center;
                justify-content: center;
                z-index: 50;
            }
            .report-modal-overlay--open {
                display: flex;
            }
            .report-modal {
                background: white;
                border-radius: 10px;
                max-width: 300px;
                width: 100%;
                box-shadow: 0px 7px 10px rgba(0, 0, 0, .07);
            }
            .report-modal__header {
                padding: 15px 15px 5px 15px;
            }
            .report-modal__body {
                padding: 5px 15px 15px 15px;
            }
            .report-modal__body button {
                margin-top: 10px;
            }

            .reply-toggle-container {
                display: flex;
                align-items: center;
                margin-top: 15px;
            }

            .reply-toggle-container i {
                transform: rotate(180deg);
            }

            .reply-toggle {
                margin-left: 10px;
                cursor: pointer;
            }

            .dynamic-question-reply-container {
                border-top: 1px solid #eee;
                padding-top: 15px;
                margin-top: 15px;
            }

            .dynamic-question-replies {
                padding: 15px 0;
            }

            .dynamic-question-reply {
                padding-left: 20px;
                margin-top: 15px;
                margin-bottom: 15px;
            }

        </style>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <section id="dynamic-questions-wrapper" class="dynamic-questions-wrapper content-block spacing spacing--bottom-only">

            <div class="report-modal-overlay">
                <div class="report-modal">
                    <div class="report-modal__header">
                        <h3>Do you want to report this <span id="reportType">question</span>?</h3>
                    </div>
                    <div class="report-modal__body">
                        <div class="report-modal__form">
                            <label for="report_reason">
                                Report reason:
                            </label>
                            <textarea name="report_reason" id="report_reason" placeholder="Maximum 255 characters" max="255"></textarea>
                            <button class="my-2" id="submitReport" type="submit">Report</button>
                        </div>
                        <div class="report-modal__message hide"></div>
                    </div>
                </div>
            </div>

            @if (auth()->check() && ! auth()->user()->isBanned())
                <div class="dynamic-questions-postbox">
                    <!-- <h2 class="section-title">Ask a question</h2> -->
                    <form action="{{ $event->full_url.'/questions' }}" method="post" id="question-form" class="form form--ask clearfix">

                        <div class="question-success alert alert-success" style="display: none;" role="alert">Question successfully sent.</div>
                        <div class="question-error alert alert-error" style="display: none;" role="alert">There was a problem submitting your question, please try again.</div>

                        @csrf
                        <div class="form__group">
                            <textarea rows="1" name="question" id="question" form="question-form" required placeholder="Type your question"></textarea>
                            <button class="button button--submit float-right" >Send</button>
                        </div>
                    </form>
                </div>
            @endif

            <div class="dynamic-questions-actions{{ $dynamic_questions->count() > 0 ? '': ' hide' }}">
                <button type="button" class="toggle-question-order" data-sort-by="{{ $event->dynamic_question_sort === 'newest' ? 'votes' : 'newest' }}">
                    {{ $event->dynamic_question_sort === 'newest' ? 'Sort by Votes' : 'Sort by Newest' }}
                </button>
                <div class="dynamic-questions-count">{{ $dynamic_questions->count() }} question{{ $dynamic_questions->count() > 1 ? 's':'' }}</div>
            </div>

            <div class="dynamic-questions-list" data-event-id="{{ $event->id }}">

                @if ($event->allow_question_replies)
                    <template class="question-reply-template">
                        <div class="dynamic-question-reply not-loaded">
                            <div class="question-info">
                                <div class="question-avatar" data-name=""></div>
                                <div class="question-info-text">
                                    <div class="question-name"></div>
                                    <div class="question-time" data-time=""></div>
                                </div>
                            </div>
                            <span class="question-title"></span>
                            <br><span data-question-id="" class="question-report" data-report-url="">Report this reply</span>
                        </div>
                    </template>
                @endif

                @if($dynamic_questions->count())
                    @foreach($dynamic_questions as $question)
                        @if (! $question->hasUserReported(auth()->user()))
                            <div class="dynamic-question" data-question-id="{{ $question->id }}"{{ auth()->check() && $question->hasUserVoted(auth()->user()) ? " data-question-voted=true" : '' }} data-votes="{{ $question->votes_count }}" data-created="{{ $question->created_at->timestamp }}">
                                <div class="question-info">
                                    <div class="question-avatar" data-name="{{ optional($question->user)->name ?? $question->name ?? 'Unknown' }}">{{ optional($question->user)->name ? substr($question->user->name, 0,1) : ($question->name ? substr($question->name, 0,1) : 'U') }}</div>
                                    <div class="question-info-text">
                                        <div class="question-name">{{ $question->user->name ?? $question->name ?? 'Unknown' }}</div>
                                        <div class="question-meta">
                                            <div class="question-time" data-time="{{ $question->created_at->valueOf() }}"></div>
                                            @if(auth()->check() && optional($question->user)->id !== auth('web')->user()->id)
                                                <span data-question-id="{{ $question->id }}" class="question-report" data-report-url="{{ route('questions.vote.report', [$project->url, $event->url, $question]) }}">
                                Report this question
                            </span>
                                            @endif
                                        </div>

                                    </div>
                                    @if(!$event->ticket_types_enabled || optional(auth()->user())->can('vote-on-questions', $event))
                                        @if (auth()->check() && ! $question->hasUserVoted(auth()->user()))
                                            <form class="vote-form" method="post" data-question-id="{{ $question->id }}" action="{{ route('questions.vote.store', [$project->url, $event->url, $question]) }}">
                                                @csrf
                                                <button class="vote-button" data-question-id="{{ $question->id }}" type="button"><span class="vote-count">{{ $question->votes_count }}</span>&nbsp;<i class="fa fa-thumbs-up"></i></button>
                                            </form>
                                        @else
                                            <div class="vote-form">
                                                <button disabled class="vote-button" data-question-id="{{ $question->id }}" type="button"><span class="vote-count">{{ $question->votes_count }}</span>&nbsp;<i class="fa fa-thumbs-up"></i></button>
                                            </div>
                                        @endif
                                    @endif
                                </div>
                                <span class="question-title">{{ $question->question }}</span>
                                @if(!$event->ticket_types_enabled || optional(auth()->user())->can('reply-to-questions', $event))
                                    @if ($event->allow_question_replies)
                                        <div class="reply-toggle-container">
                                            <i class="fa fa-reply" aria-hidden="true"></i>
                                            <span data-toggle="questionReplies{{ $question->id }}" class="reply-toggle">{{ $question->public_replies_count }} {{ $question->public_replies_count === 1 ? 'Reply' : 'Replies' }}</span>
                                        </div>

                                        <div data-route="{{ route('questions.replies.index', [$project->url, $event->url, $question]) }}" class="dynamic-question-reply-container hide" data-question-id="{{ $question->id }}" id="questionReplies{{ $question->id }}">
                                            @if (auth()->check() && ! auth()->user()->isBanned())
                                                <div class="dynamic-questions-postbox">
                                                    <form id="questionReplyForm-{{ $question->id }}" action="{{ route('questions.reply', [$project->url, $event->url, $question]) }}" method="POST" class="reply-form form form--ask clearfix">
                                                        <div class="question-success alert alert-success" style="display: none;" role="alert">Reply successfully sent.</div>
                                                        <div class="question-error alert alert-error" style="display: none;" role="alert">There was a problem submitting your reply, please try again.</div>

                                                        @csrf
                                                        <div class="form__group">
                                                            <textarea rows="1" name="reply" form="questionReplyForm-{{ $question->id }}" required placeholder="Type your reply"></textarea>
                                                            <button class="reply-button button button--submit float-right" >Send</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            @endif

                                            <div class="dynamic-question-replies">Loading replies...</div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        @endif
                    @endforeach
                @else
                    <div class="noq" >Be the first one to ask a question!</div>
                @endif
            </div>

            <template class="dynamic-question-template">
                <div class="dynamic-question">
                    <div class="question-info">
                        <div class="question-avatar"></div>
                        <div class="question-info-text">
                            <div class="question-name"></div>

                            <div class="question-meta">
                                <div class="question-time"></div>
                                <span class="question-report">Report this question</span>
                            </div>

                        </div>
                        @if($event->allow_question_replies && !$event->ticket_types_enabled || optional(auth()->user())->can('vote-on-questions', $event))
                            <form method="post" class="vote-form">
                                <button class="vote-button active" type="button"><span class="vote-count"></span>&nbsp;<i class="fa fa-thumbs-up"></i></button>
                            </form>
                        @endif
                    </div>
                    <span class="question-title"></span>

                    @if(!$event->ticket_types_enabled || optional(auth()->user())->can('reply-to-questions', $event))

                        <div class="reply-toggle-container">
                            <i class="fa fa-reply" aria-hidden="true"></i>
                            <span data-toggle="" class="reply-toggle">0 Replies</span>
                        </div>
                        <div data-route="" class="dynamic-question-reply-container hide">
                            <div class="dynamic-questions-postbox">
                                <form method="POST" class="reply-form form form--ask clearfix">
                                    <div class="question-success alert alert-success" style="display: none;" role="alert">Reply successfully sent.</div>
                                    <div class="question-error alert alert-error" style="display: none;" role="alert">There was a problem submitting your reply, please try again.</div>
                                    <div class="form__group">
                                        <textarea rows="1" name="reply" required="" placeholder="Type your reply"></textarea><div class="word-count">300</div>
                                        <button class="reply-button button button--submit float-right">Send</button>
                                    </div>
                                </form>
                            </div>
                            <div class="dynamic-question-replies">Loading replies...</div>
                        </div>
                    @endif
                </div>
            </template>

        </section>

    @elseif($event->hasQuestions())
        <section class="content-block spacing question-container container">
            <h4 class="">Ask a question</h4>
            <form action="{{ $event->full_url.'/questions' }}" method="post" id="question-form" class="form form--ask clearfix">

                <div class="question-success alert alert-success" style="display: none;" role="alert">Question successfully sent.</div>
                <div class="question-error alert alert-success" style="display: none;" role="alert">There was a problem submitting your question, please try again.</div>

                @csrf
                <div class="form__group">
                        <textarea style="margin-bottom: 20px;" rows="2" name="question" id="question" form="question-form"
                                  placeholder="Type your question here..." required></textarea>
                    <button class="button button--submit float-right" >Ask your question</button>
                </div>
            </form>
        </section>
        <section class="content-block spacing question-replies-container">
            <h4 class="section-title{{ $question_replies->count() === 0 ? ' hide' : '' }}">
                Question replies
                <span class="unread-replies-count">
                        (<span>{{ $question_replies->count() }}</span>)
                    </span>
            </h4>
            <div class="question-replies hide">
                @foreach ($question_replies as $reply)
                    <div class="question-reply">
                        <p>{!! $reply->reply !!}<br>
                            <em>In reply to:</em > {{ $reply->question }}
                        </p>
                    </div>
                @endforeach
            </div>
        </section>

    @endif
@endif