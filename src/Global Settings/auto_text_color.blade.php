<?php
/**
 * Auto text colour
 *
 * Automatically define the text colour of the banner based on the background overlay.
 *
 * @package StreamGo
 * @since StreamGo 1.0
 */

/* The HEX colour code. */
$background = $branding->accent_3;
$accent1 = $branding->accent_1;
$accent2 = $branding->accent_2;


/* The body text colour. */
$body = $branding->color;

/* Covert the colour to RGB. */
list($rBack, $gBack, $bBack) = sscanf($background, "#%02x%02x%02x");
list($rAcc1, $gAcc1, $bAcc1) = sscanf($accent1, "#%02x%02x%02x");
list($rAcc2, $gAcc2, $bAcc2) = sscanf($accent2, "#%02x%02x%02x");


?>

<style>

    /* Apply the background overlay to the container with 0.8 opacity. */
    .banner__overlay {
        background: rgba(<?php echo $rBack; ?>,<?php echo $gBack; ?>,<?php echo $bBack; ?>,0.8);

        /* If the client wants no background set this to 'background: none;'. */
    }

    .form-container .form-title {
        background: <?php echo $accent2; ?>;
    }

    .share a {
        background-color: <?php echo $accent2; ?>;
    }

    button,
    .button,
    input[type="submit"] {
        background-color: <?php echo $accent2; ?>;
        border-color: <?php echo $accent2; ?>;
    }

    button:hover,
    .button:hover,
    input[type="submit"]:hover {
        background-color: #333333;
        border-color: #333333;
    }

    p,
    ul,
    ol {
        /* color: <?php echo $body; ?>; */
    }

    .layout .main-content .alert {
        background: <?php echo $accent1; ?>;
    }

    .content-block h2 p {
        color: <?php echo $accent2; ?>
    }

    .sidebar-item h2 p {
        color: <?php echo $background; ?>
}
    .banner .banner__overlay .banner__login a {
        background-color: <?php echo $accent2; ?>
    }
</style>

<script>
    /* Set up the rgb variable. */
    var rgbBack = [<?php echo $rBack; ?>,<?php echo $gBack; ?>,<?php echo $bBack; ?>];
    var rgbAcc1 = [<?php echo $rAcc1; ?>,<?php echo $gAcc1; ?>,<?php echo $bAcc1; ?>];
    var rgbAcc2 = [<?php echo $rAcc2; ?>,<?php echo $gAcc2; ?>,<?php echo $bAcc2; ?>];


    /* Use the W3C algorithm to work out which text colour to use based on the
    background colour. */
    function setContrast() {

        /* http://www.w3.org/TR/AERT#color-contrast. */
        var oBack = Math.round(((parseInt(rgbBack[0]) * 299) +
            (parseInt(rgbBack[1]) * 587) +
            (parseInt(rgbBack[2]) * 114)) / 1000);
        var foreBack = (oBack > 125) ? 'black' : 'white';

        /* Add the determined colour to all the appropriate elements. */
        $('.js-auto-color').css('color', foreBack);
        $('.js-auto-color h1').css('color', foreBack);
        $('.js-auto-color h2').css('color', foreBack);
        $('.js-auto-color h3').css('color', foreBack);
        $('.js-auto-color p').css('color', foreBack);
        $('.js-auto-color span').css('color', foreBack);

        var oAcc1 = Math.round(((parseInt(rgbAcc1[0]) * 299) +
            (parseInt(rgbAcc1[1]) * 587) +
            (parseInt(rgbAcc1[2]) * 114)) / 1000);
        var foreAcc1 = (oAcc1 > 125) ? 'black' : 'white';

        /* Add the determined colour to all the appropriate elements. */
        var oAcc2 = Math.round(((parseInt(rgbAcc2[0]) * 299) +
            (parseInt(rgbAcc2[1]) * 587) +
            (parseInt(rgbAcc2[2]) * 114)) / 1000);
        var foreAcc2 = (oAcc2 > 125) ? 'black' : 'white';

        /* Add the determined colour to all the appropriate elements. */
        $('.js-auto-color').css('color', foreAcc2);
        $('.js-auto-color h1').css('color', foreAcc2);
        $('.js-auto-color h2').css('color', foreAcc2);
        $('.js-auto-color h3').css('color', foreAcc2);
        $('.js-auto-color p').css('color', foreAcc2);
        $('.banner__login button').css('border-color', foreAcc2);
        $('.banner__login button').css('color', foreAcc2);
        $('.downloads a:hover').css('color', foreAcc2);
    }
</script>