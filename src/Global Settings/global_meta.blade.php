<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{ $project->name }}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<style>
    {{ $branding->bg_color ? "body{background-color:{$branding->bg_color};}" : '' }}
    {{ $branding->border_color ? "a{color:{$branding->border_color};}" : '' }}
    {{ $branding->accent_3 ? ".section-title{color:{$branding->accent_3};}" : '' }}
    {{ $branding->border_color ? ".button, input[type='submit'], button{background-color:{$branding->border_color};}" : '' }}
    {{ $branding->bg_img ? ".banner{background-image:url(".storage_url($branding->bg_img).");}" : '' }}
    {{ $branding->accent_2 ? ".sidebar-item a{color:{$branding->accent_2};}" : '' }}
</style>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>