<link rel="stylesheet" href="https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-styles/polls.css">

<div class="poll__container">
    @if(isset($active_poll))
        <div class="poll registration-form poll__form-container">
            <form class="poll__form" action="{{ $active_poll->endpoint }}" method="post">
                @csrf

                <label class="labelTitle">{!! $active_poll->label !!}</label>

                <div class="poll__inputs">

                    @switch($active_poll->type)

                        @case('radio')
                        @foreach(explode('|', $active_poll->value) AS $val)
                            <label><input type="radio" name="poll" value="{{ $val }}"> {{ $val }}</label>
                        @endforeach
                        @break

                        @case('checkbox')
                        @foreach(explode('|', $active_poll->value) AS $val)
                            <label><input type="checkbox" name="poll" value="{{ $val }}"> {{ $val }}</label>
                        @endforeach
                        @break

                        @case('text')
                        <input type="text" name="poll">
                        @break

                        @case('textarea')
                        <textarea name="poll"></textarea>
                        @break

                    @endswitch

                </div>

                <button>Submit</button>

            </form>
        </div>

    @endif

    <template class="poll-radio">
        <label><input type="radio" name="poll" value=""></label>
    </template>

    <template class="poll-checkbox">
        <label><input type="checkbox" name="poll" value=""></label>
    </template>

    <template class="poll-text">
        <input type="text" name="poll" placeholder='Your Answer' >
    </template>

    <template class="poll-textarea">
        <textarea name="poll" placeholder='Your Answer'></textarea>
    </template>

    <template class="poll-form-template">
        <div class="poll registration-form poll__form-container">
            <form class="poll__form" method="post">
                @csrf
                <div class="poll__form-inner">
                    <label></label>
                    <div class="poll__inputs"></div>
                </div>
                <button>Submit</button>
            </form>
        </div>
    </template>

    <template class="poll-results">
        <div class="poll">
            <div class='result-container'>
                <h2></h2>
                <a class="close"></a>
                <div class="poll-results__chart"></div>
            </div>
        </div>
    </template>

    @if($stacked_active_polls)
        @foreach($stacked_active_polls as $stackedPoll)
            <template class="stacked-poll" data-json="{{ $stackedPoll }}"></template>
        @endforeach
    @endif

</div>