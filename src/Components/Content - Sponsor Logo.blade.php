@if ($event && $event->event_type === 'sponsor-page' && $event->img)
    <section class="sponsor-logo-container">
        <img class="sponsor-logo" alt="{{ $event->name }}" src="{{ storage_url($event->img) }}" />
        <div class="sponsor-logo-overlay">
            <button class="button primary sponsor-logo-modal-trigger">View Image</button>
            <a class="button primary sponsor-logo-download" href="{{ storage_url($event->img) }}" target="_blank">Download</a>
        </div>
    </section>
    <div class="modal sponsor-logo-modal" id="sponsor-logo-modal">
        <div class="modal-background"></div>
        <div class="modal-content">
            <p class="image">
                <img class="sponsor-logo" alt="{{ $event->name }}" src="{{ storage_url($event->img) }}" />
            </p>
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
    </div>
    <script>
        window.addEventListener('load', () => {
            const trigger = document.querySelector('.sponsor-logo-modal-trigger');
            const modal = document.querySelector('.sponsor-logo-modal');
            const overlay = modal.querySelector(".modal-background");
            const closeBtn = modal.querySelector(".modal-close");
            trigger.addEventListener('click', () => {
                modal.classList.add('is-active');
            });
            closeBtn.addEventListener('click', () => {
                modal.classList.remove('is-active');
            });
            overlay.addEventListener('click', () => {
                modal.classList.remove('is-active');
            });
        });
    </script>
@endif
