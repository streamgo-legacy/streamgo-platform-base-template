<section class="event-details container">
    <h4>Event details</h4>
    <div class="webinar-details">
        @if ($project->getBlock('CONTENT_DETAILS'))
            {!! $project->getBlock('CONTENT_DETAILS') !!}
        @else
            <script
                src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/smart-event-details/smart-event-details.js">
            </script>
            <div data-ed>
                <div>
                    <div>Date</div>
                    <div>Time</div>
                    <div>Duration</div>
                </div>
                <div>
                    <div data-ed-starttime="">{!! $project->start_time->tz('UTC') !!}</div>
                    <div data-ed-endtime="">{!! $project->end_time->tz('UTC') !!}</div>
                    <div data-ed-duration=""> </div>
                    <div data-ed-projectTz="" style="display:none;">Europe/London</div>
                </div>
                @if ($project->getBlock('CONTENT_DETAILS_SETTINGS'))
                    {!! $project->getBlock('CONTENT_DETAILS_SETTINGS') !!}
                    <script>
                        ed_init(ed_init_settings);
                    </script>
                @else
                    <script>
                        ed_init();
                    </script>
                @endif
            </div>
        @endif
    </div>
</section>