<section id="login" class='form-container {{ $branding->bg_img ? "" : "no-bg" }}'>
    <h4 class="form-title">Login</h4>

    <form action="{{ route('users.login', $project->url) }}" class="login form" method="post">

        @if($project->getBlock("LOGIN_PAGE_REG_SUGGESTION"))
            {!! $project->getBlock("LOGIN_PAGE_REG_SUGGESTION") !!}
        @else
        <p>Not registered? <a href="{{ route('users.register', $project->url) }}">Register here</a></p>
        <br/>
        @endif

        @csrf

        @if (session()->has('forced_logout'))
            <div class="reg-error" style="background: rgba(237,41,57,.2)">
                <h4>You have been logged out</h4>
                <p>
                    Another user has entered the event using your credentials.
                    Only one user is permitted access per set of credentials.
                    Please ensure you have not shared your access to this event.
                </p>
            </div>
            <br/>
        @endif

        @if ($errors->any())
            <div class="reg-error" style="background: rgba(237,41,57,.2)">
                <h4>Error:</h4>
                <p><b>Please complete all required fields;<br/>

                        @foreach ($errors->all() as $error)
                            <br/>&bull; {{ $error }}
                        @endforeach
                    </b></p>
                <p>If you continue to receive an error, please contact the live chat support on the bottom right of the
                    page.</p>
            </div>
            <br/>
        @endif

        @if ($project->login_required)
            <div class="form__group">
                <label for="email" class="label" >Email</label>
                <input class="input" type="email" name="email" id="email" placeholder="Enter your email address"
                    value="{{ old('email', request()->input('email')) }}"
                    required 
                />
            </div>
        @endif

        @if ($project->passcode_required !== 'no')
            <div class="form__group">
                <label class="required label" for="passcode">Passcode</label>
                <input class="input" type="password" name="passcode" id="passcode" value="{{ old('passcode') }}" required />
            </div>
        @endif

        <!-- Check to see if a passcode is required for the event, otherwise don't show this section -->
        @if ($project->passcode_required !== 'no')
            @if ($project->send_registration_emails)
                <div id="resendPasscode">
                    <a class="m-b resendPasscodeBtn" id="register-resend-confirmation-btn">Forgotten Passcode</a>
                    <p class="resend-confirmation-success" style="display: none">Success! Your registration confirmation
                        email which contains the event passcode has been resent to your email inbox. Enjoy the event!
                    </p>
                    <p class="resend-confirmation-error" style="display: none">There was an error and your passcode
                        email did not send. Please enter your email address into the Email box and click Forgotten
                        Passcode again to retry.</p>
                </div>
            @endif
        @endif
        <!-- end of passcode check code -->


        <button class="button primary" type="submit">Login</button>

    </form>
</section>
