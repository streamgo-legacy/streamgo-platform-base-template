<!-- Agenda Timeline -->
<section class="agenda-list container">
    @if (!isset($banner_existed))
        <h4 class="agenda-list__title">Agenda List</h4>
    @endif

    <ul class="items" data-switches>
        @foreach (request()->has('tags') ? $project->agenda()->with('tags')->withTagString(request()->get('tags'))->get() : $project->agenda()->with('tags')->get() as $agenda_event)
            @if ($agenda_event->room_type !== 'dynamic_round_tables')
                @if ($agenda_event->event_type === 'on-demand')
                    <li id="{!! $agenda_event->id !!}"
                        class="agenda-item {{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name) )->implode(' ') }}"
                        data-tags="{{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name) ) }}"
                        data-switch="{!! $agenda_event->start_time->valueOf() !!}"
                        data-switch-priority="1"
                        data-start-time="{!! $agenda_event->start_time->valueOf() !!}"
                        @if ($agenda_event->end_time)
                        data-end-time="{!! $agenda_event->end_time->valueOf() !!}"
                        @else
                        data-end-time="4102401599000"
                        @endif
                        data-title="{!! $agenda_event->name !!}"
                        data-url="{!! $agenda_event->url !!}"
                        data-ca-source
                        data-od
                    >
                        <div class="agenda-item-details">
                            <div class="agenda-item-times">
                                <span class="agenda-item-time agenda-item-time__after">Available On Demand</span>
                                <span class="dot"></span>
                            </div>
                            
                            <h4 class="agenda-item-title">
                                <a href="{!! $agenda_event->url !!}">
                                    {!! $agenda_event->name !!}
                                </a>
                            </h4>
                            
                            <p class="agenda-item-content">{!! $agenda_event->content !!}</p>
                            @if ($agenda_event->presenters)
                                <br />
                                <div class="agenda-item-presenters">
                                    @foreach ($agenda_event->presenters as $presenter)
                                        <div class="agenda-item-presenter js-presenter" data-id="0"
                                            data-name="{!! $presenter->full_name !!}"
                                            @if ($presenter->title) data-title="{!! $presenter->title !!}" @endif
                                            @if (is_object($presenter->photo) && $presenter->photo?->path) data-img="{!! storage_file_exists($presenter->photo->path)
                                                ? storage_url($presenter->photo->path)
                                                : $presenter->photo->path !!}" @endif
                                            @if ($presenter->company) data-company="{!! $presenter->company !!}" @endif
                                        >
                                            <div class="js-presenter-bio-trigger">
                                                @if (is_object($presenter->photo) && $presenter->photo?->path)
                                                    <img class="agenda-item-presenter-image"
                                                        src="{!! storage_url($presenter->photo->path) !!}"
                                                        alt="{!! $presenter->full_name !!}" />
                                                @endif
                                                <div class="agenda-item-presenter-details">
                                                    <span
                                                        class="agenda-item-presenter-name"
                                                        title='{!! $presenter->full_name !!}'
                                                    >{!! $presenter->full_name !!}</span>
                                                    <span
                                                        class="agenda-item-presenter-title"
                                                        title="{!! $presenter->title !!}"
                                                    >{!! $presenter->title !!}</span>
                                                    <span
                                                        class="agenda-item-presenter-company"
                                                        title="{!! $presenter->company !!}"
                                                    >{!! $presenter->company !!}</span>
                                                </div>
                                                <div class="presenter-details__bio js-presenter-bio">
                                                    {!! $presenter->bio !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            <div data-ca-buttons></div>
                        </div>
                    </li>
                @elseif ($agenda_event->end_time->valueOf() > now()->valueOf())
                <li id="{!! $agenda_event->id !!}"
                    class="agenda-item {{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name) )->implode(' ') }}"
                    data-tags="{{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name) ) }}"
                    data-switch="{!! $agenda_event->start_time->valueOf() !!}"
                    data-switch-priority="0"
                    data-start-time="{!! $agenda_event->start_time->valueOf() !!}"
                    data-end-time="{!! $agenda_event->end_time->valueOf() !!}"
                    data-hide="{!! $agenda_event->end_time->valueOf() !!}"
                    data-title="{!! $agenda_event->name !!}"
                    data-url="{!! $agenda_event->url !!}" 
                    data-ca-source
                >
                    <div
                        class="agenda-date-divider"
                        data-start-time="{!! $agenda_event->start_time->valueOf() !!}"
                    ></div>
                    <div class="agenda-item-details">
                        <div class="agenda-item-times">
                            <span class="agenda-item-time agenda-item-time__before"
                                data-hide="{!! $agenda_event->start_time->valueOf() !!}">
                                {!! $agenda_event->start_time->tz('UTC')->format('H:i A') !!}
                                {!! $agenda_event->end_time ? $agenda_event->end_time->tz('UTC')->format('- H:i A') : '' !!}</span>
                            <span class="agenda-item-time agenda-item-time__now"
                                data-show="{!! $agenda_event->start_time->valueOf() !!}"
                                data-hide="{!! $agenda_event->end_time->valueOf() !!}" style="display: none;">Live
                                NOW</span>
                            <span class="agenda-item-time agenda-item-time__after"
                                data-show="{!! $agenda_event->end_time->valueOf() !!}"
                                style="display: none;">Available
                                On Demand</span>
                            <span class="dot"></span>
                        </div>
                        
                        <h4 class="agenda-item-title">
                            <a href="{!! $agenda_event->url !!}">
                                {!! $agenda_event->name !!}
                            </a>
                        </h4>

                        <div data-ca-buttons></div>
                        <p class="agenda-item-content">{!! $agenda_event->content !!}</p>
                        @if ($agenda_event->presenters)
                            <br />
                            <div class="agenda-item-presenters">
                                @foreach ($agenda_event->presenters as $presenter)
                                    <div class="agenda-item-presenter js-presenter" data-id="0"
                                        data-name="{!! $presenter->full_name !!}"
                                        @if ($presenter->title) data-title="{!! $presenter->title !!}" @endif
                                        @if (is_object($presenter->photo) && $presenter->photo?->path) data-img="{!! storage_file_exists($presenter->photo->path)
                                            ? storage_url($presenter->photo->path)
                                            : $presenter->photo->path !!}" @endif
                                        @if ($presenter->company) data-company="{!! $presenter->company !!}" @endif
                                    >
                                        <div class="js-presenter-bio-trigger">
                                            @if (is_object($presenter->photo) && $presenter->photo?->path)
                                                <img class="agenda-item-presenter-image"
                                                    src="{!! storage_url($presenter->photo->path) !!}"
                                                    alt="{!! $presenter->full_name !!}" />
                                            @endif
                                            <div class="agenda-item-presenter-details">
                                                <span
                                                    class="agenda-item-presenter-name"
                                                    title='{!! $presenter->full_name !!}'
                                                >{!! $presenter->full_name !!}</span>
                                                <span
                                                    class="agenda-item-presenter-title"
                                                    title="{!! $presenter->title !!}"
                                                >{!! $presenter->title !!}</span>
                                                <span
                                                    class="agenda-item-presenter-company"
                                                    title="{!! $presenter->company !!}"
                                                >{!! $presenter->company !!}</span>
                                            </div>
                                            <div class="presenter-details__bio js-presenter-bio">
                                                <p>{!! $presenter->bio !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </li>
                @else

                @endif
            @endif
        @endforeach
    </ul>
</section>
<!-- end of Agenda Timeline -->