<section class="navigation">
    <div id="primary-menu-trigger">
        <svg class="svg-trigger" viewBox="0 0 100 100">
            <path
                d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20">
            </path>
            <path d="m 30,50 h 40"></path>
            <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20">
            </path>
        </svg>
    </div>

    <nav class="primary-menu with-arrows">
        <ul class="menu-container">
            @foreach ($project->navList() as $item)
                <li class="menu-item">
                    <a class="menu-link{!! (string) $item->class !!}"
                        href="@if ($item->event) /{!! $project->url !!}/{!! $item->event->url !!} @else {!! $item->link !!} @endif"
                        {!! $item->new_tab ? 'target="_blank"' : '' !!}>{!! $item->title !!}</a>
                    @if (!empty($item->children))
                        <ul class="sub-menu-container">
                            @foreach ($item->children as $child)
                                <li class="menu-item">
                                    <a class="menu-link {!! (string) $child->class !!}"
                                        href="@if ($child->event) /{!! $project->url !!}/{!! $child->event->url !!} @else {!! $child->link !!} @endif"
                                        {!! $child->new_tab ? 'target="__blank"' : '' !!}>{!! $child->title !!}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
</section>
