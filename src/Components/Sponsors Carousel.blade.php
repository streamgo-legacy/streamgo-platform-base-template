<!-- Sponsors Carousel Component -->
<section class="sponsors-carousel">
    <section class="container">
        <div class="title-wrapper">
            <h4 class="sponsors-carousel-title">
                @if ($project->getBlock('SPONSORS_CAROUSEL_TITLE'))
                    {!! $project->getBlock('SPONSORS_CAROUSEL_TITLE') !!}
                @else
                    Sponsors
                @endif
            </h4>
            <span class="title-undeline"></span>
        </div>
        <!-- slider -->
        <div class="sponsors-carousel-container sponsors-carousel-glide">
            <!-- slider track -->
            <div class="track glide__track" data-glide-el='track'>
                <ul class="slides glide__slides">
                    @foreach ($sponsorEvents = $project->events()->where('event_type', 'sponsor-page')->where('event_page_type_id', '!=' , 8)->orderBy('order')->get() as $sponsorEvent)
                    <li 
                        class="sponsor_item glide__slide {{ $sponsorEvent->tags->map(fn($tag) => Str::slug($tag->name) )->implode(' ') }}"
                        data-tags="{{ $sponsorEvent->tags->map(fn($tag) => Str::slug($tag->name)) }}"
                    >
                        <div class="sponsor_item_link">
                            <a href="{{ $sponsorEvent->full_url }}">
                                @if ($sponsorEvent->img)
                                    <img alt="{{ $sponsorEvent->name }}" src="{{ storage_url($sponsorEvent->img) }}"/>
                                @else
                                    <H4>{{ $sponsorEvent->name }}</H4>
                                @endif
                                <div class="sponsor_item_desc">
                                    {!! $project->getBlock('SPONSOR_CAROUSEL_DESC_'.$sponsorEvent->name) !!}
                                </div>
                                <div class="sponsor_item_button button primary">Visit</div>
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <!-- slider arrows -->
            <div class="glide__arrows" data-glide-el="controls">
                <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                    <span class="material-symbols-outlined">chevron_left</span>
                </button>
                <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                    <span class="material-symbols-outlined">chevron_right</span>
                </button>
            </div>
    
            <!-- slider bullets -->
            <div class="glide__bullets" data-glide-el="controls[nav]">
                @foreach ($sponsorEvents as $sponsorEvent)
                    <button class="glide__bullet" data-glide-dir="={{ $loop->index }}"></button>
                @endforeach
            </div>
        </div>

        <!-- view all button -->
        @if($sponsorListPage = $project->pages()->firstWhere('event_page_type_id', 5))
        <div class="sponsors-carousel-btn-wrapper">
            <a class="button is-rounded" href="{{ $sponsorListPage->full_url }}">
                {{ $project->getBlock('SPONSORS_CAROUSEL_ALL_BUTTON_TEXT') ?: "View All Sponsors" }}
            </a>
        </div>
        @endif
    </section>
</section>
<!-- end of Sponsors Carousel -->