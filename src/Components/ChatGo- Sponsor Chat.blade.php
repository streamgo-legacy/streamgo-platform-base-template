@if(($project->isLoggedIn() || session('chat_go_token')) && is_array($project->chatgo_event) && $event->hasAssociatedChatGoSponsor() )
	<section id='chatgo-sponsor' class='container'></section>
	<script>
      window.addEventListener('load', function(e){
          chatGo.init("{!! $project->chatgo_event['key'] !!}", {
              "mode": "embed",
              "embedId": "chatgo-sponsor",
              "dmId": {{ $event->chatgo_sponsor['id'] }},
            	isAutoAuth: true,
          });
      });
	</script>
@endif