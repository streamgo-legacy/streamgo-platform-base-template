@if ($project->getBlock('PRESENTERS'))
    <section class="presenters-container container">
        <div class="title-wrapper">
            <h4 class="agenda-carousel-title">
                @if ($project->getBlock('PRESENTERS_TITLE'))
                    {!! $project->getBlock('PRESENTERS_TITLE') !!}
                @else
                    Presenters
                @endif
            </h4>
            <span class="title-undeline"></span>
        </div>
        <div class="presenters">
            {!! $project->getBlock('PRESENTERS') !!}
        </div>
    </section>
@else
<section class="presenters-container container">
    <div class="title-wrapper">
        <h4 class="agenda-carousel-title">
            @if ($project->getBlock('PRESENTERS_TITLE'))
                {!! $project->getBlock('PRESENTERS_TITLE') !!}
            @else
                Presenters
            @endif
        </h4>
        <span class="title-undeline"></span>
    </div>
    <div class="presenters">
        @if (isset($event) && !$event->isPage())
            @php $presenters = $event->presenters; @endphp
        @else
            @php $presenters = $project->presenters; @endphp
        @endif
        
        @foreach ($presenters as $presenter)
            <div 
                class="presenter js-presenter"
                data-name="{!! $presenter->full_name !!}"
                @if ($presenter->title) data-title="{!! $presenter->title !!}" @endif
                @if (is_object($presenter->photo) && $presenter->photo?->path) data-img="{!! storage_file_exists($presenter->photo->path) ? storage_url($presenter->photo->path) : $presenter->photo->path !!}" @endif
                @if ($presenter->company) data-company="{!! $presenter->company !!}" @endif
            >
                <div class="presenter-image">
                    @if (is_object($presenter->photo) && $presenter->photo?->path)
                        <picture>
                            <source srcset="{!! storage_file_exists($presenter->photo->path)
                                ? storage_url($presenter->photo->path)
                                : $presenter->photo->path !!}" alt="{!! $presenter->full_name !!}" />
                            <img class="js-presenter-bio-trigger" src="{!! storage_file_exists($presenter->photo->path)
                                ? storage_url($presenter->photo->path)
                                : $presenter->photo->path !!}" alt="{!! $presenter->full_name !!}" />
                        </picture>
                    @else
                        <picture>
                            <source
                                srcset="https://sg-template-assets.s3.eu-west-2.amazonaws.com/user-placeholder.png"
                                alt="{!! $presenter->full_name !!}" />
                            <img class="js-presenter-bio-trigger" src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/user-placeholder.png"
                                alt="{!! $presenter->full_name !!}" />
                        </picture>
                    @endif
                </div>
                <div class="presenter-details">
                    <h4 class="primary presenter-details__name"><strong>{!! $presenter->full_name !!}</strong></h4>
                    @if ($presenter->title)
                        <p class="presenter-details__subtitle presenter-details__title">{!! $presenter->title !!}
                        </p>
                    @endif
                    @if ($presenter->company)
                        <p class="medium presenter-details__subtitle presenter-details__company">
                            {!! $presenter->company !!}</p>
                    @endif
                    @if ($presenter->bio)
                        <div class="presenter-details__bio-container">
                            <button class="presenter-details__bio-button js-presenter-bio-trigger">View Bio</button>
                            <div class="presenter-details__bio js-presenter-bio">
                                {!! $presenter->bio !!}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</section>
@endif