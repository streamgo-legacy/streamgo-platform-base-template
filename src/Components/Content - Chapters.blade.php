@if (optional($event)->supportsChapters())
  @if (optional($chapters)->count())
  <section class="container content-chapters">
    <h4 class="content-chapters__title">Chapters</h4>

    
      <ul class="content-chapters__list">
        @foreach ($chapters as $chapter)
          <li class="content-chapters__item">
            <a 
              href="#;?chapter-link={{ $chapter->id }}" 
              class="chapter-link"
              data-chapter-start="{{ $chapter->start_time }}"
            >
              <span class="chapter-time">{!! $chapter->start_time !!}</span>
              <span class='chapter-name'>{!! $chapter->name !!}</span>
            </a>
          </li>
        @endforeach
      </ul>
  </section>
  @endif
@endif
