@if(($project->isLoggedIn() || session('chat_go_token')) && is_array($project->chatgo_event))
    <section id='chatgo-embed' class='chatgo-embed-container container'></section>
    <script>
        window.addEventListener('load', function(e){
            chatGo.init("{!! $project->chatgo_event['key'] !!}", {
                "mode": "embed",
                "embedId": "chatgo-embed",
                isAutoAuth: true,
            });
        });
    </script>
@endif