{{-- $project->renderSetting('auto_text_color') --}}
</section>
</section>
</section>
<section id="banner" class="js-auto-color banner {!! $branding->bg_img ? "custom-bg" : "no-bg" !!}">
    @if($project->getBlock('PUBLIC_BANNER_CONTENT') && !$project->isLoggedIn())
        {!! $project->getBlock('PUBLIC_BANNER_CONTENT') !!}
    @elseif($project->getBlock('BANNER_CONTENT'))
        {!! $project->getBlock('BANNER_CONTENT') !!}
    @else
    <div class="banner__overlay"></div>
    <div class="container inner">
            <div class="details">
                @if (!isset($event))
                    @if (request()->routeIs('event.certificate', 'project.certificate'))
                        <div class="banner__title isEvent">
                            @if ($project->getBlock('CPD_BANNER_TITLE'))
                                <h1>{!! $project->getBlock('CPD_BANNER_TITLE') !!}</h1>
                            @else
                                <h1>{!! $project->name !!}</h1>
                            @endif
                        </div>
                    @elseif (now()->lessThan($project->start_time))
                        @if (request()->is($project->url . '/thanks'))
                            <p class="subtitle">Thanks for registering for</p>
                        @elseif (request()->is($project->url . '/login'))
                            <p class="timeToWatch">Time to watch</p>
                        @endif

                        <div class="banner__title">
                            <h1>{!! $project->getBlock('PROJECT_TITLE') ? $project->getBlock('PROJECT_TITLE') : $project->name !!}</h1>
                        </div>
                        <br />
                        <p class="banner__register-by">
                            @if (request()->is($project->url . '/register'))
                                Register by:
                            @else
                                Starting in:
                            @endif
                        </p>

                        <span class="js-countdown countdown"></span>
                    @else
                        <p>{{ now()->lessThan($project->end_time) ? 'LIVE NOW:' : ($project->getBlock('OD_TEXT') ?: "Available to view immediately on-demand") }}</p>
                        <div class="banner__title">
                            @if ($project->getBlock('PROJECT_TITLE'))
                                <h1>{!! $project->getBlock('PROJECT_TITLE') !!}</h1>
                            @else
                                <h1>{!! $project->name !!}</h1>
                            @endif
                        </div>
                    @endif
                @else
                    <div class="banner__title notEvent">
                        @if (request()->is($project->url . '/holding'))
                            <h1 class="js-auto-color">{!! $project->name !!}</h1>
                            @if ( now()->gt($project->end_time) )
                                <h4 class="js-auto-color">This Event has ended.</h4>
                            @elseif ( now()->gt($project->start_time) )
                                <h4 class="js-auto-color">This Event has started.</h4>
                                <a class="button light" href="{!! $project->full_url !!}">View Event</a>
                            @else
                                <span class="js-countdown countdown"></span>
                                <h4 class="js-auto-color">Please come back on {!! $project->start_time->format('jS F') !!}, at {!! $project->start_time->format('g:ia T') !!} to view the event</h4>
                            @endif
                        @else
                            @if ($project->getBlock('PROJECT_TITLE'))
                                <h1 class="js-auto-color">{!! $project->getBlock('PROJECT_TITLE') !!}</h1>
                            @else
                                @if($event && $event->event_type === 'sponsor-page')
                                    @if($event->img && $event->event_page_type_id !== 8)
                                        <img class="banner__sponsor-logo" alt="{{ $event->name }}" src="{{ storage_url($event->img) }}"/>
                                        <h5 class="js-auto-color banner__sponsor-title">{!! $event->name !!}</h5>
                                    @else
                                        <div class="banner__poster-wrapper">
                                            <h4 class="js-auto-color banner__poster-title">{!! $event->name !!}</h4>
                                            @if($event->img)
                                            <a
                                                href="{{ storage_url($event->img) }}"
                                                target="_blank"
                                                class="banner__poster-img__link"
                                            >
                                                <img 
                                                    class="banner__poster-img" 
                                                    alt="{{ $event->name }}" 
                                                    src="{{ storage_url($event->img) }}"
                                                />
                                                <p class="overlay">View Image</p>
                                            </a>
                                            @endif
                                        </div>
                                    @endif
                                @else
                                <h1 class="js-auto-color">{!! $event->name !!}</h1>
                                @endif
                            @endif
                        @endif
                    </div>

                    @if($eventTags = $event->tags)
                    <div class="banner-tags">
                        @foreach ($eventTags as $tag)
                            <span class="banner-tag {{ Str::slug($tag->name) }}" data-tag="{{ Str::slug($tag->name) }}">{{ $tag->name }}</span>
                        @endforeach
                    </div>
                    @endif

                @endif
            </div>


            @if ($project->login_required and !isset($event) and (!request()->routeIs('event.certificate', 'project.certificate')))
                <div class="banner__login">
                    @if ($project->isLoggedIn())
                        <span class="js-auto-color">Looks like you're registered:</span> <a href="{!! $project->full_url !!}"
                            class="button">View event</a>
                    @else
                        @if (request()->is($project->url . '/register'))
                            <span class="js-auto-color">Already registered?</span> <a
                                href="{!! route('users.login', $project->url) !!}" class="button">Login</a>
                        @elseif(request()->is($project->url . '/thanks'))
                            @if ($project->passcode_required === 'no')
                                <span class="js-auto-color">Looks like you're registered:</span> <a
                                    href="{!! $project->full_url !!}" class="button">View
                                    event</a>
                            @else
                                <span class="js-auto-color">Already registered?</span> <a
                                    href="{!! route('users.login', $project->url) !!}" class="button">Login</a>
                            @endif
                        @else
                            <span class="js-auto-color">Not registered?</span> <a href="{!! $project->full_url !!}/register"
                                class="button">Register here</a>
                        @endif
                    @endif
                </div>
            @endif
    </div>
    @endif
    <div class="banner__border">
        <div class="nectar-shape-divider-wrap" style="height:100px; background-color: transparent;" data-front="" data-style="mountains" data-position="bottom">
            <svg class="nectar-shape-divider" fill="{!! $branding->bg_color !!}" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 300" preserveAspectRatio="none"> <path d="M 1014 264 v 122 h -808 l -172 -86 s 310.42 -22.84 402 -79 c 106 -65 154 -61 268 -12 c 107 46 195.11 5.94 275 137 z" data-translate-y="-70"></path> <path d="M -302 55 s 235.27 208.25 352 159 c 128 -54 233 -98 303 -73 c 92.68 33.1 181.28 115.19 235 108 c 104.9 -14 176.52 -173.06 267 -118 c 85.61 52.09 145 123 145 123 v 74 l -1306 10 z" data-translate-y="60"></path> <path d="M -286 255 s 214 -103 338 -129 s 203 29 384 101 c 145.57 57.91 178.7 50.79 272 0 c 79 -43 301 -224 385 -63 c 53 101.63 -62 129 -62 129 l -107 84 l -1212 12 z" data-translate-y="-120"></path> <path d="M -24 69 s 299.68 301.66 413 245 c 8 -4 233 2 284 42 c 17.47 13.7 172 -132 217 -174 c 54.8 -51.15 128 -90 188 -39 c 76.12 64.7 118 99 118 99 l -12 132 l -1212 12 z" data-translate-y="120"></path> <path d="M -12 201 s 70 83 194 57 s 160.29 -36.77 274 6 c 109 41 184.82 24.36 265 -15 c 55 -27 116.5 -57.69 214 4 c 49 31 95 26 95 26 l -6 151 l -1036 10 z"></path> </svg>
        </div>
    </div>
</section>
@php
    $banner_existed = true;
@endphp
<section class="container">
    <section class="columns is-desktop">
        <section class="column">