<section class="container content_sponsor_container">
  @if ($project->getBlock('Sponsor List Title'))
    <h4 class="sponsor_title">{!! $project->getBlock('Sponsor List Title') !!}</h4>
  @else
    @if(!isset($banner_existed))
    <h4 class="sponsor_title">{!! 'Event Sponsors' !!}</h4>
    @endif
  @endif

  <div class="sponsors">
    @if ($project->getBlock('custom_sponsor_content'))

      {!! $project->getBlock('custom_sponsor_content') !!}
    @else
      @foreach ($project->events()->where('event_type', 'sponsor-page')->where('event_page_type_id', '!=' , 8)->orderBy('order')->get() as $sponsorEvent)
        <div class="sponsor_item {{ $sponsorEvent->tags->map(fn($tag) => Str::slug($tag->name) )->implode(' ') }}"
          data-tags="{{ $sponsorEvent->tags->map(fn($tag) => Str::slug($tag->name)) }}"
        >
          <div class="sponsor_item_link">
            <a href="{{ $sponsorEvent->full_url }}">
              @if ($sponsorEvent->img)
                <img alt="{{ $sponsorEvent->name }}" src="{{ storage_url($sponsorEvent->img) }}"/>
              @else
                <H4>{{ $sponsorEvent->name }}</H4>
              @endif
              <div class="sponsor_item_desc">
                {!! $project->getBlock('SPONSOR_DESC_'.$sponsorEvent->name) !!}
              </div>
              <div class="sponsor_item_button">Visit</div>
            </a>
          </div>
        </div>
      @endforeach
    @endif

  </div>

</section>