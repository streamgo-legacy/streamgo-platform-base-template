<section class="discover-go-embed-container container">
    @if($project->getBlock('DISCOVER_GO_EMBED_TITLE'))
        {!! $project->getBlock('DISCOVER_GO_EMBED_TITLE') !!}
    @else
        <h4 class="disover-go-embed-title" >Search Video</h4>
    @endif
    <div id='discover-go-embed-content' class="discover-go-embed-content">

    </div>
    <script>
        window.addEventListener('load', () => {
            const searchDiv = document.getElementById(`discover-go-container`);
            const embedDiv = document.getElementById(`discover-go-embed-content`);
            const uwVideoSearchBtn = document.querySelector('.widget-button[data-trigger="discoverGo"]');
            if(!searchDiv){return};
            embedDiv.appendChild(searchDiv);
            uwVideoSearchBtn.style.display = 'none';
        })
    </script>
</section>