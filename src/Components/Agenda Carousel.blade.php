</section>
</section>
</section>
<!-- Agenda Carousel -->
<section class="agenda-carousel container">
    <div class="title-wrapper">
        <h4 class="agenda-carousel-title">
            @if ($project->getBlock('agenda_carousel_title'))
                {!! $project->getBlock('agenda_carousel_title') !!}
            @else
                Up Next
            @endif
        </h4>
        <span class="title-undeline"></span>
    </div>
    <div class="carousel-container glide">
        <div class="track glide__track" data-glide-el='track'>
            <ul class="slides glide__slides" data-switches>
                @foreach ($agenda_events = request()->has('tags') ? $project->agenda()->with('tags')->withTagString(request()->get('tags'))->get() : $project->agenda()->with('tags')->get() as $agenda_event)
                    @if ($agenda_event->event_type === 'on-demand')
                        <li class="agenda-item glide__slide {{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name))->implode(' ') }}"
                            id="{!! $agenda_event->id !!}"
                            data-tags="{{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name)) }}"
                            data-switch="{{ $agenda_event->start_time->valueOf() }}" data-switch-priority="1"
                            data-start-time="{{ $agenda_event->start_time->valueOf() }}"
                            @if ($agenda_event->end_time) data-end-time="{!! $agenda_event->end_time->valueOf() !!}"
                            @else
                            data-end-time="4102401599000" @endif
                            data-title="{!! $agenda_event->name !!}" data-url="{!! $agenda_event->full_url !!}" data-ca-source
                            data-od>
                            <a href="{!! $agenda_event->full_url !!}" class="agenda-item-thumbnail">
                                @isset($agenda_event->img)
                                    <img src="{{ storage_url($agenda_event->img) }}" alt="{{ $agenda_event->name }}"
                                        class="agenda-carousel-preview-image" />
                                @else
                                    @isset($agenda_event->video()->poster)
                                        <img src="{{ storage_url($agenda_event->video()->poster) }}"
                                            alt="{{ $agenda_event->name }}" class="agenda-carousel-preview-image" />
                                    @else
                                        <img src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/poster-placeholder.jpg"
                                            alt="{{ $agenda_event->name }}" class="agenda-carousel-preview-image" />
                                    @endisset
                                @endisset
                            </a>

                            <div class="agenda-item-times">
                                <span class="agenda-item-time agenda-item-time__after tag light">Available
                                    OnDemand</span>
                            </div>
                            <div data-ca-buttons></div>
                            <div class="agenda-item-tags">
                                @foreach ($agenda_event->tags as $tag)
                                    <span class="agenda-item-tag" data-tag="{{ Str::slug($tag->name) }}" >{{ Str::slug($tag->name) }}</span>
                                @endforeach
                            </div>
                            <div class="agenda-item-details">
                                <h4 class="agenda-item-title">
                                    <a href="{!! $agenda_event->full_url !!}">{!! $agenda_event->name !!}</a>
                                </h4>
                            </div>
                            <div class="agenda-item-presenters">
                                @foreach ($agenda_event->presenters as $presenter)
                                <div class="agenda-item-presenter">
                                    @if (is_object($presenter->photo) && $presenter->photo?->path)
                                        <img class="agenda-item-presenter-image"
                                            src="{!! storage_url($presenter->photo->path) !!}"
                                            alt="{!! $presenter->full_name !!}" />
                                    @endif
                                    <div class="agenda-item-presenter-details">
                                        <span
                                            class="agenda-item-presenter-name"
                                            title='{!! $presenter->full_name !!}'
                                        >{!! $presenter->full_name !!}</span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </li>
                    @else
                        @if ($agenda_event->end_time->valueOf() > now()->valueOf())
                            <li class="agenda-item glide__slide {{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name))->implode(' ') }}"
                                id="{!! $agenda_event->id !!}"
                                data-tags="{{ $agenda_event->tags->map(fn($tag) => Str::slug($tag->name)) }}"
                                data-switch="{{ $agenda_event->start_time->valueOf() }}" data-switch-priority="0"
                                data-start-time="{{ $agenda_event->start_time->valueOf() }}"
                                data-end-time="{{ $agenda_event->end_time->valueOf() }}"
                                data-title="{!! $agenda_event->name !!}" data-url="{!! $agenda_event->full_url !!}" data-ca-source>
                                <a href="{!! $agenda_event->full_url !!}" class="agenda-item-thumbnail">
                                    @isset($agenda_event->img)
                                        <img src="{{ storage_url($agenda_event->img) }}" alt="{{ $agenda_event->name }}"
                                            class="agenda-carousel-preview-image" />
                                    @else
                                        @isset($agenda_event->video()->poster)
                                            <img src="{{ storage_url($agenda_event->video()->poster) }}"
                                                alt="{{ $agenda_event->name }}" class="agenda-carousel-preview-image" />
                                        @else
                                            <img src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/poster-placeholder.jpg"
                                                alt="{{ $agenda_event->name }}" class="agenda-carousel-preview-image" />
                                        @endisset
                                    @endisset
                                </a>

                                <div class="agenda-item-times">
                                    <span class="agenda-item-time agenda-item-time__before tag light"
                                        data-hide="{!! $agenda_event->start_time->valueOf() !!}">
                                        {{ $agenda_event->start_time->format('H:i') }}
                                        {{ $agenda_event->end_time ? $agenda_event->end_time->format('- H:i') : '' }}</span>
                                    <span class="agenda-item-time agenda-item-time__now tag light"
                                        data-show="{!! $agenda_event->start_time->valueOf() !!}" data-hide="{!! $agenda_event->end_time->valueOf() !!}"
                                        style="display: none;">Live NOW</span>
                                    <span class="agenda-item-time agenda-item-time__after tag light"
                                        data-show="{!! $agenda_event->end_time->valueOf() !!}" style="display: none;">Available
                                        OnDemand</span>
                                </div>
                                <div data-ca-buttons></div>
                                <div class="agenda-item-tags">
                                    @foreach ($agenda_event->tags as $tag)
                                        <span class="agenda-item-tag" data-tag="{{ Str::slug($tag->name) }}">{{ $tag->name }}</span>
                                    @endforeach
                                </div>
                                <div class="agenda-item-details">
                                    <h4 class="agenda-item-title">
                                        <a href="{!! $agenda_event->full_url !!}">{!! $agenda_event->name !!}</a>
                                    </h4>
                                </div>
                                <div class="agenda-item-presenters">
                                    @foreach ($agenda_event->presenters as $presenter)
                                    <div class="agenda-item-presenter">
                                        @if (is_object($presenter->photo) && $presenter->photo?->path)
                                            <img class="agenda-item-presenter-image"
                                                src="{!! storage_url($presenter->photo->path) !!}"
                                                alt="{!! $presenter->full_name !!}" />
                                        @endif
                                        <div class="agenda-item-presenter-details">
                                            <span
                                                class="agenda-item-presenter-name"
                                                title='{!! $presenter->full_name !!}'
                                            >{!! $presenter->full_name !!}</span>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="glide__arrows" data-glide-el="controls">
            <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                <span class="material-symbols-outlined">chevron_left</span>
            </button>
            <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                <span class="material-symbols-outlined">chevron_right</span>
            </button>
        </div>
        <div class="glide__bullets" data-glide-el="controls[nav]">
            @foreach ($agenda_events as $agenda_event)
                    @if ($agenda_event->event_type === 'on-demand')
                        <button class="glide__bullet" data-glide-dir="={{ $loop->index }}"></button>
                    @else
                        @if ($agenda_event->end_time->valueOf() > now()->valueOf())
                        <button class="glide__bullet" data-glide-dir="={{ $loop->index }}"></button>
                        @endif
                    @endif
                @endforeach
        </div>
    </div>
    @if($eventListPage = $project->pages()->firstWhere("event_page_type_id", 2))
    <div class="agenda-page-btn-wrapper">
        <a class="button is-rounded" href="{{ $eventListPage->full_url }}">
            <i class="fa fa-calendar"></i>
            {{ $project->getBlock('AGENDA_CAROUSEL_ALL_BUTTON_TEXT') ?: "View All Events" }}
        </a>
    </div>
    @endif
</section>
<!-- end of Agenda Carousel -->
<section class="container">
    <section class="columns is-desktop">
        <section class="column">
