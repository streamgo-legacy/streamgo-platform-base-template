<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .event-interact .interact-chooser form input[type="submit"] {
        margin-top: 1rem;
        border-radius: 0;
        min-width: 100px;
    }
    .event-interact .tab-button-container {
        margin-bottom: 1rem;
    }
    .tab {
        border-radius: 0;
        /*border-top-left-radius: 15px;
        border-top-right-radius: 15px;*/
        min-width: 100px;
        border: none;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .tab:not(.tab--active) {
        background: #ccc;
    }
    .tab-button-container {
        display: flex;
        align-items: center;
    }

    .tab-count {
        border-radius: 50%;
        color: #ED2939;
        background: white;
        font-size: 12px;
        width: 18px;
        height: 18px;
        margin-left: 10px;
    }

    #dynamic-questions-wrapper{
        display: flex;
        flex-direction: column;
        background: #eee;
        box-sizing: border-box;
        padding: 12px !important;
        max-height: 70vh;
        margin: 2rem 0rem;
        border-radius: 16px;
        box-shadow: inset 0px 0px 8px #ddd;
        font-size: 14px;
        overflow: scroll;
        border: 1px solid #eee;
    }
    .dynamic-questions-wrapper button{
        border-radius: 99px;
    }
    .dynamic-questions-postbox{
        box-shadow: 0px 12px 24px rgba(0,0,0,0.07);
        background: #fff;
        border-radius: 8px;
        box-sizing: border-box;
        padding: 12px;
        transition: 500ms;
    }
    .dynamic-questions-postbox .form__group textarea{
        min-height: 0px;
        transition: 500ms;
        border: none;
        resize: none;
        height: 22px;
        margin: 0px;
        padding: 0px 12px;
    }
    .dynamic-questions-postbox .form__group button{
        display: none;
    }
    .dynamic-questions-postbox.opened .form__group textarea{
        height: 80px;
    }
    .dynamic-questions-postbox.opened .form__group button{
        display: block;
    }
    .dynamic-questions-list{
        /* margin-top: 24px; */
    }
    .dynamic-question{
        display: flex;
        flex-direction: column;
        background: #fff;
        box-sizing: border-box;
        padding: 12px;
        border-radius: 8px;
        box-shadow: 0px 6px 12px rgba(0,0,0,0.07);
        margin-bottom: 12px;
    }
    .dynamic-question .question-info{
        display: flex;
        flex-direction: row;
    }
    .dynamic-question .question-avatar{
        width: 36px;
        height: 36px;
        background: #ddd;
        border-radius: 99px;
        font-size: 18px;
        color: #fff;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .dynamic-question .question-info-text{
        display: flex;
        flex-direction: column;
        align-items: start;
        justify-content: space-evenly;
        margin-left: 12px;
    }
    .question-name{
        font-weight: bold;
    }
    .question-time{
        font-size: 12px;
        color: #888;
    }
    .dynamic-question .question-info .vote-form{
        margin-left: auto;
    }
    .dynamic-question .vote-button, .dynamic-questions-postbox .form__group button{
        padding: 4px 16px;
        font-size: 14px;
    }
    .dynamic-question .vote-button > *{
        pointer-events: none;
    }
    .dynamic-question .vote-button:disabled, .dynamic-question .vote-button:disabled:hover{
        padding: 2px 16px;
        font-size: 14px;
        color: #135 !important;
        box-sizing: border-box;
        border: 1px solid #fff;
        box-shadow: 0px 0px 0px 2px #d0d0d0;
    }
    .dynamic-question .question-title{
        margin-top: 12px;
        font-size: 14px;
        padding: 4px;
    }
    .toggle-question-order, .toggle-question-order:focus{
        display: inline-block;
        width: auto;
        margin: 36px auto 12px 16px;
        font-size: 14px;
        padding: 6px 24px;
    }
    .dynamic-questions-postbox .form__group{
        margin: 0px;
    }
    .dynamic-questions-postbox .alert{
        font-size: 14px;
        border-radius: 8px;
        padding: 12px !important;
        margin: 12px;
        box-sizing: border-box;
        width: auto;
    }
    .dynamic-questions-postbox .word-count{
        text-align: right;
        margin: 12px;
        font-size: 11px;
        color: #999;
        display: none;
    }
    .dynamic-questions-postbox.opened .word-count{
        display: block;
    }
    .dynamic-questions-actions{
        display: flex;
        flex-direction: row;
        justify-content: start;
        align-items: baseline;
    }
    .dynamic-questions-actions.hide{
        display: none;
    }
    .dynamic-questions-count{
        color: #666;
        margin-right: 12px;
    }
    .dynamic-questions-list .noq{
        padding: 48px;
        display: block;
        font-size: 16px;
        text-align: center;
    }

    .hide {
        display: none;
    }
</style>

<section class="interact-chooser content-block content-block--text spacing spacing--bottom-only">
    <h2>Choose an event to interact with</h2>
    <form method="POST" action="{{ route('event.interact.choose-event', [$project->url]) }}">
        @csrf
        <div class="form__group">
            <label for="event">Event Name</label>
            <select class="js-select2" name="event" id="event">
                <option value="">Please choose an event to interact with</option>
                @foreach ($events as $eventChoice)
                    <option {{ ! empty($event) && $eventChoice->url === $event->url ? 'selected ' : '' }}value="{{ $eventChoice->url }}">{{ $eventChoice->name }}</option>
                @endforeach
            </select>
        </div>
        <input type="submit" value="Go" />
    </form>
</section>

@if (! empty($event))
    <div class="tab-button-container">
        <button type="button" class="tab tab--active" data-tab="questionsTab">
            Questions
            <div class="dynamic-questions-count tab-count">{{ count($dynamic_questions) }}</div>
        </button>
        @isset($polls)
            <button type="button" class="tab" data-tab="pollsTab">
                Polls
            </button>
        @endisset
    </div>
    <div class="tab-container">
        <div id="questionsTab" class="tab-content">
            <section id="dynamic-questions-wrapper" class="dynamic-questions-wrapper content-block spacing spacing--bottom-only">
                <div class="dynamic-questions-postbox">
                    <!-- <h2 class="section-title">Ask a question</h2> -->
                    <form action="{{ $event->full_url.'/questions' }}" method="post" id="question-form" class="form form--ask clearfix">
                        <div class="question-success alert alert-success" style="display: none;" role="alert">Question successfully sent.</div>
                        <div class="question-error alert alert-error" style="display: none;" role="alert">There was a problem submitting your question, please try again.</div>
                        @csrf
                        <div class="form__group">
                            <textarea rows="1" name="question" id="question" form="question-form" required placeholder="Type you question"></textarea>
                            <button class="button button--submit float-right" >Send</button>
                        </div>
                    </form>
                </div>
                <div class="dynamic-questions-actions{{ count($dynamic_questions) > 0 ? '': ' hide' }}">
                    <button type="button" class="toggle-question-order" data-sort-by="{{ $event->dynamic_question_sort === 'newest' ? 'votes' : 'newest' }}">
                        {{ $event->dynamic_question_sort === 'newest' ? 'Sort by Votes' : 'Sort by Newest' }}
                    </button>
                    <div class="dynamic-questions-count">{{ count($dynamic_questions) }} question{{ count($dynamic_questions) > 1 ? 's':'' }}</div>
                </div>
                <div class="dynamic-questions-list" data-event-id="{{ $event->id }}">
                    @forelse($dynamic_questions as $question)
                        <div class="dynamic-question" data-question-id="{{ $question->id }}"{{ $question->hasUserVoted(auth()->user()) ? " data-question-voted=true" : '' }} data-votes="{{ $question->votes_count }}" data-created="{{ $question->created_at->timestamp }}">
                            <div class="question-info">
                                <div class="question-avatar" data-name="{{ $question->user->name ?? $question->name }}">{{ $question->user->name[0] ?? $question->name[0] }}</div>
                                <div class="question-info-text">
                                    <div class="question-name">{{ $question->user->name ?? $question->name }}</div>
                                    <div class="question-time" data-time="{{ $question->created_at }}"></div>
                                </div>
                                @if (auth()->check() && ! $question->hasUserVoted(auth()->user()))
                                    <form class="vote-form" method="post" data-question-id="{{ $question->id }}" action="{{ route('questions.vote.store', [$project->url, $event->url, $question]) }}">
                                        @csrf
                                        <button class="vote-button" data-question-id="{{ $question->id }}" type="button"><span class="vote-count">{{ $question->votes_count }}</span>&nbsp;<i class="fa fa-thumbs-up"></i></button>
                                    </form>
                                @else
                                    <div class="vote-form">
                                        <button disabled class="vote-button" data-question-id="{{ $question->id }}" type="button"><span class="vote-count">{{ $question->votes_count }}</span>&nbsp;<i class="fa fa-thumbs-up"></i></button>
                                    </div>
                                @endif
                            </div>
                            <span class="question-title">{{ $question->question }}</span>
                        </div>
                    @empty
                        <div class="noq" >Be the first one to ask a question!</div>
                    @endforelse
                </div>
                <template class="dynamic-question-template">
                    <div class="dynamic-question">
                        <div class="question-info">
                            <div class="question-avatar"></div>
                            <div class="question-info-text">
                                <div class="question-name"></div>
                                <div class="question-time"></div>
                            </div>
                            <form method="post" class="vote-form">
                                <button class="vote-button active" type="button"><span class="vote-count"></span>&nbsp;<i class="fa fa-thumbs-up"></i></button>
                            </form>
                        </div>
                        <span class="question-title"></span>
                    </div>
                </template>

            </section>
            <section class="content-block spacing question-replies-container">
                <h2 class="section-title">
                    Question replies
                    <span class="unread-replies-count">
                        ({{ $question_replies->where('seen', null)->count() }})
                    </span>
                </h2>
                <div class="question-replies hide">
                    @forelse ($question_replies as $reply)
                        <div class="question-reply">
                            <p>{!! $reply->reply !!}<br>
                                <em>In reply to:</em > {{ $reply->question }}
                            </p>
                        </div>
                    @empty
                    @endforelse
                </div>
            </section>
        </div>
        <div id="pollsTab" class="tab-content hide poll__container">

            @if(! empty($polls))
                @foreach ($polls as $active_poll)

                    <div class="registration-form poll__form-container" data-poll-id="{{ $active_poll->id }}" >
                        <form class="poll__form" action="{{ $active_poll->endpoint }}" method="post">
                            @csrf

                            <label class="labelTitle">{!! $active_poll->label !!}</label>

                            <div class="poll__inputs">

                                @switch($active_poll->type)

                                    @case('radio')
                                    @foreach(explode('|', $active_poll->value) AS $val)
                                        <label><input type="radio" name="poll" value="{{ $val }}"> {{ $val }}</label>
                                    @endforeach
                                    @break

                                    @case('checkbox')
                                    @foreach(explode('|', $active_poll->value) AS $val)
                                        <label><input type="checkbox" name="poll" value="{{ $val }}"> {{ $val }}</label>
                                    @endforeach
                                    @break

                                    @case('text')
                                    <input type="text" name="poll">
                                    @break

                                    @case('textarea')
                                    <textarea name="poll"></textarea>
                                    @break

                                @endswitch

                            </div>

                            <button>Submit</button>

                        </form>
                    </div>
                @endforeach

            @endif

            <template class="poll-radio">
                <label><input type="radio" name="poll" value=""></label>
            </template>

            <template class="poll-checkbox">
                <label><input type="checkbox" name="poll" value=""></label>
            </template>

            <template class="poll-text">
                <input type="text" name="poll">
            </template>

            <template class="poll-textarea">
                <input type="text" name="poll">
            </template>

            <template class="poll-form-template">
                <div class="registration-form poll__form-container">
                    <form class="poll__form" method="post">
                        @csrf
                        <label></label>
                        <div class="poll__inputs"></div>
                        <button>Submit</button>
                    </form>
                </div>
            </template>

            <template class="poll-results">
                <div class="poll">
                    <h2></h2>
                    <a class="close"></a>
                    <div class="poll-results__chart"></div>
                </div>
            </template>

        </div>
    </div>
    @isset($alert)
        <div class="ws-alert alert">
            <a class="close">Close</a>
            <p class="content">
                {!! $alert->payload !!}
            </p>
        </div>
    @endisset



@endif