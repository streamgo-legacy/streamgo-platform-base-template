<!-- <script src="https://cdn-aws.s3-eu-west-1.amazonaws.com/cdn-sg-productions/scripts/breakout.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/breakout-room/breakoutStaging-dev.js">
</script>

<div id="lecture-theatre">

    <div id="background-container" class="lecture-theatre">

        <div class="primary">

            <h1>{{ $event->name }}</h1>

            <div id="video-section" class="content-video">

                <div class="grid">

                    <div class="col col-1 hide-for-medium-down"></div>

                    <div class="col col-md col-10">

                        <section class="content-block spacing spacing--bottom-only">

                            @php
                                $hex_color = isset($branding->accent_2) ? str_replace('#', '', $branding->accent_2) : '44BDB5';
                                $display_name = $project->getBlock('Breakout Display Name Format') ? $project->getBlock('Breakout Display Name Format') : $user->full_name;
                            @endphp

                            <div class="breakoutVc">
                                <div id="breakoutFull">Sorry this breakout room is full</div>
                            </div>

                            @if ($event->room_type == 'individual_meetings')

                                <script>
                                    breakoutRoom.initSimple(
                                        '{{ $event->id }}-breakout', // room name
                                        '{{ $display_name }}',
                                        '{{ $hex_color }}', // hex colour (optional - default streamGo colour)
                                        {{ $event->toggle_chat }}, // chat (optional - default true)
                                        {{ $event->toggle_screen_share }}, // screenshare (optional - default true)
                                        {{ $event->toggle_video }} // video (optional - default true)
                                    );
                                </script>
                            @elseif($event->room_type == 'round_tables')
                                <script>
                                    console.log('init');
                                    breakoutRoom.init(
                                        '{{ $event->id }}-breakout-', // room prefix
                                        '{{ $user->email }}', // unique identifier
                                        '{{ $display_name }}',
                                        '{{ $event->max_people_per_room }}', // max per room
                                        '{{ $event->max_rooms }}', // max rooms
                                        '{{ $hex_color }}', // hex colour (optional - default streamGo colour)
                                        {{ $event->toggle_chat }}, // chat (optional - default true)
                                        {{ $event->toggle_screen_share }}, // screenshare (optional - default true)
                                        {{ $event->toggle_video }} // video (optional - default true)
                                    );
                                </script>
                            @elseif($event->room_type == 'dynamic_round_tables')
                                <style>
                                    .sgBreakoutroom_card {
                                        box-sizing: border-box;
                                        box-shadow: 0px 0px 12px #ddd;
                                        padding: 1.5rem;
                                        display: flex;
                                        flex-direction: column;
                                        min-width: 200px;
                                        width: 25%;
                                        border-radius: 6px;
                                        justify-content: center;
                                    }

                                    .sgBreakoutroom_cover {
                                        text-align: center;
                                        margin-bottom: 1.5rem;
                                    }

                                    @media only screen and (max-width: 600px) {
                                        .sgBreakoutroom_card {
                                            width: 100%;
                                        }
                                    }
                                </style>

                                @for ($i = 0; $i < $event->max_rooms; $i++)
                                    <div id='table-cover-template-{{ $i }}' class='sgBreakoutroom_cover'
                                        style="display: none;">
                                        @if ($project->getBlock('roundtable_image_' . $i))
                                            {!! $project->getBlock('roundtable_image_' . $i) !!}
                                        @endif
                                        @if ($project->getBlock('roundtable_title_' . $i))
                                            {!! $project->getBlock('roundtable_title_' . $i) !!}
                                        @endif
                                    </div>
                                @endfor

                                <script id='dynamic-round-table-script-tag'>
                                    @if ($project->getBlock('roundtable_title_1'))
                                        console.log(`{!! $project->getBlock('roundtable_title_1') !!}`);
                                    @endif

                                    // get all round table settings here as vars first
                                    const DynamicRoundtableSettings = {
                                        maxRooms: parseInt(`{{ $event->max_rooms }}`),
                                        maxPerRoom: parseInt(`{{ $event->max_people_per_room }}`),
                                        roomPrefix: `{{ $event->id }}-breakout`,
                                        userId: `{{ $user->email }}`,
                                        userDisplayName: `{{ $display_name }}`,
                                        roomDisplayName: `Table`
                                    }
                                    document.getElementById('breakoutFull').style.display = 'none';

                                    window.addEventListener('load', () => {
                                        // hide the breakoutVc for now
                                        document.getElementById('breakoutFull').style.display = 'none';
                                        var scriptTag = document.getElementById('dynamic-round-table-script-tag');
                                        var tableContainer = document.createElement('div');
                                        tableContainer.style.display = 'flex';
                                        tableContainer.style.flexDirection = 'row';
                                        tableContainer.style.flexWrap = 'wrap';
                                        tableContainer.id = 'round-table-table-container';
                                        scriptTag.insertAdjacentElement('beforebegin', tableContainer);

                                        const createTableDiv = async (index) => {
                                            var tableDiv = document.createElement('div');
                                            var tableHolder = document.createElement('div');
                                            var tableId = `${DynamicRoundtableSettings.roomPrefix}-${index}`;

                                            var cover = document.getElementById(`table-cover-template-${index}`);
                                            tableDiv.id = tableId + `-div`;
                                            tableHolder.id = tableId + `-table`;
                                            tableDiv.classList.add('sgBreakoutroom_card');
                                            tableDiv.style.minWidth = '200px';
                                            tableDiv.style.margin = '12px';
                                            tableDiv.insertAdjacentElement('beforeend', tableHolder);
                                            tableContainer.insertAdjacentElement('beforeend', tableDiv);
                                            if (cover) {
                                                tableDiv.insertAdjacentElement('afterbegin', cover);
                                                cover.style.display = 'block';
                                            }
                                            return tableHolder.id;
                                        }

                                        const openDynamicRoom = (roomName) => {
                                            var tableContainer = document.getElementById('round-table-table-container');
                                            tableContainer.style.display = 'none';

                                            var breakoutVc = document.querySelector('.breakoutVc');
                                            breakoutVc.style.display = 'block';
                                            var closeIFrameDiv = document.createElement('div');
                                            closeIFrameDiv.id = 'close-iframe-div';
                                            var closeButton = document.createElement('div');
                                            closeButton.innerHTML = '< Back';
                                            closeButton.style.padding = '6px';
                                            closeButton.style.color = '#fff';
                                            closeButton.style.cursor = 'pointer';
                                            closeButton.addEventListener('click', () => {
                                                console.log('back clicked');
                                                var iframe = document.getElementById('breakoutIframe');
                                                var closeIFrameDiv = document.getElementById('close-iframe-div');
                                                iframe.remove();
                                                closeIFrameDiv.remove();
                                                document.getElementById('breakoutLoadingContainer').remove();
                                                breakoutVc.style.display = 'none';
                                                tableContainer.style.display = 'flex';
                                            });
                                            closeIFrameDiv.insertAdjacentElement('afterbegin', closeButton);
                                            breakoutVc.insertAdjacentElement('afterbegin', closeIFrameDiv);

                                            breakoutRoom.dynamic(
                                                roomName, // room name
                                                '{{ $user->email }}', // unique identifier
                                                '{{ $display_name }}',
                                                '{{ $event->max_people_per_room }}', // max per room
                                                '{{ $hex_color }}', // hex colour (optional - default streamGo colour)
                                                {{ $event->toggle_chat }}, // chat (optional - default true)
                                                {{ $event->toggle_screen_share }}, // screenshare (optional - default true)
                                                {{ $event->toggle_video }} // video (optional - default true)
                                            );
                                        }

                                        const createNewTable = async (index) => {
                                            const newTable = await createTableDiv(index);

                                            breakoutRoom.getRoomParticipantsTableGraphicalCallback({
                                                elementId: newTable, // ID of the element you want to update
                                                roomName: `${DynamicRoundtableSettings.roomDisplayName}-${index}`, // room name of the dynamic breakout
                                                update: true, // does it auto update (Every 5 seconds)
                                                seatRoomUrl: '{{ $project->full_url }}/roundtable-06-09-21-room',
                                                maxSpaces: 10, // what is the max number of users in room
                                                accentColor: '{{ $branding->accent_1 }}',
                                                tableDisplayName: `${DynamicRoundtableSettings.roomDisplayName} ${index+1}`,
                                                callback: () => openDynamicRoom(
                                                    `${DynamicRoundtableSettings.roomDisplayName}-${index}`)
                                            });
                                        }

                                        for (var i = 0; i < DynamicRoundtableSettings.maxRooms; i++) {
                                            createNewTable(i);
                                        }
                                    });
                                </script>

                            @endif


                        </section>

                    </div>

                    <div class="col col-1 hide-for-medium-down"></div>

                </div>

            </div>
