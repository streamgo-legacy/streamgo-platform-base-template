@if (now()->lessThan($project->start_time->subMinutes(30)))
    <section id="calendar-buttons" class="calendar-buttons container">
        <h4 class="">Add to your calendar</h4>
        <div class="grid">
            <a 
                target="_blank"
                class='button button--calendar button--google'
                href="https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start={!! $project->start_time->format('d-m-Y H:i') !!}&end={!! $project->end_time->format('d-m-Y H:i') !!}&alarm=10&location={!! $project->full_url !!}%2Flogin&title={!! urlencode($project->name) !!}&description={!! urlencode($project->getBlock('CALCONTENT')) !!}&timezone=UTC&service=google">
                <i class="bi bi-google"></i> Google Calendar
            </a>
            <a 
                target="_blank"
                class='button button--calendar button--outlook'
                href="https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start={!! $project->start_time->format('d-m-Y H:i') !!}&end={!! $project->end_time->format('d-m-Y H:i') !!}&alarm=10&location={!! $project->full_url !!}%2Flogin&title={!! urlencode($project->name) !!}&description={!! urlencode($project->getBlock('CALCONTENT')) !!}&timezone=UTC&service=outlook">
                <i class="bi bi-microsoft"></i> Outlook
            </a>
            <a
                target='_blank'
                class='button button--calendar button--ical'
                href="https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start={!! $project->start_time->format('d-m-Y H:i') !!}&end={!! $project->end_time->format('d-m-Y H:i') !!}&alarm=10&location={!! $project->full_url !!}%2Flogin&title={!! urlencode($project->name) !!}&description={!! urlencode($project->getBlock('CALCONTENT')) !!}&timezone=UTC&service=apple">
                <i class="bi bi-apple"></i> iCal
            </a>
            <a 
                target="_blank"
                class='button button--calendar button--yahoo'
                href="https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start={!! $project->start_time->format('d-m-Y H:i') !!}&end={!! $project->end_time->format('d-m-Y H:i') !!}&alarm=10&location={!! $project->full_url !!}%2Flogin&title={!! urlencode($project->name) !!}&description={!! urlencode($project->getBlock('CALCONTENT')) !!}&timezone=UTC&service=yahoo">
                <i class="bi bi-envelope"></i> Yahoo
            </a>
        </div>
    </section>
@endif