<!-- Header Component v1.5.0 -->
<body class="main-content event-{{ last_segment() }} {{ last_segment() }}{{ isset($event) ? ($event->isPage() ? ' isPage' : ' isEvent') : '' }}{{ $project->isLoggedIn() ? ' isLoggedIn' : ' isPublic' }}">
    @if (! request()->is($project->url . '/holding'))
    
        @if ($project->isLoggedIn() && !request()->routeIs('users.thanks'))
            <style>
                .watch-live-banner{
                    display: none;
                }
            </style>
            {!! $project->type->id === 2 ? $project->getBlock('GLOBAL_ALERTS') : '' !!}
        @endif
        
        <header id="site-head" class="site-head">

            <section class="unified-widget{{ $project->isLoggedIn() && !request()->routeIs('users.thanks') ? ' shouldCollapse' : ' noCollapse' }}">
                @if ($project->getBlock('UW_SETTINGS'))
                    {!! $project->getBlock('UW_SETTINGS') !!}
                @else
                    <script>
                        var uwSettings = {
                            customAgenda: {
                                enable: {{ $project->type->id === 2 ? 'true' : 'false' }}
                            },
                            discoverGo: {
                                enable: true
                            },
                            treasureHunt: {
                                enable: {{ $project->hasTreasureHunt() ? "true" : "false" }},
                            },
                            chatGo: {
                                enable: {{ is_array($project->chatgo_event) ? 'true' : 'false' }}
                            },
                            cpd: {
                                enable: {{ !empty($project->cpdSetting) && $project->cpdSetting->enabled ? 'true' : 'false' }}
                            },
                            leaderboard: {
                                enable: {{ $project->type->id === 2 && $project->leaderboard_setting && $project->leaderboard_setting?->enabled ? 'true' : 'false' }}
                            },
                            techSupport: {
                                enable: true
                            }
                        }
                    </script>
                @endif
    
                <div class="inner container">
                    @if ($project->isLoggedIn() && !request()->routeIs('users.thanks') && $project->type->id === 2)
                        <div 
                            class="alerts-container"
                            @if($eventListPage = $project->pages()->firstWhere("event_page_type_id", 2))
                                data-agenda-url="{{ $eventListPage->full_url }}"
                            @endif
                        ></div>
                    @endif
                    <div class="widget-buttons collapse">
                        <button class="widget-toggle">
                            <i class="bi bi-three-dots expand-icon"></i>
                            <i class="bi bi-arrow-right collapse-icon"></i>
                        </button>
                        @if ($project->isLoggedIn() && !request()->routeIs('users.thanks'))
                            <button class="widget-button" data-trigger="customAgenda" >
                                <i class="bi bi-calendar-check-fill"></i>
                                <div class="uwToolTip">
                                    <span class="arrow"></span>
                                    <span class="tip">{!! $project->getBlock('UW_CUSTOM_AGENDA_TITLE') ?: 'My Agenda' !!}</span>
                                </div>
                            </button>
    
                            <button class="widget-button" data-trigger="discoverGo" >
                                <i class="bi bi-search"></i>
                                <div class="uwToolTip">
                                    <span class="arrow"></span>
                                    <span class="tip">{!! $project->getBlock('UW_DISCOVERGO_TITLE') ?: 'Search Videos' !!}</span>
                                </div>
                            </button>
    
                            <button class="widget-button" data-trigger="treasureHunt" >
                                <i class="bi bi-gem"></i>
                                <div class="uwToolTip">
                                    <span class="arrow"></span>
                                    <span class="tip">
                                        @if($project->hasTreasureHunt())
                                        {{ $project->treasure_setting->title ?: 'Treasure Hunt' }}
                                        @else
                                        Treasure Hunt
                                        @endif
                                    </span>
                                </div>
                            </button>
    
                            <button class="widget-button" data-trigger="chatGo" >
                                <i class="bi bi-chat-fill"></i>
                                <div class="uwToolTip">
                                    <span class="arrow"></span>
                                    <span class="tip">{!! $project->getBlock('UW_CHATGO_TITLE') ?: 'ChatGo' !!}</span>
                                </div>
                                <div class="uw-chatgo-notifications-container" id='chatgo-new-chat-bubble'>
                                    <div class="arrow"></div>
                                    <div id='chatgo-new-chat-bubble-content'></div>
                                </div>
                            </button>
    
                            <button class="widget-button" data-trigger="cpd" >
                                <i class="bi bi-award-fill"></i>
                                <div class="uwToolTip">
                                    <span class="arrow"></span>
                                    <span class="tip">{!! $project->getBlock('UW_CPD_TITLE') ?: 'Certificates' !!}</span>
                                </div>
                            </button>
                            <button class="widget-button" data-trigger="leaderboard" >
                                <i class="bi bi-trophy-fill"></i>
                                <div class="uwToolTip">
                                    <span class="arrow"></span>
                                    <span class="tip">{{ $project->leaderboard_setting?->title ?: 'Leaderboard' }}</span>
                                </div>
                            </button>
                        @endif
                        <button
                                class="widget-button"
                                data-trigger="techSupport"
                        >
                            <i class="bi bi-question-square-fill"></i>
                            <p class="button-text">{!! $project->getBlock('UW_TECH_SUPPORT_TITLE') ?: 'Tech Support' !!}</p>
                            <div class="uwToolTip">
                                <span class="arrow"></span>
                                <span class="tip">{!! $project->getBlock('UW_TECH_SUPPORT_TITLE') ?: 'Tech Support' !!}</span>
                            </div>
                        </button>
                    </div>
                    @if ($project->isLoggedIn() && !request()->routeIs('users.thanks'))
                        <div class="widget-drawers">
                            <div class="widget-drawer" data-drawer="customAgenda" >
                                <h4 class="drawer-title">
                                    {!! $project->getBlock('UW_CUSTOM_AGENDA_TITLE') ?: 'My Agenda' !!}
                                    <i class="las la-times closeBtn"></i>
                                </h4>
                                @if($project->getBlock('UW_CUSTOM_AGENDA_DESC'))
                                    <div class="drawer-desc">
                                        {!! $project->getBlock('UW_CUSTOM_AGENDA_DESC') !!}
                                    </div>
                                @endif
                                <div class="drawer-content" data-ca-container >
    
                                </div>
                            </div>
                            <div class="widget-drawer" data-drawer="discoverGo" >
                                <h4 class="drawer-title">{!! $project->getBlock('UW_DISCOVERGO_TITLE') ?: 'Search Videos' !!}
                                    <i class="las la-times closeBtn"></i>
                                </h4>
                                @if($project->getBlock('UW_DISCOVERGO_DESC'))
                                    <div class="drawer-desc">
                                        {!! $project->getBlock('UW_DISCOVERGO_DESC') !!}
                                    </div>
                                @endif
                                <div id='discover-go-container' class="drawer-content iframe"></div>
                            </div>
                            <div class="widget-drawer" data-drawer="treasureHunt">
                                @if($project->hasTreasureHunt())
                                    <h4 class="drawer-title">
                                        {{ $project->treasure_setting->title ?: 'Treasure Hunt' }}
                                        <i class="las la-times closeBtn"></i>
                                    </h4>
                                    @if($project->treasure_setting->instruction_text)
                                        <div class="drawer-desc">
                                            {{ $project->treasure_setting->instruction_text }}
                                        </div>
                                    @endif
                                    <div id="th-passport" class="th-passport"></div>
                                @endif
                            </div>
                            <div class="widget-drawer" data-drawer="chatGo">
                                <h4 class="drawer-title">
                                        {!! $project->getBlock('UW_CHATGO_TITLE') ?: 'ChatGo' !!}
                                    <i class="las la-times closeBtn"></i>
                                </h4>
                                @if($project->getBlock('UW_CHATGO_DESC'))
                                    <div class="drawer-desc">
                                        {!! $project->getBlock('UW_CHATGO_DESC') !!}
                                    </div>
                                @endif
                                <div id='chatGo-container' class="drawer-content iframe"></div>
                            </div>
                            <div class="widget-drawer" data-drawer="cpd">
                                <h4 class="drawer-title">
                                        {!! $project->getBlock('UW_CPD_TITLE') ?: 'Certificates' !!}
                                    <i class="las la-times closeBtn"></i>
                                </h4>
                                @if($project->getBlock('UW_CPD_DESC'))
                                    <div class="drawer-desc">
                                        {!! $project->getBlock('UW_CPD_DESC') !!}
                                    </div>
                                @endif
                                @if(!empty($project->cpdSetting) && $project->cpdSetting->enabled)
                                    <div class="drawer-content iframe">
                                        <div 
                                            class="uw-cpd-progress-container"
                                            @if(isset($event) && $event->id && $event->activeCpd)
                                            data-event-id="{{ $event->id }}"
                                            data-event-start-time="{{ $event->start_time->valueOf() }}"
                                            data-event-end-time="{{ $event->end_time ? $event->end_time->valueOf() : '4102401599000' }}"
                                            data-event-type="{{ $event->event_type }}"
                                            @endif
                                        >
                                            <p class="uw-cpd-progress-text">
                                                <span class="uw-cpd-progress-progress"></span>
                                            </p>
                                            <span class="material-symbols-outlined uw-cpd-refresh-btn">refresh</span>
                                        </div>
                                        <a class="button uw-cpd-full-progress-btn" href='{{ route('project.certificate', $project->url) }}' target="_blank">
                                            View Full Progress
                                        </a>
                                    </div>
                                @endif
                            </div>
                            <div class="widget-drawer" data-drawer="leaderboard">
                                <h4 class="drawer-title">
                                        {!! $project->getBlock('UW_LEADERBOARD_TITLE') ?: 'Leaderboard' !!}
                                    <i class="las la-times closeBtn"></i>
                                </h4>
                                @if($project->leaderboard_setting?->description)
                                    <div class="drawer-desc">
                                        {{ $project->leaderboard_setting?->description }}
                                    </div>
                                @endif
                                <div class="drawer-content">
                                    @if($user->leaderboard_score)
                                    <div class="my-score-item">
                                        <label>Your Score:</label> 
                                        <p>{{ $user->leaderboard_score->score }} points</p>
                                    </div>
                                    @endif
                                    <h5>Top 10</h5>
                                    <ul class="leaderboard-list">
                                        @foreach ($project->leaderboard(10) as $leaderboardUser)
                                            <li class='leaderboard-item'>
                                                <div class="leaderboard-item__badge">
                                                    @if($loop->index < 3)
                                                        <i class="las la-award top3"></i>
                                                    @else
                                                        <span>{!! $loop->index + 1 !!}</span>
                                                    @endif
                                                </div>
                                                <p class="name" >{{ $leaderboardUser->name }} {{ $leaderboardUser->surname }}</p>
                                                <span class="score">{{ $leaderboardUser->score }} points</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </section>
            
            @if($project->getBlock('HEADER_ADDITIONAL_CONTENT'))
            <div class="additional-header-content">
                {!! $project->getBlock('HEADER_ADDITIONAL_CONTENT') !!}
            </div>
            @endif
            

            <div class="inner container">
    
            @if ($project->isLoggedIn() && !request()->routeIs('users.thanks') && count($project->navList()))
                <!-- burger button for responsive nav menu -->
                <button class="primary-menu__toggle">
                    <span class="material-symbols-outlined">menu</span>
                </button>
            @endif
    
            <!-- logo -->
            <div class="logo-container">
                @if ($branding->logo)
                    <a target="_blank" href="{{ $branding->website ?? $project->full_url }}">
                        <img class="site-head__logo" src="{{ storage_url($branding->logo) }}" alt="Logo"/>
                    </a>
                @elseif($project->getBlock('MULTIPLE_BRANDING_LOGOS'))
                    {!! $project->getBlock('MULTIPLE_BRANDING_LOGOS') !!}
                @else
                    <a target="_blank" href="{{ $branding->website ?? $project->full_url }}">
                        {{ $project->name }}
                    </a>
                @endif
            </div>
    
            <!-- Sponsor Carousel -->
            @if ($project->getBlock('SPONSORS_CAROUSEL'))
                <div class="site-head__sponsors-container{{ (!$project->isLoggedIn() || !count($project->navList())) || $project->type->id !== 2 ? " float-right" : " " }}">
                    {!! $project->getBlock('SPONSORS_CAROUSEL') !!}
                </div>
            @elseif($project->events()->published()->whereHas('event_page_type', fn($q) => $q->where('name', 'Sponsor Page'))->whereNotNull('img')->count())
                    <div class="site-head__sponsors-container{{ (!$project->isLoggedIn() || !count($project->navList())) || $project->type->id !== 2 ? " float-right" : " " }}">
                        <p class="site-head__sponsors-by-text">
                                {!! $project->getBlock('SPONSOR_CAROUSEL_SUBTITLE') ?: 'Sponsored By' !!}
                        </p>
                        <div class="site-head__sponsors">
                            <div class="site-head__sponsors__track">
                                @foreach ($project->events()->published()->whereHas('event_page_type', fn($q) => $q->where('name', 'Sponsor Page'))->whereNotNull('img')->get() as $headerSponsor)
                                    <a 
                                        href="{{ $project->isLoggedIn() ? $headerSponsor->full_url : ($headerSponsor->external_url ?: $headerSponsor->full_url) }}" class="site-head__sponsor-link"
                                        @if( !$project->isLoggedIn())
                                        target="_blank"
                                        @endif
                                    >
                                        <img alt="{{ $headerSponsor->name }}" src="{{ storage_url($headerSponsor->img) }}"/>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
            @endif
    
                {{-- If user is logged in, show navigations --}}
                @if ($project->isLoggedIn() && !request()->routeIs('users.thanks') && count($project->navList()))
                    <!-- navigation menu -->
                        <nav class="primary-menu">
                            <div class="overlay"></div>
                            <ul>
                                <li class="menu-item closeBtn">
                                    <span class="material-symbols-outlined closeIcon">close</span>
                                </li>
    
                                @foreach ($project->navList() as $item)
                                    <li class="menu-item{{ $item->event && $item->event->url === last_segment() ? ' active' : '' }}">
                                        <a id="{{ $item->id }}" class="menu-link {{ $item->class }}"
                                           href="{{ $item->event ? $item->event->full_url : $item->link() }}"
                                                {!! $item->new_tab ? 'target="_blank"' : '' !!}>{{ $item->title }}</a>
                                        @if (!empty($item->children))
                                            <ul class="sub-menu-container">
                                                @foreach ($item->children as $child)
                                                    <li class="menu-item">
                                                        <a id="{{ $child->id }}"
                                                           class="menu-link {{$child->class }}"
                                                           href="{{ $child->event ? $child->event->full_url : $child->link() }}"
                                                                {!! $child->new_tab ? 'target="_blank"' : '' !!}>{{ $child->title }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </nav>
                        <!-- end of navigation menu -->
                @endif
    
            </div>
        </header>
    @endif
    <main>
        <section class="container">
            <section class="columns is-desktop">
                <section class="column">
    @php $current_section = 'main'; @endphp