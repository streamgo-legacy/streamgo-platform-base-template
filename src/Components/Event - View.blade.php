<section id="video-section" class="container">
    <div id="video-player">
        <div class="player-container" oncontextmenu="return false;" style="position:relative;">
            <!-- Import streamGo Video Player -->
            {!! $player !!}

            <!-- Ratings For Events -->
            @if($project->ratings && $event->event_type !== 'on-demand')
            <div id="ratings-container" class="ratings-container"></div>

            <div id="m-ratings-container" class="m-ratings-container">
                <div id="m-rating-btn" class="m-rating-btn">
                    <div class="current-rating">
                        <img class="current-rating-img" />
                    </div>
                    <span class="btnText">Tap to React</span>
                </div>
                <div class="m-ratings-holders">
                    <div class="m-ratings-holders-text">Tap to React</div>
                    <div class="m-ratings-holders-icons">

                    </div>
                </div>
            </div>

            <template class="ratings-item-template">
                <button class='ratings-item'>
                    <img class='rating-img' />
                    <i class="fa fa-check-circle selected-icon"></i>
                    <div class="ratings-tooltip">
                        <div class="tooltip-arrow"></div>
                        <span class="rating-name"></span>
                        <span class="rating-count">0</span>
                    </div>
                </button>
            </template>

            <template class="m-ratings-item-template">
                <button class="m-rating-item-btn">
                    <img class="m-rating-item-img" />
                </button>
            </template>
            @endif
            <!-- End of Ratings For Events -->

            <!-- Import Global Settings Alerts -->
            {!! $global_alert !!}
            <!-- Import Global Settings Polls -->
            {!! $global_poll !!}
        </div>

        <!-- Import Global Settings Questions -->
        {!! $global_questions !!}
    </div>
</section>