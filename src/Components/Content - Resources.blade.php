@if ($project->getBlock('resources'))
    <section class="resources-container container custom-resources">
        <div class="title-wrapper">
            <h4 class="resources-title">
                {{ request()->is($project->url . '/thanks') ? ($project->getBlock('RESOURCES_TITLE_THANKS') ?: 'Resource') : ($project->getBlock('RESOURCES_TITLE') ?: 'Resource') }}
            </h4>
            <span class="title-undeline"></span>
        </div>
        <div class="resources-downloads">
            {!! $project->getBlock('RESOURCES') !!}
        </div>
    </section>
@else
    @if (isset($event) && $event->resources && $event->resources->count())
        <section class="resources-container container auto-resources">
            <div class="title-wrapper">
                <h4 class="resources-title">
                    {{ request()->is($project->url . '/thanks') ? ($project->getBlock('RESOURCES_TITLE_THANKS') ?: 'Resource') : ($project->getBlock('RESOURCES_TITLE') ?: 'Resource') }}
                </h4>
                <span class="title-undeline"></span>
            </div>

            <div class="resources-downloads">
                @foreach ($event->resources as $resource)
                    <a href="{!! isset($resource->data->path) && storage_file_exists($resource->data->path)
                        ? storage_url($resource->data->path)
                        : $resource->data->path !!}" rel="noopener" target="_blank" class='resources-item'>

                        @if ($resource->placeholder)
                            <picture class="resource-icon">
                                <source loading="lazy" decoding="async"
                                    srcset="{!! storage_url($resource->placeholder->path) !!}"
                                    alt="{!! $resource->placeholder->name !!}" />
                                <img loading="lazy" decoding="async"
                                    src="{!! storage_url($resource->placeholder->path) !!}"
                                    alt="{!! $resource->placeholder->name !!}" />
                            </picture>
                        @else
                            @switch($resource->event_resource_type_id)
                                @case(\App\Models\EventResourceTypes::TYPES['PDF'])
                                    @php
                                        $imgSrc = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/files/002-pdf.png';
                                    @endphp
                                @break

                                @case(\App\Models\EventResourceTypes::TYPES['IMAGE'])
                                    @php
                                        $imgSrc = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/files/002-pdf.png';
                                    @endphp
                                @break

                                @case(\App\Models\EventResourceTypes::TYPES['VIDEO'])
                                    @php
                                        $imgSrc = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/files/004-browser.png';
                                    @endphp
                                @break

                                @case(\App\Models\EventResourceTypes::TYPES['PODCAST'])
                                    @php
                                        $imgSrc = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/files/click.png';
                                    @endphp
                                @break

                                @case(\App\Models\EventResourceTypes::TYPES['WEBSITE'])
                                    @php
                                        $imgSrc = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/files/click.png';
                                    @endphp
                                @break
                            @endswitch
                            <picture class="resource-icon">
                                <source loading="lazy" decoding="async"
                                    srcset="@php echo($imgSrc) @endphp"
                                    alt="{!! $resource->event_resource_type_id !!}" />
                                <img loading="lazy" decoding="async"
                                    src="@php echo($imgSrc) @endphp"
                                    alt="{!! $resource->event_resource_type_id !!}" width="75" height="75">
                            </picture>
                        @endif

                        {{-- @isset($resource->placeholder->path)
                            <span class="resource-icon">
                                <img src="{!! storage_url($resource->placeholder->path) !!}">
                            </span>
                        @endisset --}}
                        <span class="resource-name">{!! $resource->name !!}</span>
                    </a>
                @endforeach
            </div>

        </section>
    @endif
@endif