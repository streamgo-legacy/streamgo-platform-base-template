@if ($project->feeds()->count())
    <section class="social-feed-container"
        data-feed-base-url="{{ route('feed.single', [$project->url, $project->feeds()->first()->id]) }}"
        data-feed-id="{{ $project->feeds()->first()->id }}">
    </section>
@endif
