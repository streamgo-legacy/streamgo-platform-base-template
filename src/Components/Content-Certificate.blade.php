<div class="container cpd-container">
    <h4>{!! $project->getBlock('CPD_TITLE') ?: 'My Certificates' !!}</h4>

    @if ($project->getBlock('CPD_DESC'))
        <div class="cpd-desc">
            {!! $project->getBlock('CPD_DESC') !!}
        </div>
    @endif

    <div class="summary">
        <div class="summary__header">
            <h5 class="title">Summary</h5>
            <button class="certificate-refresh" onclick="window.location.reload()">
                <span class="material-symbols-outlined">refresh</span>
                Refresh Progress
            </button>
        </div>
        <div class="summary__details">
            <p>Progress: {{ $totalCpdPassed }} /{{ $totalCpdLength }} {{ plural('min', $totalCpdLength) }}</p>
            @if ($project->cpdSetting->single_certification && count($passedEvents) && $totalCpdPassed >= $project->cpdSetting->minimum_minutes_watched)
                <a target="_blank" class="certificate-link button" href="{{ route('event.certificate', [$project->url, $passedEvents->first()->url]) }}">
                    <b>Download certificate</b>
                </a>
            @endif
        </div>
    </div>

    @if (true)
        <h5>Session Progress</h5>
        <ul class="event-progress-list">
            {{-- header for list --}}
            <li class="cdn-item item-header">
                <p class="name col">Session</p>
                <p class="length col">Length</p>
                <p class="status col">Status</p>
                <p class="viewed col">Viewed</p>
                <p class="cpd-poll col">Polls</p>
                <p class="cpd-poll col">Correct Polls</p>
                <p class="certificate-link col">Certificates</p>
            </li>

            @foreach ($passedEvents as $event)
            <script>
                console.log(`cpd data for event: {{ $event->name }}`, {!! $event->cpd->toJson() !!});
                @if($event->userCpd)
                console.log(`cpd user data for event: {{ $event->name }}`, {!! $event->userCpd->toJson() !!});
                @endif
            </script>
                <li class="cdn-item passed-event">
                    <div class="name col">
                        <a href="{{ $event->full_url }}">{{ $event->name }}</a>
                    </div>
                    <div class="length col">
                        <label class="col__label">Length</label>
                        <p>{{ $event->cpd->cpd_length }} {{ plural('min', $event->cpd->cpd_length) }}</p>
                    </div>
                    <div class="status col">
                        <svg title="Passed" xmlns="http://www.w3.org/2000/svg" class="cpd-icon" viewBox="0 0 20 20"
                            fill="#2ECC40">
                            <path fill-rule="evenodd"
                                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                    @isset($event->cpd->rules->percent_viewed)
                        <div class="viewed col">
                            <label class="col__label">Viewed</label>
                            <p>{{ round($event->userCpd->results?->event_viewed_percent ?? 0) }}% Viewed</p>
                        </div>
                    @endisset

                    @isset($event->cpd->rules->polls_answered)
                        <div class=" cpd-poll poll-submission col">
                            <label class="col__label">Polls</label>
                            <p>{{ round($event->userCpd->results?->polls_answered ?? 0) }}/{{ $event->cpd->rules->polls_answered ?? 0 }}</p>
                        </div>
                    @endisset

                    @isset($event->cpd->rules->polls_pass_mark)
                        <div class="cpd-poll poll-pass-mark col">
                            <label class="col__label">Correct Polls</label>
                            <p>{{ round($event->userCpd->results?->polls_answered_correctly_percent ?? 0) }}% / {{ $event->cpd->rules->polls_pass_mark ?? 0 }}%</p>
                            @if($event->polls->isNotEmpty())
                                <button type="button" class="expand-polls-button btn btn-primary" data-event-id="{{ $event->id }}">
                                    Expand
                                </button>
                            @endif
                        </div>
                    @endisset

                    @if (!$project->cpdSetting->single_certification)
                        <a target="_blank" class="certificate-link button col" href="{{ route('event.certificate', [$project->url, $event->url]) }}">
                            <b>View certificate</b>
                        </a>
                    @else
                        <button class="certificate-link completed button col" disabled>
                            <span class="material-symbols-outlined">done</span>
                            <b>Completed</b>
                        </button>
                    @endif

                    @if($event->polls->isNotEmpty())
                        <div class="poll-container-row hide" data-event-id="{{ $event->id }}">
                            <div class="poll-container-item poll-container-header">
                                <h4>Poll</h4><h4>Pass Status</h4><h4>Retake Poll</h4>
                            </div>
                            @foreach($event->polls as $poll)
                            <div 
                                class="poll-container-item"
                                data-poll-json="{{ json_encode($poll->prepareForFrontend(), JSON_THROW_ON_ERROR) }}"
                            >
                                <div>
                                    <label>Poll</label>
                                    <p>{{ $poll->label }}</p>
                                </div>
                                <div>
                                    <label>Pass Status</label>
                                    @if($pollPassStatuses->get($poll->id))
                                        <svg title="Passed" xmlns="http://www.w3.org/2000/svg" class="cpd-icon" viewBox="0 0 20 20"
                                            fill="#2ECC40">
                                            <path fill-rule="evenodd"
                                                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    @else
                                        <svg title="Not yet passed" xmlns="http://www.w3.org/2000/svg" class="cpd-icon"
                                            viewBox="0 0 20 20" fill="#FF4136">
                                            <path fill-rule="evenodd"
                                                d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    @endif
                                </div>
                                <div>
                                    @if($event->cpd->attendee_can_retake_poll && $pollPassStatuses->get($poll->id) === false)
                                    <table>
                                        <tbody>
                                            <tr data-poll-json="{{ json_encode($poll->prepareForFrontend(), JSON_THROW_ON_ERROR) }}">
                                                <td>
                                                    <a class="retake-poll btn btn-primary button secondary" href="#">
                                                        <small>Retake Poll</small>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </div>
                    @endif
                </li>
            @endforeach

            @foreach ($otherEvents as $event)
                <script>
                    console.log(`cpd data for event: {{ $event->name }}`, {!! $event->cpd->toJson() !!});
                    @if($event->userCpd)
                    console.log(`cpd user data for event: {{ $event->name }}`, {!! $event->userCpd->toJson() !!});
                    @endif
                </script>
                <li class="cdn-item other-event">
                    <div class="name col">
                        <a href="{{ $event->full_url }}">{{ $event->name }}</a>
                    </div>
                    <div class="length col">
                        <label class="col__label">Length</label>
                        <p>{{ $event->cpd->cpd_length }} {{ plural('min', $event->cpd->cpd_length) }}</p>
                    </div>
                    <div class="status col">
                        <svg title="Not yet passed" xmlns="http://www.w3.org/2000/svg" class="cpd-icon"
                            viewBox="0 0 20 20" fill="#FF4136">
                            <path fill-rule="evenodd"
                                d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>

                    @isset($event->cpd->rules->percent_viewed)
                        <div class="viewed col">
                            <label class="col__label">Viewed</label>
                            <p>
                                <span class='viewed-percent'>{{ round($event->userCpd->results?->event_viewed_percent ?? 0) }}% Viewed</span>
                                <span class="required">{{ $event->cpd->rules->percent_viewed }}% required</span>
                            </p>
                        </div>
                    @endisset

                    @isset($event->cpd->rules->polls_answered)
                        <div class=" cpd-poll poll-submission col">
                            <label class="col__label">Polls</label>
                            <p>{{ round($event->userCpd->results?->polls_answered ?? 0) }}/{{ $event->cpd->rules->polls_answered ?? 0 }}</p>
                        </div>
                    @endisset

                    @isset($event->cpd->rules->polls_pass_mark)
                        <div class="cpd-poll poll-pass-mark col">
                            <label class="col__label">Correct Polls</label>
                            <p>{{ round($event->userCpd->results?->polls_answered_correctly_percent ?? 0) }}% / {{ $event->cpd->rules->polls_pass_mark ?? 0 }}%</p>
                            @if($event->polls->isNotEmpty())
                                <button type="button" class="expand-polls-button btn btn-primary" data-event-id="{{ $event->id }}">
                                    Expand
                                </button>
                            @endif
                        </div>
                    @endisset

                    <button class="certificate-link in-progress button col" disabled>
                        <b>In Progress</b>
                    </button>

                    @if($event->polls->isNotEmpty())
                    <div class="poll-container-row hide" data-event-id="{{ $event->id }}">
                        <div class="poll-container-item poll-container-header">
                            <h4>Poll</h4><h4>Pass Status</h4><h4>Retake Poll</h4>
                        </div>
                        @foreach($event->polls as $poll)
                        <div 
                            class="poll-container-item"
                            data-poll-json="{{ json_encode($poll->prepareForFrontend(), JSON_THROW_ON_ERROR) }}"
                        >
                            <div>
                                <label>Poll</label>
                                <p>{{ $poll->label }}</p>
                            </div>
                            <div>
                                <label>Pass Status</label>
                                @if($pollPassStatuses->get($poll->id))
                                    <svg title="Passed" xmlns="http://www.w3.org/2000/svg" class="cpd-icon" viewBox="0 0 20 20"
                                        fill="#2ECC40">
                                        <path fill-rule="evenodd"
                                            d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                            clip-rule="evenodd" />
                                    </svg>
                                @else
                                    <svg title="Not yet passed" xmlns="http://www.w3.org/2000/svg" class="cpd-icon"
                                        viewBox="0 0 20 20" fill="#FF4136">
                                        <path fill-rule="evenodd"
                                            d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                                            clip-rule="evenodd" />
                                    </svg>
                                @endif
                            </div>
                            <div>
                                @if($event->cpd->attendee_can_retake_poll && $pollPassStatuses->get($poll->id) === false)
                                <table>
                                    <tbody>
                                        <tr data-poll-json="{{ json_encode($poll->prepareForFrontend(), JSON_THROW_ON_ERROR) }}">
                                            <td>
                                                <a class="retake-poll btn btn-primary button secondary" href="#">
                                                    <small>Retake Poll</small>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                @endif
                </li>
            @endforeach
        </ul>

        <div class="poll__container"></div>
        {{-- Start: Poll form template --}}
        <template class="poll-form-template">
            <div class="poll registration-form poll__form-container">
                <form class="poll__form" method="post">
                    @csrf
                    <label></label>
                    <div class="poll__inputs"></div>
                    <button>Submit</button>
                </form>
            </div>
        </template>

        <template class="poll-radio">
            <label><input type="radio" name="poll" value=""></label>
        </template>

        <template class="poll-checkbox">
            <label><input type="checkbox" name="poll" value=""></label>
        </template>

        <template class="poll-text">
            <input type="text" name="poll">
        </template>

        <template class="poll-textarea">
            <input type="text" name="poll">
        </template>
        {{-- End: Poll form template --}}
    @endif
</div>