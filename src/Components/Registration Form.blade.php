<style>
    .hidden {
        display: none !important;
    }
</style>

<section id="register" class="form-container {!! $branding->bg_img ? "" : "no-bg" !!}">

    @if ($project->getBlock('REGISTRATION_TEXT'))
        <h4 class="form-title">{!! $project->getBlock('REGISTRATION_TEXT') !!}</h4>
    @else
        <h4 class="form-title">Register</h4>
    @endif

    <form action="{!! route('users.create', $project->url) !!}" class="register form" method="post">

        @csrf

        @if (session()->has('forced_logout'))
            <div class="reg-error" style="background: rgba(237,41,57,.2)">
                <h4>You have been logged out</h4>
                <p>
                    Another user has entered the event using your credentials.
                    Only one user is permitted access per set of credentials.
                    Please ensure you have not shared your access to this event.
                </p>
            </div>
        @endif

        @if ($errors->any())
            <div class="reg-error" style="background: rgba(237,41,57,.2)">
                <h4>Error:</h4>
                <p><b>{!! $errors->first() !!}</b></p>
                <p>If you continue to receive an error during registration, please contact the live chat support on the
                    bottom right of the page.</p>
            </div>
        @endif

        @if ($project->bot_protection)
            <div class="form__group" style="display:none">
                <label for="full-name" class="label">Full Name</label>
                <input class="input" type="text" name="full_name" id="full-name" placeholder="Enter your full name"
                    autocomplete="off">
            </div>
        @endif

        <div class="form__group">
            <label class="required label" for="name">First Name</label>
            <input class="input" type="text" name="name" id="name" placeholder="Enter your first name"
                value="{!! old('name') !!}" required>
        </div>

        <div class="form__group">
            <label class="required label" for="surname">Last Name</label>
            <input class="input" type="text" name="surname" id="surname" placeholder="Enter your last name"
                value="{!! old('surname') !!}" required>
        </div>

        <div class="form__group">
            <label class="required label" for="email">Email</label>
            <input class="input" type="email" name="email" id="email" placeholder="Enter your email address"
                value="{!! old('email') !!}" required>
        </div>

        @if (isset($additionalProjects) && !$additionalProjects->isEmpty())

            @if ($project->getBlock('Multi-reg Text'))
                {!! $project->getBlock('Multi-reg Text') !!}
            @else
                <p>Do you also want to register for the following events?</p>
            @endif

            @foreach ($additionalProjects as $additionalProject)
                <div class="form__group form__group--checkbox">
                    <div class="linked-event">
                        <label class="label" for="input-register-{!! $additionalProject->id !!}">
                            @if ($additionalProject->content_blocks->firstWhere('reference', 'PROJECT_TITLE') !== null)
                                <span>{!! $additionalProject->content_blocks->firstWhere('reference', 'PROJECT_TITLE')->content !!}</span><br />
                            @else
                                <span>{!! $additionalProject->name !!}</span><br />
                            @endif
                            {!! $additionalProject->start_time->format('jS F Y') !!} -
                            {!! $additionalProject->start_time->format('G:i') !!} ({!! $additionalProject->timezone !!})
                        </label>
                        <input class="input" type="checkbox" class="additional-event-choice"
                            name="additional_events[{!! $additionalProject->id !!}]"
                            id="input-register-{!! $additionalProject->id !!}" value="1"
                            data-project-id="{!! $additionalProject->id !!}"
                            {!! old('additional_events.' . $additionalProject->id) === '1' ? ' checked' : '' !!}>
                    </div>
                </div>
            @endforeach

        @endif

        <div class="additional-registration-fields">

            @foreach ($fields as $field)
                @switch($field['field']->type)
                    @case('text')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}-0">{!! $field['field']->label !!}</label>
                            <input placeholder="Enter your {!! $field['field']->label !!}" type="text"
                                class="additional-registration-field__input input"
                                name="custom_input[{!! $field['field']->ref !!}]"
                                value="{!! old("custom_input.{$field['field']->ref}") !!}"
                                id="input-{!! $field['field']->ref !!}"{!! $field['field']->required ? ' data-required' : '' !!}>
                        </div>
                    @break

                    @case('question')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}">{!! $field['field']->label !!}</label>
                            <textarea class="additional-registration-field__input textarea" name="custom_input[{!! $field['field']->ref !!}]"
                                id="input-{!! $field['field']->ref !!}"{!! $field['field']->required ? ' data-required' : '' !!}>{!! old("custom_input.{$field['field']->ref}") !!}</textarea>
                        </div>
                    @break

                    @case('textarea')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}">{!! $field['field']->label !!}</label>
                            <textarea class="additional-registration-field__input textarea" name="custom_input[{!! $field['field']->ref !!}]"
                                id="input-{!! $field['field']->ref !!}"{!! $field['field']->required ? ' data-required' : '' !!}>{!! old("custom_input.{$field['field']->ref}") !!}</textarea>
                        </div>
                    @break

                    @case('radio')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}-0">{!! $field['field']->label !!}</label>
                                <div class="radios">
                                    @foreach (explode('|', $field['field']->value) as $value)
                                        <label for="input-{!! $field['field']->ref !!}-{!! $loop->index !!}" class="radio" >
                                            <input type="radio"
                                                class="additional-registration-field__input"
                                                name="custom_input[{!! $field['field']->ref !!}]"
                                                id="input-{!! $field['field']->ref !!}-{!! $loop->index !!}"
                                                value="{!! $value !!}"{!! old("custom_input.{$field['field']->ref}") == $value ? ' checked' : '' !!}{!! $field['field']->required ? ' data-required' : '' !!}
                                            />
                                            {!! $value !!}
                                        </label>
                                    @endforeach
                                </div>
                        </div>
                    @break

                    @case('checkbox')
                        <div class="form__group form__group--checkbox{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!} form__group-{{ $field['field']->ref }}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="label" for="input-{!! $field['field']->ref !!}-0">{!! $field['field']->label !!}</label>
                            <div class="{!! $field['field']->required ? 'required' : '' !!} checkboxes">
                                @foreach (explode('|', $field['field']->value) as $value)
                                    <div class="checkbox-wrapper">
                                        <label for="input-{!! $field['field']->ref !!}-{!! $loop->index !!}" class="checkbox">
                                            <input type="checkbox" class="additional-registration-field__input"
                                                name="custom_input[{!! $field['field']->ref !!}]"{!! $loop->index !!}]"
                                                id="input-{!! $field['field']->ref !!}-{!! $loop->index !!}"
                                                value="{!! $value !!}"{!! old("custom_input.{$field['field']->ref}.{$loop->index}") == $value ? ' checked' : '' !!} 
                                            />
                                            {!! $value !!}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @break

                    @case('networking')
                        <div class="form__group form__group--checkbox{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <div class="{!! $field['field']->required ? 'required' : '' !!}">
                                <label for="input-{!! $field['field']->ref !!} checkbox">
                                    <input type="checkbox" class="additional-registration-field__input"
                                    name="custom_input[{!! $field['field']->ref !!}]" id="input-{!! $field['field']->ref !!}"
                                    value="1"{!! old("custom_input.{$field['field']->ref}") == '1' ? ' checked' : '' !!}>
                                    {!! $field['field']->label !!}
                                </label>
                                
                            </div>
                        </div>
                    @break

                    @case('select')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!} form__group-{{ $field['field']->ref }}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}"
                                class="col-sm-4 control-label"
                            >{!! $field['field']->label !!}
                            </label>
                            <div class="select">
                                <select class="" id="input-{!! $field['field']->ref !!}"
                                    class="additional-registration-field__input"
                                    name="custom_input[{!! $field['field']->ref !!}]"{!! $field['field']->required ? ' data-required' : '' !!}
                                >
                                    <option
                                        value
                                        selected
                                        disabled
                                    >Please select</option>
                                    @foreach (explode('|', $field['field']->value) as $value)
                                        @if(strtolower($value) !== 'please select')
                                        <option
                                            value="{{ $value }}"
                                            {!! old("custom_input.{$field['field']->ref}") == $value ? ' selected' : '' !!}
                                        >
                                            {{ $value }}
                                        </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @break

                    @case('countries')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}"
                                class="col-sm-4 control-label">{!! $field['field']->label !!}
                            </label>
                            <select 
                                class="js-select2"
                                id="input-{!! $field['field']->ref !!}"
                                class="additional-registration-field__input"
                                name="custom_input[{!! $field['field']->ref !!}]"{!! $field['field']->required ? ' data-required' : '' !!}
                                data-select2-search-placeholder="Type your country..."
                            >
                                @foreach (config('countries') as $value)
                                    <option
                                        value="{!! strtolower($value) !== 'please select' ? $value : '' !!}"{!! old("custom_input.{$field['field']->ref}") == $value ? ' selected' : '' !!}>
                                        {!! $value !!}</option>
                                @endforeach
                            </select>
                        </div>
                    @break

                    @case('content')
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label for="input-{!! $field['field']->ref !!}" class="label" >{!! $field['field']->label !!}</label>
                            <div>{!! $field['field']->value !!}</div>
                        </div>
                    @break

                    @case('hidden')
                        <input type="hidden" name="custom_input[{!! $field['field']->ref !!}]"
                            id="input-{!! $field['field']->ref !!}" value="{!! $field['field']->value !!}">
                    @break

                    @case('ticket_type')
                        <div class="form__group ticket-types__row{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!}"
                                for="input-{!! $field['field']->ref !!}"
                                class="col-sm-4 control-label">{!! $field['field']->label !!}
                            </label>
                            @if($field['field']->values->count() > 1)
                            <select class="js-select2 ticket-type-select" id="input-{!! $field['field']->ref !!}"
                                class="additional-registration-field__input" name="custom_input[{!! $field['field']->ref !!}]"
                                {!! $field['field']->required ? ' data-required' : '' !!}>
                                @foreach ($field['field']->values as $ticketTypeOption)
                                    <option
                                        {!! optional($ticketTypeOption->ticket)->is_paid ? 'data-paid="true" data-cost="' . $ticketTypeOption->ticket->price_formatted . '" ' : '' !!}

                                        value="{!! strtolower($ticketTypeOption->value) !== 'please select' ? $ticketTypeOption->value : '' !!}"
                                        {!! old("custom_input.{$field['field']->ref}", request('ticket')) == $ticketTypeOption->value ? ' selected' : '' !!}
                                    >
                                        {!! $ticketTypeOption->value !!}
                                        {!! optional($ticketTypeOption->ticket)->is_paid ? '(' . $ticketTypeOption->ticket->price_formatted . ')' : '' !!}
                                    </option>
                                @endforeach
                            </select>
                            @else
                                @foreach ($field['field']->values as $ticketTypeOption)
                                <input 
                                    type="hidden"
                                    id="input-{{ $field['field']->ref }}"
                                    name="custom_input[{{ $field['field']->ref }}]"
                                    value="{{ $ticketTypeOption->value }}"
                                    {!! $field['field']->required ? ' data-required' : '' !!}
                                />
                                <span class="tag medium price-tag">
                                    {{ $ticketTypeOption->value }}
                                    {{ optional($ticketTypeOption->ticket)->is_paid ? '(' . $ticketTypeOption->ticket->price_formatted . ')' : '' }}
                                </span>
                                @endforeach
                            @endif
                        </div>
                    @break

                    @default
                        <div class="form__group{!! !$additionalProjects->isEmpty() && !in_array($project->id, $field['projects']) ? ' hidden' : '' !!}"
                            data-show-for="{!! implode(',', $field['projects']) !!}">
                            <label class="{!! $field['field']->required ? 'required' : '' !!} label"
                                for="input-{!! $field['field']->ref !!}">{!! $field['field']->label !!}</label>
                            <input placeholder="Enter your {!! $field['field']->label !!}"
                                class="{!! $field['field']->type === 'number' || $field['field']->type === 'tel' ? 'input' : $field['field']->type !!}"
                                type="{!! $field['field']->type !!}" name="custom_input[{!! $field['field']->ref !!}]"
                                value="{!! old("custom_input.{$field['field']->ref}") !!}"
                                id="input-{!! $field['field']->ref !!}"{!! $field['field']->required ? ' data-required' : '' !!}>
                        </div>
                @endswitch
            @endforeach

        </div>

        @if ($project->getBlock('cookie_policy'))
            {!! $project->getBlock('cookie_policy') !!}
        @else
            <small class="cookie-policy">Our registration process uses cookies, by submitting this registration form you agree to
                our <a href="https://streamgo.co.uk/cookies" rel="noopener" target="_blank">cookie policy</a>.</small>
        @endif

        @if($price = collect($fields)->firstWhere('field.type', 'ticket_type')['field']['values'][0]->ticket?->price_formatted ?? null)
            <button class="button primary" type="submit">Pay {{ $price }} & Register</button>
        @else
            <button class="button primary" type="submit">Register</button>
        @endif

        <small class="require-tip">* Required Fields</small>

    </form>
</section>