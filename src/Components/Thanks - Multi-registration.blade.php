@if (isset($additionalProjects) && ! $additionalProjects->isEmpty() && (auth('web')->user() || session()->has('unauthenticated_user')))
    <style>
        .hidden {
            display: none;
        }
    </style>
    <div id="multiReg" class="form-container spacing spacing--bottom-only">
        <form action="{{ route('users.create.additional', $project->url) }}" class="register form" method="post">

            @csrf

            <p>
                Do you also want to register for the following events?
            </p>

            @foreach ($additionalProjects as $additionalProject)
                <div class="form__group form__group--checkbox">
                    <div class="linked-event">
                        <label for="input-register-{{ $additionalProject->id }}">
                            <span>{{ $additionalProject->name }}</span><br/>{{ $additionalProject->start_time->format('jS F Y') }} - {{ $additionalProject->start_time->format('G:i') }} ({{ $additionalProject->timezone }})
                        </label>
                        <input type="checkbox" class="additional-event-choice" name="additional_events[{{ $additionalProject->id }}]" id="input-register-{{ $additionalProject->id }}" value="1" data-project-id="{{ $additionalProject->id }}">
                    </div>
                </div>
            @endforeach
            <div class="additional-registration-fields">
                @foreach($fields AS $field)
                    @switch($field['field']->type)
                        @case('text')
                        <div class="form__group{{ ! in_array($project->id, $field['projects']) ? ' hidden' : '' }}" data-show-for="{{ implode(',', $field['projects']) }}">
                            <label class="{{ $field['field']->required ? 'required' : '' }}" for="input-{{ $field['field']->ref }}-0">{!! $field['field']->label !!}</label>
                            <input placeholder="Enter your {{ $field['field']->label }}" type="text" class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]" value="{{ old("custom_input.{$field['field']->ref}") }}" id="input-{{ $field['field']->ref }}"{{ $field['field']->required ? ' data-required' : '' }}>
                        </div>
                        @break

                        @case('question')
                        <div class="form__group{{ ! in_array($project->id, $field['projects']) ? ' hidden' : '' }}" data-show-for="{{ implode(',', $field['projects']) }}">
                            <label class="{{ $field['field']->required ? 'required' : '' }}" for="input-{{ $field['field']->ref }}">{!! $field['field']->label !!}</label>
                            <textarea class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]" id="input-{{ $field['field']->ref }}"{{ $field['field']->required ? ' data-required' : '' }}>{{ old("custom_input.{$field['field']->ref}") }}</textarea>
                        </div>
                        @break

                        @case('textarea')
                        <div class="form__group{{ ! in_array($project->id, $field['projects']) ? ' hidden' : '' }}" data-show-for="{{ implode(',', $field['projects']) }}">
                            <label class="{{ $field['field']->required ? 'required' : '' }}" for="input-{{ $field['field']->ref }}">{!! $field['field']->label !!}</label>
                            <textarea class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]" id="input-{{ $field['field']->ref }}"{{ $field['field']->required ? ' data-required' : '' }}>{{ old("custom_input.{$field['field']->ref}") }}</textarea>
                        </div>
                        @break

                        @case('radio')
                        <div class="form__group{{ ! in_array($project->id, $field['projects']) ? ' hidden' : '' }}" data-show-for="{{ implode(',', $field['projects']) }}">
                            <label class="{{ $field['field']->required ? 'required' : '' }}" for="input-{{ $field['field']->ref }}-0">{!! $field['field']->label !!}</label>
                            @foreach(explode('|', $field['field']->value) AS $value)
                                <label for="input-{{ $field['field']->ref }}-{{ $loop->index }}"><input type="radio" class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]" id="input-{{ $field['field']->ref }}-{{ $loop->index }}" value="{{ $value }}"{{ old("custom_input.{$field['field']->ref}") == $value ? ' checked' : '' }}{{ $field['field']->required ? ' data-required' : '' }}> {{ $value }}</label>
                            @endforeach
                        </div>
                        @break

                        @case('checkbox')
                        <div class="form__group form__group! --checkbox{{ in_array($project->id, $field['projects']) ? ' hidden' : '' }}">
                            <div class="{{ $field['field']->required ? 'required' : '' }}">
                                <label for="input-{{ $field['field']->ref }}-0">{!! $field['field']->label !!}</label>
                                @foreach(explode('|', $field['field']->value) AS $value)
                                    <input type="checkbox" class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]"{{ $loop->index }}]" id="input-{{ $field['field']->ref }}-{{ $loop->index }}" value="{{ $value }}"{{ old("custom_input.{$field['field']->ref}.{$loop->index}") == $value ? ' checked' : '' }}><label for="input-{{ $field['field']->ref }}-{{ $loop->index }}"> {{ $value }}</label>
                                @endforeach
                            </div>
                        </div>
                        @break

                        @case('networking')
                        <div class="form__group form__group! --checkbox{{ in_array($project->id, $field['projects']) ? ' hidden' : '' }}">
                            <div class="{{ $field['field']->required ? 'required' : '' }}">
                                <label for="input-{{ $field['field']->ref }}">{!! $field['field']->label !!}</label>
                                <input type="checkbox" class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]" id="input-{{ $field['field']->ref }}" value="1"{{ old("custom_input.{$field['field']->ref}") == '1' ? ' checked' : '' }}>
                            </div>
                        </div>
                        @break

                        @case('select')
                        <div class="form__group{{ ! in_array($project->id, $field['projects']) ? ' hidden' : '' }}" data-show-for="{{ implode(',', $field['projects']) }}">
                            <label class="{{ $field['field']->required ? 'required' : '' }}" for="input-{{ $field['field']->ref }}" class="col-sm-4 control-label">{!! $field['field']->label !!}</label>
                            <select class="js-select2" id="input-{{ $field['field']->ref }}" class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]"{{ $field['field']->required ? ' data-required' : '' }}>
                                @foreach(explode('|', $field['field']->value) AS $value)
                                    <option value="{{ strtolower($value) !== 'please select' ? $value : '' }}"{{ old("custom_input.{$field['field']->ref}") == $value ? ' selected' : '' }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        @break

                        @case('countries')
                        <div class="form__group{{ ! in_array($project->id, $field['projects']) ? ' hidden' : '' }}" data-show-for="{{ implode(',', $field['projects']) }}">
                            <label class="{{ $field['field']->required ? 'required' : '' }}" for="input-{{ $field['field']->ref }}" class="col-sm-4 control-label">{!! $field['field']->label !!}</label>
                            <select class="js-select2" id="input-{{ $field['field']->ref }}" class="additional-registration-field__input" name="custom_input[{{ $field['field']->ref }}]"{{ $field['field']->required ? ' data-required' : '' }} data-select2-search-placeholder="Type your country...">
                                @foreach(config('countries') AS $value)
                                    <option value="{{ strtolower($value) !== 'please select' ? $value : '' }}"{{ old("custom_input.{$field['field']->ref}") == $value ? ' selected' : '' }}>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        @break

                    @endswitch

                @endforeach
            </div>


            <button class="js-auto-color button button--primary"  type="submit">Register</button>

        </form>
    </div>
@endif