<section class="share-widget container">
  <div class="title-wrapper">
      <h4>
          @if ($project->getBlock('SHARE_TITLE'))
              {!! $project->getBlock('SHARE_TITLE') !!}
          @else
              Share
          @endif
      </h4>
      <span class="title-undeline"></span>
  </div>
  <ul class="share">
      <script>
          function shareLinkedIn(){
              var url = ('{!! $project->getBlock('social_link') !!}').length === 0 ? 'http://www.linkedin.com/shareArticle?mini=true&url={!! $project->full_url !!}' : ('http://www.linkedin.com/shareArticle?mini=true&url={!! $project->getBlock('SOCIAL_LINK') !!}').replace(/(<([^>]+)>)/ig,"");
              // window.location.assign(url);
              window.open(url, '_blank')
          }
          function shareFacebook(){
              var url = ('{!! $project->getBlock('social_link') !!}').length === 0 ? 'http://www.facebook.com/sharer/sharer.php?u=https://{!! $project->full_url !!}' : ('http://www.facebook.com/sharer/sharer.php?u={!! $project->getBlock('SOCIAL_LINK') !!}').replace(/(<([^>]+)>)/ig,"");
              // window.location.assign(url);
              window.open(url, '_blank')
          }
          function shareTwitter(){
              var url = ('{!! $project->getBlock('social_link') !!}').length === 0 ? 'http://twitter.com/intent/tweet?url=https://{!! $project->full_url !!}' : ('http://twitter.com/intent/tweet?url={!! $project->getBlock('SOCIAL_LINK') !!}').replace(/(<([^>]+)>)/ig,"");
              // window.location.assign(url);
              window.open(url, '_blank')
          }
      </script>
      <li><a style="cursor: pointer;" target="_blank" class="share__button share__linkedin" onclick="shareLinkedIn()"><i class='bi bi-linkedin'></i></a></li>
      <li><a style="cursor: pointer;" target="_blank" class="share__button share__facebook" onclick="shareFacebook()"><i class='bi bi-facebook'></i></a></li>
      <li><a style="cursor: pointer;" target="_blank" class="share__button share__twitter" onclick="shareTwitter()"><i class='bi bi-twitter'></i></a></li>
  </ul>
</section>