{{-- closing tags for previous section --}}
</section>
</section>
</section>

{{-- Start of Custom Banner --}}
<section id="banner" class="js-auto-color banner custom-banner {!! $branding->bg_img ? "no-border" : "" !!}">
    <div class="container inner"
        style="padding-top: 2rem; flex-direction: column;"
    >
    <div class="details">
        @if (!isset($event))
            @if (now()->lessThan($project->start_time))
                @if (request()->is($project->url . '/thanks'))
                    <p class="subtitle">Thanks for registering for</p>
                @elseif(request()->is($project->url . '/login'))
                    <p class="timeToWatch">Time to watch</p>
                @endif

                <div class="title">
                    <h1>{!! $project->getBlock('PROJECT_TITLE') ? $project->getBlock('PROJECT_TITLE') : $project->name !!}</h1>
                </div>
                <br />
                <p class="banner__register-by">
                    @if (request()->is($project->url . '/register'))
                        Register by:
                    @else
                        Starting in:
                    @endif
                </p>

                <span class="js-countdown countdown"></span>
            @else
                <p>{!! now()->lessThan($project->end_time) ? 'LIVE NOW:' : $project->getBlock('OD_TEXT') !!}</p>
                <div class="banner__title">
                    @if ($project->getBlock('PROJECT_TITLE'))
                        <h1>{!! $project->getBlock('PROJECT_TITLE') !!}</h1>
                    @else
                        <h1>{{ $project->name }}</h1>
                    @endif
                </div>
            @endif
        @else
            <div class="banner__title">
                @if ($project->getBlock('PROJECT_TITLE'))
                    <h1>{!! $project->getBlock('PROJECT_TITLE') !!}</h1>
                @else
                    {{-- <h1>{{ $project->name }}</h1> --}}
                    <h1>{!! $event->name !!}</h1>
                @endif
            </div>
        @endif
    </div>

        <section 
            class="columns is-desktop"
            style="width: 100%"
        >
            <section class="column">
                @php $current_section = 'custom_banner'; @endphp