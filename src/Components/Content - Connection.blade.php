<!-- VIDEOGO LIBRARY -->
<script src="https://cdn-aws.s3-eu-west-1.amazonaws.com/cdn-sg-productions/scripts/videoGo.js?v=1"></script>
<script>
  $(document).ready(function () {
    driftInit({
      id: 'myDriftCall1',
      createWidget: false,
      classes: [],
      maxCount: 6000,
    });
    driftInit({
      id: 'myDriftCall2',
      createWidget: false,
      classes: [],
      maxCount: 6000,
    });
    $('.drift-toggle-container').css({ 'visibility': 'hidden' });
  });
</script>

<section id="test-connection" class="test-connection-container container">
  <h4 class="">We're testing your connection</h4>
  <div class="player-container" style="position: relative">
    <div class="videoGoPlayer" id="player1"></div>
  </div>
  <p>We're just checking your connection to make sure you can watch the event. See your results below.</p>
  <div id="error">
    <div class="error-message">
      <h3><i class="fa fa-times-circle"></i> Your system currently cannot view the event</h3>
      <p>We're here to help - please contact our live chat support for assistance.
        <br /><br /><br />
        <button class="drift-open-chat" id="myDriftCall1" style="background-color: #ed2939 !important; border: none">
          Live chat support
        </button>
      </p>
    </div>
  </div>

  <div id="working" style="display: none">
    <div class="error-message" style="background: rgba(41, 237, 61, 0.2)">
      <h3 style="color: #008000"><i class="fa fa-check-circle"></i> Your system can view the event</h3>
      <p>It looks like you're all set, you should be able to see the video and hear some audio (if it's unmuted).
        However, if you're still worried just contact our live chat support for assistance.
        <br /><br /><br />
        <button style="background-color: #29ed3d !important; border: none" id="myDriftCall2">
          Live chat support
        </button>
      </p>
    </div>
  </div>
</section>

<script>
  var options = {
    id: 'player1',
    sources: [
      {
        file: 'https://streamgo-hls.secdn.net/streamgo-live/play/streamgo/playlist.m3u8',
        label: 'SE',
      },
      {
        file: 'https://sg-test-stream.gcdn.co/streamgo-live/play/streamgo/playlist.m3u8',
        label: 'Gcore',
      },
    ],
    disablePip: true,
    accent: '{!! $branding->accent_1 !!}',
    eventType: 'Webinar',
    isTestPlayer: true,
    currentTimeUTC: videoGo.currentDate(),
    eventStartUTC: '2020-01-01 00:00:00', // UTC DATE
    eventEndUTC: '2999-01-01 00:00:00', // UTC DATE
    aspectRatio: '16:9',
  };
  document.addEventListener('DOMContentLoaded', function () {
    videoGo.init(options);
    videoGo.players[0].player.player.on('play', function () {
      document.getElementById('working').style.display = 'block';
      document.getElementById('error').style.display = 'none';
    });
    videoGo.players[0].player.player.on('error', function () {
      document.getElementById('working').style.display = 'none';
      document.getElementById('error').style.display = 'block';
    });
    window.setTimeout(function () {
      $('.player-container').fadeOut();
      videoGo.players[0].player.destroy();
    }, 45000);
  });
</script>
<style>
  .video-js .vjs-fullscreen-control,
  .video-js .vjs-quality-selector {
    display: none !important;
  }
</style>