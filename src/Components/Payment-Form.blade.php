{{-- <style>
    #paymentForm button[type=submit] {
        margin-top: 1rem;
    }

    .payment-error-text {
        color: #c60000;
        display: block;
        margin-top: 1rem;
    }
</style> --}}
<section class="payment-form">
    @if ($paying)
        <h4 class="payment-form__title">Payment Form</h4>

        <form class="form" id="paymentForm" action="{{ route('users.pay', $project->url) }}" method="post">
            @csrf

            <input type="hidden" id="stripe_client_secret" value="{{ $clientSecret }}" />
            <input type="hidden" id="stripe_payment_intent_id" value="{{ $paymentIntent }}" />
            <input type="hidden" id="stripe_redirect" value="{{ route('users.post-pay', [$project->url]) }}">

            <div id="paymentElement"></div>

            <span class="payment-error-text"></span>

            <button id="payButton" type="submit">Pay £{{ $cost }} now</button>
        </form>
    @else
        <h4 class="js-auto-color form-title">Processing Payment</h4>
        <div class="register form">
            Processing payment, please wait
        </div>
    @endif
</section>

@if (!$paying)
    <script>
        setInterval(function() {
            window.location.reload();
        }, 5000);
    </script>
@endif
