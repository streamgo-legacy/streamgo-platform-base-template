@if(determine_base_template($project->template) === 1)
    @if ($project->getBlock("EVENT_CUSTOM_CONTENT"))
        {!! $project->getBlock("EVENT_CUSTOM_CONTENT") !!}
    @endif

    <div class="player-container" oncontextmenu="return false;" style="position:relative;">
        <div id="primary-video">
            {!! $project->getBlock('VIDEOPRE') !!}
        </div>
    </div>

    <!-- css to make sure the event player styled -->
    <style>    
        #video-section{
            z-index: 999;
        }
        #video-section {
    padding: 1rem;
    }

    .videoGoPlayer {
        border: 8px solid var(--white);
        border-radius: 0.5rem;
        box-shadow: 0px 0px 12px rgb(0 0 0 / 30%);
    }

    .vjs-button {
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .vjs-big-play-button .vjs-icon-placeholder {
        width: 70px;
        height: 70px;
        background: black;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 3rem;
        line-height: 70px;
        border-radius: 999px;
    }

    .videoGoPlayer .video-js {
        padding-top: 0px !important;
        width: 100%;
        height: 100% !important;
        position: absolute !important;
        top: 0;
    }
    .video-js .vjs-tech {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .vjs-control-bar{
    height: 32px !important;
    }
    .vjs-control-bar .vjs-control{
    height: 32px !important;
    }
    .vjs-control .vjs-button{
    justify-content: center;
    align-items: center;
    }
    .vjs-button > .vjs-icon-placeholder{
    display: flex !important;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
    }
    </style>
@else
    <div class="videoGoPlayer" id="primary-video">
        {!! $project->getBlock('VIDEOPRE') !!}
    </div>
@endif
    
<!-- VIDEOGO LIBRARY -->
<script src="https://cdn-aws.s3-eu-west-1.amazonaws.com/cdn-sg-productions/scripts/videoGo.js?v=7"></script>

<script>

    var options = {
        id: 'primary-video',
        @if(!empty(optional($event->video())->sources) && $event->video()->sources->count() > 1)
        sources: [
            @foreach($event->video()->sources AS $vid)
                { file: "{{ $vid->source }}", label: "{{ $vid->label }}" {{ $vid->default_source ? ', default: true' : '' }} }
                {{ !$loop->last ? ',' : '' }}
            @endforeach
        ],
        @else
            sources: [{ file: "{{ !empty($event->video()) && !empty(optional($event->video()->sources)->first()) ? $event->video()->sources->first()->source : ($event->project->isCollection() ? config('app.videoscout_default_video') : config('app.default_video')) }}" }],
        @endif
        eventId: {{ $event->id ?? '0'}},
                eventType: '{{ in_array($event->event_type, ['page', 'sponsor-page']) ? 'On-Demand' : $event->event_type() }}',
        currentTimeUTC: '{{ gmdate('Y-m-d H:i:s') }}', // UTC DATE
        eventStartUTC: '{{ $event->start_time?->tz('UTC')->format('Y-m-d H:i:s') ?? gmdate('Y-m-d H:i:s') }}', // UTC DATE
        eventEndUTC: '{{ $event->end_time?->tz('UTC')->format('Y-m-d H:i:s') ?? '' }}', // UTC DATE
        accent: '{{ $branding->accent_1 }}',
        aspectRatio: '{{ $event->video()->ratio ?? '16:9' }}',

        @if(!empty($live_captions) && $live_captions->count())
            captionsWebsocketUrl: "wss://{{ config('api.presentgo.captions.domain') }}/captions?eventId={{ $event->id }}",
            captionLanguages: @json($live_captions),
        @endif

        @isset($event->video()->poster)
            poster: "{{ storage_url($event->video()->poster) }}",
        @endisset
        tracks: [
        @isset($event->video()->vtt)
            { 
                kind: "thumbnails",
                file: "{{ storage_url($event->video()->vtt) }}"
            },
        @endisset

        @if(optional($event->video())->chapters_vtt)
            {
                kind: "chapters",
                file: "{{ storage_url($event->video()->chapters_vtt) }}"
            },
        @endisset

        @if(!empty($event->video()) && !$event->video()->hide_captions && $event->video()->captions->count())
            @foreach($event->video()->captions AS $caption)
                { kind: "captions", file: "{{ $caption->source }}", label: "{{ $caption->label }}" }
                {{ !$loop->last || $event->scheduled_poll_vtt ? ',' : '' }}
            @endforeach
        @endif
        @if($event->scheduled_poll_vtt)
            {
                kind: "metadata",
                file: "{{ $event->scheduledPollVttFullUrl() }}"
            },
        @endif
        ],
    };

    @isset($event->redirect_url)
        var redirectAfterFinish = "{{ $event->redirect_url }}";
    @endisset

    document.addEventListener('DOMContentLoaded', function () {
        videoGo.init(options);
    });
    
</script>