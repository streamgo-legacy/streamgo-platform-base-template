<section class="event-details-container container">
    <h4 class="event-details__title">Event details</h4>
    @if ($project->getBlock('CONTENT_DETAILS'))
        <div class="event-details">
            {!! $project->getBlock('CONTENT_DETAILS') !!}
        </div>
    @else
        <div 
            class="event-details" 
            data-ed
            data-ed-start-time="{!! $project->start_time->valueOf() !!}"
            data-ed-end-time="{!! $project->end_time->valueOf() !!}"
        >
            <div class="event-details__item event-details__item__date">
                <label class="event-details_label">Date</label>
                <div class="event-details_content">
                    <div class="skeleton"></div>
                </div>
            </div>
            <div class="event-details__item event-details__item__time">
                <label class="event-details_label">Time</label>
                <div class="event-details_content">
                    <div class="skeleton"></div>
                </div>
            </div>
            <div class="event-details__item event-details__item__duration">
                <label class="event-details_label">Duration</label>
                <div class="event-details_content">
                    <div class="skeleton"></div>
                </div>
            </div>
        </div>
    @endif
</section>
