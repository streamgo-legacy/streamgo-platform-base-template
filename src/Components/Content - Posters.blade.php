
<section class="container content_posters_container">
  @if(!isset($banner_existed))
    <h4 class="poster_title">{{ $project->getBlock('POSTER_LIST_TITLE') ?: "Poster Hall" }}</h4>
  @endif


    @if ($project->getBlock('CUSTOM_POSTERS_CONTENT'))
      {!! $project->getBlock('CUSTOM_POSTERS_CONTENT') !!}
    @else
      @if($project->events()->where('event_type', 'sponsor-page')->where('event_page_type_id', 8)->has('tags')->count())
        @foreach ($project->events()->where('event_type', 'sponsor-page')->where('event_page_type_id', 8)->get()->groupBy(fn($item) => $item->tags->first()->name ?? 'Others')->sortKeysUsing(fn($a) => $a === 'Others' ? 1 : 0 ) as $key => $GroupedPosterEvents)
        <div class="posters-container">
          <h4 class="posters-title">{{ $key }}</h4>
          <div class="posters">
            @foreach($GroupedPosterEvents as $posterEvent)
              @if ($posterEvent->img)
              <div 
                class="poster_item"
                data-tags="{{ $posterEvent->tags->map(fn($tag) => Str::slug($tag->name)) }}"
              >
                <div class="poster_item_link">
                  <a href="{{ $posterEvent->url }}">
                    <img alt="{{ $posterEvent->name }}" src="{{ storage_url($posterEvent->img) }}"/>
                    <br/>
                    <h4>{{ $posterEvent->name }}</h4>
                  </a>
                </div>
              </div>
              @endif
            @endforeach
          </div>
        </div>
        @endforeach
      @else
        <div class="posters">
          @foreach ($project->events()->where('event_type', 'sponsor-page')->where('event_page_type_id', 8)->get() as $posterEvent)
            @if ($posterEvent->img)
            <div class="poster_item">
              <div class="poster_item_link">
                <a href="{{ $posterEvent->url }}">
                  <img alt="{{ $posterEvent->name }}" src="{{ storage_url($posterEvent->img) }}"/>
                  <br/>
                  <h4>{{ $posterEvent->name }}</h4>
                </a>
              </div>
            </div>
            @endif
          @endforeach
        </div>
      @endif
    @endif
</section>