@if(($project->isLoggedIn() || session('chat_go_token')) && is_array($project->chatgo_event) && $event->hasAssociatedChatGoSponsor() )
	<script>
      window.addEventListener('load', function(e){
          chatGo.init("{!! $project->chatgo_event['key'] !!}", {
              "mode": "widget",
              "dmId": {{ $event->chatgo_sponsor['id'] }},
            	isAutoAuth: true,
          });
      });
	</script>
@endif