<?php

return [
    'color' => '#333',
    'bg_color' => '#fff',
    'border_color' => '#fff',
    'accent_1' => '#44bdb5',
    'accent_2' => 'rgba(252, 155, 30, 1)',
    'accent_3' => '#cc44cc',
];