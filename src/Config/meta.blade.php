<!-- Base Template v1.5 -->
<!-- Social Share Settings Start -->
@if ($branding->social_title)
    <meta property="og:title" content="{{ $branding->social_title }}">
@elseif($project->getBlock('social_title'))
    <meta property="og:title" content="{{ $project->getBlock('social_title') }}">
@else
    <meta property="og:title" content="{{ $project->name }}">
@endif

@if ($branding->social_description)
    <meta property="og:description" content="{{ $branding->social_description }}">
@elseif($project->getBlock('social_description'))
    <meta property="og:description" content="{{ $project->getBlock('social_description') }}">
@elseif(!empty($project->getBlock('webinar_description')))
    <meta property="og:description" content="{{ $project->getBlock('WEBINAR_DESCRIPTION') }}">
@else
    <meta property="og:description" content="{{ $project->name }}">
@endif


@if(!empty($branding->social_image))
    <meta property="og:image" content="{{ storage_url($branding->social_image) }}">
@elseif(!empty($branding->bg_img))
    <meta property="og:image" content="{{ storage_url($branding->bg_img) }}">
@endif

@if ($branding->social_link)
    <meta property="og:url" content="{{ $branding->social_link }}">
@else
    <meta property="og:url" content="{{ $project->full_url }}">
@endif

<meta name="twitter:card" content="summary_large_image">
<!-- Social Share Settings End -->

@if(!empty($branding->favicon))
    <link rel="icon" type="image/png" href="{{ storage_url($branding->favicon) }}"/>
@endif


<style>
    {{ $branding->bg_color ? "body{background-color:{$branding->bg_color};}" : '' }}
    {{ $branding->accent_1 ? "a{color:{$branding->accent_1};}" : '' }}
    {{ $branding->accent_3 ? ".section-title{color:{$branding->accent_3};}" : '' }}
    {{ $branding->border_color ? ".button, input[type='submit'], button{background-color:{$branding->border_color};}" : '' }}
    {{ $branding->bg_img ? ".banner{background-image:url(".storage_url($branding->bg_img).");}" : '' }}
    {{ $branding->accent_2 ? ".sidebar-item a{color:{$branding->accent_2};}" : '' }}
    {!! $branding->styles !!}
</style>

<!-- script for global css -->
<link href="{{ vapor_asset('/css/sgGlobal.css') }}" rel="stylesheet"/>

<!-- default css vars based on brandings -->
<style>
    :root{
        --primary: {{ $branding->accent_1 }};
        --secondary: {{ $branding->accent_2 }};
        --alternate: {{ $branding->accent_3 }};
        --white: #ffffff;
        --light: #eeeeee;
        --medium: #888888;
        --dark: #333333;
        --darker: #222222;
        --black: #000000;
        --success: green;
        --warning: #ccaa33;
        --danger: #dd6666;
        --font: {{ $branding->color }};
        --bg-color: {{ $branding->bg_color }};
    }
</style>

<!-- script for global JS -->
<script src="{{ vapor_asset('/js/sgGlobal.js') }}"></script>

@if(isset($event) && !empty($event->branding))
    <style>
        {!! $event->branding->styles !!}
    </style>
@endif