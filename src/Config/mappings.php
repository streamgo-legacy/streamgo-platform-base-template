<?php

return [
    'Home' => [
            'Header',
            'Banner',
            'Content',
            'Splitter',
            'Content - Share',
            'Agenda Carousel',
            'Footer',
        ],
    'Login' => [
            'Header',
            'Banner',
            'Content - Details',
            'Content',
            'Splitter',
            'Login Form',
            'Footer',
        ],
    'Register' => [
            'Header',
            'Banner',
            'Content - Details',
            'Content',
            'Content - Presenters',
            'Splitter',
            'Registration Form',
            'Footer',
        ],
    'Thanks' => [
            'Header',
            'Banner',
            'Content - Details',
            'Content',
            'Content - Connection',
            'Splitter',
            'Calendar',
            'Thanks - Multi-registration',
            'Footer',
        ],
    'Event' => [
            'Header',
            'Banner',
            'Event - View',
            'Content',
            'Content - Presenters',
            'Splitter',
            'ChatGo - Embed',
            'Content - Share',
            'Agenda Carousel',
            'Footer',
        ],
    'Interact' => [
            'Header',
            'Banner',
            'Event - Interact',
            'Footer',
        ],
    'Payment' => [
            'Header',
            'Banner',
            'Payment-Form',
            'Footer',
        ],
    'Certificate' => [
            'Header',
            'Banner',
            'Content-Certificate',
            'Footer',
        ],
];