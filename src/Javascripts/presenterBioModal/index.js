const getModal = (dataset, bioHtml) => {
	const { name, title, company, img } = dataset;
	const container = document.createElement("div");
	const overlay = document.createElement("div");
	const modal = document.createElement("div");
	const closeBtn = document.createElement("button");
	container.classList.add("presenter-bio-modal-container");
	overlay.classList.add("overlay");
	modal.classList.add("presenter-bio-modal");
	closeBtn.classList.add("close-btn");

	const closeBtnHtml = `<span class="material-symbols-outlined">close</span>`;

	const html = `<div class='presenter-info'>
        ${
					typeof img !== null && typeof img !== "undefined"
						? `<img class='presenter-img' src='${img}' alt='${name}'/>`
						: ""
				}
        <div class='presenter-details'>
            <h4 class='presenter-name'>${name}</h4>
            ${
							typeof title !== "undefined" && title !== ""
								? `<p class='presenter-title'>${title}</p>`
								: ""
						}
            ${
							typeof company !== "undefined" && company !== ""
								? `<p class='presenter-company'>${company}</p>`
								: ""
						}
        </div>
    </div>
    <div class='presenter-bio'>
        ${bioHtml}
    </div>`;

	closeBtn.insertAdjacentHTML("afterbegin", closeBtnHtml);
	modal.insertAdjacentHTML("afterbegin", html);
	modal.appendChild(closeBtn);
	container.appendChild(overlay);
	container.appendChild(modal);
	document.body.appendChild(container);
	setTimeout(() => {
		container.classList.add("active");
	}, 10);
	overlay.addEventListener("click", () => closeModal(container));
	closeBtn.addEventListener("click", () => closeModal(container));
};

const closeModal = (container) => {
	container.classList.remove("active");
	setTimeout(() => {
		container.remove();
	}, 490);
};

export const init = () => {
	// get all presenters
	const presenters = document.querySelectorAll(".js-presenter");

	presenters.forEach((p) => {
		const dataset = p.dataset;
		const triggers = p.querySelectorAll(".js-presenter-bio-trigger");
		const bioElement = p.querySelector('.js-presenter-bio');
		if (!triggers || !bioElement) {
			return;
		}
		triggers.forEach((trigger) => {
			trigger.style.cursor = "pointer";

			trigger.addEventListener("click", () => {
				const modalHTML = getModal(dataset, bioElement.innerHTML);
				document.body.insertAdjacentHTML("beforeend", modalHTML);
			});
		});
	});
};
