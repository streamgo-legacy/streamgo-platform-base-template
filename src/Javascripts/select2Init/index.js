export const init = () => {
    if (!jQuery.fn.select2) {
        console.log('no select2 JS found, not running select 2 init calls');
        return
    };

    $('.js-select2').each(function () {
        // get search placeholder from data attr
        const searchPlaceholder = $(this).attr(`data-select2-search-placeholder`);
        const id = $(this).attr(`id`);

        $(this).select2({
            minimumResultsForSearch: 20
        });

        if (searchPlaceholder && id) {
            console.log('have search placeholder');
            $(`#${id}`).one('select2:open', function(e) {
                $('input.select2-search__field').prop('placeholder', searchPlaceholder);
            });
        }
    })
}
