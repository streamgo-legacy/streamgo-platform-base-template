export const testPing = () => {
	console.log("Ping! Sent from sgGlobal.testPing()");
};

export const getScript = (url, id) => {
	if (typeof url !== "string") {
		return;
	}

	const tag = document.createElement("script");
	tag.id = id;
	tag.src = url;
	document.head.insertAdjacentElement(`beforeend`, tag);
};

export const forwardParams = () => {
	// get current url params
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const demoParam = urlParams.get("demo");
    const notDemo = demoParam === null;
    
    if (notDemo) { return };

	// get all href component
	window.addEventListener("load", () => {
		const projectUrl = document.querySelector('meta[name="project-url"]').content;
		const links = document.querySelectorAll(`[href]`);
		links.forEach((x) => {
            const href = x.href;
            const isProjectLinks = href.includes(projectUrl);
            if (isProjectLinks) {
                let url = new URL(href);
                url.searchParams.append(`demo`, demoParam)
                x.href = url;
            }
		});
	});
};
