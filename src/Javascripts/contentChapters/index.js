// for reference on dayjs: https://day.js.org/docs/en/installation/installation
import dayjs from "dayjs";
import duration from 'dayjs/plugin/duration';
dayjs.extend(duration);

export const init = () => {
  // need to loop over nodelist of content and send out the js for each time
  let times = document.querySelectorAll('.content-chapters .chapter-time');

  times.forEach(time => {
    const timeMs = parseInt(time.innerHTML);
    if (timeMs === NaN) { return };
    time.innerHTML = dayjs.duration(timeMs).hours > 0 ? dayjs.duration(timeMs).format('H:mm:ss') : dayjs.duration(timeMs).format('mm:ss')
  });

    // for (let i = 0; i <= contentChapterTimeMS.length; i++) {
    //       let ms = +contentChapterTimeMS[i].innerHTML
    //       let minutes = Math.floor(ms / 60000);
    //       let seconds = ((ms % 60000) / 1000).toFixed(0);
    //       const chapterTime = minutes + ":" + (seconds < 10 ? '0' : '') + seconds
    //       contentChapterTimeMS[i].textContent = chapterTime.toString()
    // }
}