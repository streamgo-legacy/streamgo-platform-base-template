import dayjs from "dayjs";
import duration from 'dayjs/plugin/duration';
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(duration);
dayjs.extend(relativeTime);

export const init = () => {
	const elements = document.querySelectorAll(`[data-ed]`);
	const elArray = Array.from(elements);
	if (elArray.length <= 0) {
		return;
	}

	elements.forEach((el) => updateEventDetails(el));
};

function updateEventDetails(element) {
	const data = element.dataset;
	const start = dayjs(Number(data.edStartTime));
    const end = dayjs(Number(data.edEndTime));
    const startDate = dayjs(`${dayjs(Number(data.edStartTime)).format('YYYY-MM-DD')}T00:00`);
    const endDate = dayjs(`${dayjs(Number(data.edEndTime)).format('YYYY-MM-DD')}T00:00`);
    const isSameDate = startDate.isSame(endDate);
    const durationInMin = end.diff(start, 'minute');
    const durationDay = end.diff(start, 'day') + 1;
    const durationDisplay = dayjs.duration(isSameDate ? durationInMin : durationDay, isSameDate ? 'minute' : 'days');
    const minFormat = durationDisplay.minutes() ? `m [Minute${durationDisplay.minutes() > 1 ? 's' : ''}]` : ``;
    const hourFormat = durationDisplay.hours() ? `H [Hour${durationDisplay.hours() > 1 ? 's' : ''}]` : ``;
    const dayFormat = durationDisplay.days() ? `D [Day${durationDisplay.days() > 1 ? 's' : ''}]` : ``;
    const durationFormat = `${dayFormat} ${hourFormat} ${minFormat}`;
    const clientTz = Intl.DateTimeFormat().resolvedOptions().timeZone;
    const isUK = clientTz === 'Europe/London';
	const userLocale =
		navigator.languages && navigator.languages.length
			? navigator.languages[0]
			: navigator.language;
	const tz = start
		.toDate()
		.toLocaleDateString(userLocale, {
			day: "2-digit",
			timeZoneName: "short",
		})
        .slice(4);
    
    const timeEl = element.querySelector('.event-details__item__time');
    const dateContent = element.querySelector('.event-details__item__date .event-details_content');
    const timeContent = element.querySelector('.event-details__item__time .event-details_content');
    const durationContent = element.querySelector('.event-details__item__duration .event-details_content');
    if (dateContent) {
        dateContent.innerHTML = isSameDate ? start.format(`DD MMMM YYYY`) : `${start.format(`MMMM DD`)} - ${end.format(`DD YYYY`)}`;
    }
    if (!isSameDate) { timeEl.style.display = 'none' };
    if (timeContent) {
        timeContent.innerHTML = `${start.format('HH:mm')} - ${end.format(`HH:mm`)} ${isUK ? `` : tz}`;
    }
    if (durationContent) {
        durationContent.innerHTML = durationDisplay.format(durationFormat);
    }
}
