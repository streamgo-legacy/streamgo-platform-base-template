

export const init = () => {
    const replyContainer = document.querySelector(".question-replies-container ");

    if (!replyContainer) { return };

	var observer = new MutationObserver(callback);
	observer.observe(replyContainer, {
		childList: true,
		subtree: true,
	});
};

function callback(mutationList) {
    mutationList.forEach(function (mutation) {
        const target = mutation.target;
        const classes = target.classList;
        console.log('mutation found', target, classes);
        if (classes.contains('question-replies')) { 
            const items = target.querySelectorAll(`.question-reply`);
            const itemsArray = Array.from(items);
            console.log(`target contains`, items, itemsArray);

            const replyContainer = document.querySelector(".question-replies-container "); 
            
            if (itemsArray.length) {
                replyContainer.classList.remove('hide');
            } else {
                replyContainer.classList.add('hide');
            }
        }
    });
};