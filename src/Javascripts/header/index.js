export const init = () => {
    window.addEventListener('scroll', () => handleSticky());
    handleSticky();
    handleMenu();
    handleSponsorCarousel();
}

function handleMenu() {
    const primaryMenu = document.querySelector('.primary-menu');
    const menuToggle = document.querySelector('.primary-menu__toggle');
    const closeBtn = document.querySelector('.primary-menu > ul > .closeBtn');
    if (!primaryMenu || !menuToggle || !closeBtn) { return };
    
    const overlay = primaryMenu.querySelector('.overlay');

    const openMenu = () => {
        primaryMenu.style.display = 'flex';
        setTimeout(() => {
            primaryMenu.classList.add('open');
        }, 20);
    }

    const closeMenu = () => {
        primaryMenu.classList.remove('open');
        setTimeout(() => {
            primaryMenu.style.display = 'none';
        }, 480);
    }

    if(menuToggle){
        menuToggle.addEventListener('click', () => {
            openMenu();
        })
    }

    if(closeBtn && overlay){
        closeBtn.addEventListener('click', () => {
            closeMenu();
        })
        overlay.addEventListener('click', () => {
            closeMenu();
        })
    }
}

function handleSticky() {
    const header = document.querySelector(`.site-head`);
    if (!header) { return };
    const headerY = header.clientHeight;
    const scrollY = window.scrollY;
    const windowHeight = window.innerHeight;
    if (scrollY > headerY * 2) {
        header.style.top = `-${headerY * 2}px`;
        if (scrollY > windowHeight / 2) {
            header.classList.add('sticky');
            document.body.style.paddingTop = `${headerY}px`;
            header.style.top = `0px`;
        }
    } else {
        document.body.style.paddingTop = `${0}px`;
        header.style.top = `0px`;
        header.classList.remove('sticky');
    }
}

function handleSponsorCarousel() {
    console.log('handleSponsorCarousel');
    const carousel = document.querySelector(`.site-head__sponsors`);
    if (!carousel) { return };

    const track = carousel.querySelector(`.site-head__sponsors__track`);
    if (!track) { return };
    const links = carousel.querySelectorAll(`.site-head__sponsor-link`);
    const linksArray = Array.from(links);
    if (linksArray.length <= 1) { return };

    const int = 10000;
    const count = linksArray.length;
    const offset = carousel.clientHeight;
    let index = 0;
    setInterval(() => {
        if (index === count - 1) {
            track.style.transition = `0ms`;
            index = 0;
        } else {
            track.style.transition = `500ms`;
            index++;
        }
        track.style.top = `-${offset * index}px`;
    }, int);
}