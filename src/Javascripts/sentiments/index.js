export const initAnimations = () => {
    if(!ratingAnimation){ 
        console.log('No Rating Animation JS func loaded, will not call ratings animations');
        return
    };

    const items = document.querySelectorAll('.ratings-item[data-order]');
    items.forEach(x => {
        const order = x.dataset.order;
        const ratingItemRef = `.ratings-item[data-order='${order}']`;
        console.log('calling rating animation for', ratingItemRef);
        ratingAnimation.addAnimation(ratingItemRef);
    });
}