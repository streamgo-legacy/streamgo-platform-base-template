import dayjs from "dayjs";

/**
 * Agenda List Functions
 * @module AgendaList
 */

/**
 * Initialise the page with the default Agenda list styles
 * @memberof AgendaList
 * @function
 * @return {null} null
 */
export const init = () => {
	//get all agenda list elements
	const lists = document.querySelectorAll(".agenda-list");

	// get all agenda list dividers
	const dividers = document.querySelectorAll(`.agenda-date-divider`);

	// loop all dividers
	dividers.forEach((x, idx) => {
		const dataset = x.dataset;
		const startTime = dataset.startTime;
		const date = dayjs(Number(startTime)).startOf("day");
		const today = dayjs().startOf("day");
		let isFirst = true;
		x.dataset.date = date.valueOf();
		if (idx > 0) {
			const prevItem = dividers[idx - 1];
			const prevDate = dayjs(Number(prevItem.dataset.startTime)).startOf("day");
			isFirst = prevDate.valueOf() !== date.valueOf();
		}
		if (isFirst) {
			x.classList.add("show");
			const html = `<div class='content'>${
				date.isSame(today) ? `Today` : String(date.format(`DD MMM`))
			}</div>`;
			x.insertAdjacentHTML("afterbegin", html);
		}
	});

	lists.forEach((l) => {
		const items = l.querySelectorAll(".agenda-item");
		items.forEach((x) => {
			const timeEl = x.querySelector(`.agenda-item-time__before`);
			if (!timeEl) { return };
			const startTime = dayjs(parseInt(x.dataset.startTime));
			const endTime = dayjs(parseInt(x.dataset.endTime));
			
			timeEl.innerHTML = `${startTime.format(`hh:mmA`)} - ${endTime.format(
				`hh:mmA`
			)}`;

			const tags = x.dataset.tags ? JSON.parse(x.dataset.tags) : [];

			if(tags.includes('no-page')){
				const titleDom = x.querySelector(`.agenda-item-title`);
				const title = x.dataset.title;
				titleDom.innerHTML = title;
			}
		});
	});
};

/**
 * returns array of agenda lists on the loaded page
 * @memberof AgendaList
 * @function
 * @returns {NodeList} NodeList Array containing all nodes classed 'agenda-list'
 */
export const getAll = () => {
	return document.querySelectorAll(".agenda-list");
};
