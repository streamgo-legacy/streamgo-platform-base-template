/**
 * Show, Hide and Switch elements based on data-show, data-hide, data-switch
 * @module ShowHideSwitch
 */

import dayjs from "dayjs";

/**
 * Initialise an interval to show/hide and switch elements, based on a given interval time (in milliseconds, default is 3000).
 * While Show/Hide of elements will be done on every intervals, switching will only be done once. To perform switching again, please call the switch() function
 * @memberof ShowHideSwitch
 * @async
 * @function init
 * @param {number} [interval=300] - Milliseconds for the interval to run the show/hide again
 * @returns {null} null
 */
export const init = async (interval = 300) => {
    const switches = getAllSwitches();
	switches.forEach(async (x) => {
		doSwitch(x);
    });
    
    const doShowHide = () => {
        const items = getAllShowHideItems();
        items.forEach(item => showHide(item));
    }

    doShowHide();
    setInterval(() => doShowHide(), interval);

	return;
};

/**
 * Return NodeList containing all switch elements (elements with data-switch)
 * @memberof ShowHideSwitch
 * @function
 * @returns {NodeList} NodeList containing all switch elements
 */
export function getAllSwitches() {
	const switches = document.querySelectorAll("[data-switches]");
	return switches;
}

/**
 * Return list of nodes that has data-show / data-hide
 * @memberof ShowHideSwitch
 * @function getAllShowHideItems
 * @returns {NodeList<Element>} List of nodes that has data-show / data-hide
 */
export function getAllShowHideItems() {
    const items = document.querySelectorAll("[data-show], [data-hide]");
	return items;
}

/**
 * Perform show / hide function on a specific element
 * @memberof ShowHideSwitch
 * @function showHide
 * @param {Node} item Element to be show/hide
 * @returns {null} null
 */
export function showHide(item) {
	const showTime = parseInt(item.dataset.show);
    const hideTime = parseInt(item.dataset.hide);
    const originDisplay = getComputedStyle(item).display;
	const now = Date.now();
	if (!showTime && !hideTime) {
		return;
    }
    
    if (
        (!showTime || showTime < now) &&
        (item.style.display === 'none' || originDisplay === 'none')
    ) {
        item.style.display = originDisplay !== 'none' ? originDisplay : 'inline-flex';
    }

    if (hideTime && hideTime < now && item.style.display !== "none") {
        item.style.display = "none";
        return
    }
}

/**
 * Perform sorting and switching of items within the given element
 * @memberof ShowHideSwitch
 * @function doSwitch
 * @param {Node} item Element to be sorted
 * @returns {null} null
 */
export async function doSwitch(element) {
	const nodeList = element.querySelectorAll(`[data-switch]`);
	const items = Array.from(nodeList);
	if (!items || items.length <= 0) {
		return;
	}
	const sortedItems = [
		...items.sort((a, b) => {
			if (parseInt(a.dataset.switch) < parseInt(b.dataset.switch)) return -1;
			if (parseInt(a.dataset.switch) > parseInt(b.dataset.switch)) return 1;
			return 0;
		}),
	];

	let switchOrders = [
		{
			order: 0,
			items: []
		}
	];
	
	sortedItems.forEach((item, index) => {
		const switchPriority = item.dataset.switchPriority;
		if(typeof switchPriority === 'undefined' || switchPriority === '0'){
			switchOrders.find(x => x.order === 0).items.push(item);
		}else{
			const idx = switchOrders.findIndex(x => x.order === parseInt(switchPriority));
			if(idx < 0){
				switchOrders.push({
					order: parseInt(switchPriority),
					items: [item]
				})
			}else{
				switchOrders[idx].items.push(item);
			}
		}
	});

	switchOrders.sort((a,b) => a.order - b.order).forEach(s => {
		s.items.forEach(x => element.append(x));
	})
	return;
}