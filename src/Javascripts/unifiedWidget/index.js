import axios from "axios";
import dayjs from "dayjs";

const defaultUwSettings = {
	customAgenda: {
		enable: true,
	},
	discoverGo: {
		enable: true,
	},
	treasureHunt: {
		enable: false,
		loots: null,
		blankLootSrc: `https://sg-project-assets.s3.eu-west-2.amazonaws.com/100119/th-badges/blankBadge.svg`,
	},
	chatGo: {
		enable: false,
	},
	cpd: {
		enable: false,
	},
	leaderboard: {
		enable: true,
	},
	techSupport: {
		enable: true,
	},
};

export const init = (uwSettings) => {
	const uw = document.querySelector(".unified-widget");
	if (!uw) {
		return;
	}

	const discoverGoContainer = document.querySelector("#discover-go-container");
	if (discoverGoContainer) {
		dg_init({
			id: "discover-go-container",
		});
	}

	const settings = uwSettings ? uwSettings : defaultUwSettings;

	// console.log('uw settings > ', settings);

	// functions to check if settings enables the service
	const buttons = document.querySelectorAll(`.widget-buttons .widget-button`);
	buttons.forEach((x) => {
		const isEnabled = settings[x.dataset.trigger].enable;
		if (isEnabled) {
			x.classList.add("show");
			x.classList.remove("hide");
		} else {
			x.classList.add("hide");
			x.classList.remove("show");
		}
	});

	// funcs for unified widget buttons and drawers
	const triggers = document.querySelectorAll(".widget-button");
	const drawers = uw.querySelectorAll(".widget-drawer");
	triggers.forEach((t) => {
		t.addEventListener("click", () => {
			const triggerWasActive = t.classList.contains("active");
			triggers.forEach(x => {
				x.classList.remove('active');
			});
			!triggerWasActive && t.classList.add('active');
			const drawerRef = t.dataset.trigger;
			if (drawerRef === "techSupport") {
				const driftLocal = localStorage.getItem("drift_convo")
					? JSON.parse(localStorage.getItem("drift_convo"))
					: null;
				if (driftLocal) {
					console.log(typeof driftLocal.time);
				}
				if (driftLocal && Date.now() - driftLocal.time < 300000) {
					drift.api.widget.show();
				} else {
					drift.api.startInteraction({
						interactionId: interactionId ? interactionId : 98329,
						goToConversation: true,
					});
				}
			}
			const drawer = document.querySelector(`[data-drawer="${drawerRef}"]`);
			if (!drawer) {
				return;
			}
			const wasOpened = drawer.classList.contains("active");
			drawers && drawers.forEach((d) => d.classList.remove("active"));
			(!wasOpened || t.dataset.openOnly) && drawer.classList.add("active");
		});
	});
	drawers.forEach((d) => {
		const close = d.querySelector(".closeBtn");
		const trigger = document.querySelector(`.widget-button[data-trigger="${d.dataset.drawer}"]`);
		if(close){
			close.addEventListener("click", () => {
				d.classList.remove("active");
				if(trigger){
					trigger.classList.remove('active');
				}
			});
		}
	});

	const toggle = document.querySelector(".widget-toggle");
	toggle.addEventListener("click", () => {
		const buttons = uw.querySelector(".widget-buttons");
		if (buttons.classList.contains("collapse")) {
			buttons.classList.remove("collapse");
			buttons.classList.add("expand");
		} else {
			buttons.classList.add("collapse");
			buttons.classList.remove("expand");
		}
	});

	// end of unified widget buttons and drawers

	// func for event alerts
	setInterval(() => {
		handleAlerts();
	}, 10000);
	handleAlerts();

	// function for cpd progress
	if (settings.cpd.enable) {
		getCpdProgress();
		handleCpdProgressButton();	
	}
};

function handleAlerts() {
	if (!uwEvents) {
		return;
	}
	let items = [...uwEvents]
		.filter(
			(x) =>
				(x.type === "webinar" || x.type === "replay") &&
				x.end_time > Date.now() &&
				!x.tags.includes('no-page')
		)
		.sort((a, b) => {
			if (dayjs(a.start_time).isBefore(dayjs(b.start_time))) {
				return -1;
			}
			if (dayjs(a.start_time).isAfter(dayjs(b.start_time))) {
				return 1;
			}
			return 0;
		});

	items.forEach((x, idx) => {
		const startTime = dayjs(x.start_time);
		const endTime = dayjs(x.end_time);
		const now = dayjs();
		const isLive =
			now.valueOf() >= startTime.valueOf() &&
			now.valueOf() <= endTime.valueOf();
		items[idx] = Object.assign(
			{
				...x,
			},
			{
				isLive,
			}
		);
	});

	const alertsContainer = document.querySelector(
		".unified-widget .alerts-container"
	);
	if (!alertsContainer) {
		return;
	}
	const liveItems = [...items].filter((x) => x.isLive);
	const agendaUrl = alertsContainer.dataset.agendaUrl;
	let htmls = "";
	if (liveItems.length > 0) {
		if (liveItems.length == 1) {
			htmls = alertHTML("live", "Live", liveItems[0].name, liveItems[0].url);
		} else {
			if(agendaUrl){
				htmls = alertHTML(
					"live",
					"Live",
					`${liveItems.length} Events are Live Now!`,
					agendaUrl
				);
			}
		}
	} else if (items.length) {
		htmls = alertHTML("upNext", "Up Next", items[0].name, items[0].url);
	} else {
		// htmls = alertHTML('hide', 'Finished', 'All Events are finished', '#');
	}
	alertsContainer.innerHTML = "";
	alertsContainer.insertAdjacentHTML("beforeend", htmls);
}

function alertHTML(typeClass, label, title, url) {
	return `
    <a class="alert ${typeClass}" href="${url}">
        <div class="alert-label">${label}</div>
        <p class="alert-content" >${title}</p>
    </a>
    `;
}

export async function getCpdProgress() {

	const progressDisplay = document.querySelector(`.uw-cpd-progress-progress`);

	// get project url
	const projectUrl = document.querySelector('meta[name="project-url"]').content;
	if (!projectUrl) { return };
	const api = `${projectUrl}/cpd/stats/`;

	// turn text to skeleton
	if (progressDisplay) {
		progressDisplay.innerHTML = '';
		progressDisplay.classList.add('skeleton');
	}

	const res = await fetchCpdApi();
	if (!res || !res.type) {
		return
	}
	
	console.log(`result cpd type: `, res.type);
	console.log(`result cpd: `, res.data);
	if (res.type === 'project') {
		// render it on the html
		if (progressDisplay && res.data) {
			progressDisplay.classList.remove('skeleton');
			const t = res.data.total ? `Progress: ${res.data.progress}/${res.data.total} minutes watched.` : `Coming Soon~`;
			progressDisplay.innerHTML = t;
		}	
	} else if (res.type === 'event') {
		const data = res.data;
		const event_progress = data.event_progress;
		console.log('event level cpd api: ', data);
		if (!event_progress) { return };
		const content_watched = event_progress.content_watched;
		const watch_progress = content_watched.progress | 0;
		const watch_required = content_watched.requirement | 0;
		const watch_required_mills = watch_required * 60000;
		const watch_total = content_watched.total | 0;
		const polls_answered = event_progress.polls_answered;
		const polls_passed = event_progress.polls_passed;
		const container = document.querySelector(`.uw-cpd-progress-container`);
		const start_time = Number(container.dataset.eventStartTime);
		const end_time = Number(container.dataset.eventEndTime);
		const event_type = container.dataset.eventType;
		const event_id = container.dataset.eventId;
		let isPassable = true;

		if(event_type !== 'on-demand'){
			// check if type exist
			const watch_type = !content_watched ? `any_content` : content_watched.type ?? `any_content`;
			if(
				(watch_type === 'any_content' && (watch_required_mills < end_time - Date.now()))
				||
				(watch_type === 'first_content' && (Date.now() < start_time + 60000))
			){
				if(Date.now() >= (start_time - 600000)){
					localStorage.setItem(`sg-cpd-${event_id}`, `1`);
				}
			}else{
				if(!Number(localStorage.getItem(`sg-cpd-${event_id}`))){
					isPassable = false;
				}
			}
		}

		const displayHtml = `
		<div class='uw-cpd-progress-detailed'>
			<h4>Progress for this event ${data.event_progress.passed ? `<span class="uw-cpd-badge passed">PASSED</span>` : `<span class="uw-cpd-badge in-progress">IN PROGRESS</span>`}</h4>
			${!isPassable ?
				`<p class='you-shall-not-pass'>Unfortunately you're too late to claim CPD for this session as too much of the session has passed. This session will turn on-demand and you will be able to gain CPD when this turns on-demand.</p>` : ``
			}

			${content_watched ?
				`<div class='card'>
					<h5>Content Watched</h5>
					<p>${watch_progress}/${watch_total} minutes</p>
					<small>Required ${watch_required} minutes to pass.</small>
				</div>` : ``
			}

			${polls_answered && polls_answered.requirement ? 
			`<div class='card'>
				<h5>Poll Answered</h5>
				<p>${polls_answered.progress}/${polls_answered.total} Answered</p>
				<small>Required ${polls_answered.requirement} poll(s) answered to pass.</small>
			</div>` : ``
			}

			${polls_passed && polls_passed.requirement ? 
				`<div class='card'>
					<h5>Poll Passed</h5>
					<p>${polls_passed.progress} answered correctly</p>
					<small>Required ${polls_passed.requirement} of poll(s) correctly answered to pass.</small>
				</div>` : ``
				}
		</div>
		`;
		progressDisplay.classList.remove('skeleton');
		progressDisplay.insertAdjacentHTML('beforeend', displayHtml);

		// auto open this drawer if is not passable
		if(!isPassable){
			const targetDrawer = document.querySelector(`.widget-drawer[data-drawer="cpd"]`);
			if(targetDrawer){
				targetDrawer.classList.add('active');
			}
		}
	}
}

export async function fetchCpdApi() {
	const container = document.querySelector(`.uw-cpd-progress-container`);
	const projectUrl = document.querySelector('meta[name="project-url"]').content;
	if (!projectUrl || !container) { return };
	const eventId = container.dataset.eventId;
	const api = `${projectUrl}/cpd/stats/${eventId ? eventId : ''}`;

	console.log('before send cpd eventId:', eventId);
	console.log('before send cpd api:', api);

	const res = await axios.get(api);
	const data = await res.data;

	return {type: eventId ? 'event' : 'project', data};
}

function handleCpdProgressButton() {
	const btn = document.querySelector(`.uw-cpd-refresh-btn`);
	if (!btn) { return };
	btn.addEventListener('click', () => {
		getCpdProgress()
	})
}