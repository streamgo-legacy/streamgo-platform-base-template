export function streamGoCountdown(
    dateString,
    elementSelector,
    urlToRedirect,
    includeDays,
    hideAtEnd
  ) {
    var self = this;
  
    self.countdownDateString = dateString;
    self.includeDays = includeDays;
    self.elementSelector =
      elementSelector.indexOf('#') === 0 || elementSelector.indexOf('.') === 0
        ? elementSelector
        : '#' + elementSelector;
    self.urlToRedirect = urlToRedirect;
    self.hideAtEnd = hideAtEnd;
  
    self.convertLocalDate = function (date) {
      var localDate = new Date(date);
  
      var utcDate = new Date(localDate);
      utcDate.setMinutes(localDate.getMinutes() + localDate.getTimezoneOffset());
  
      return utcDate;
    };
  
    // set the HTML of the countdown clock
    self.setCountdown = function () {
      self.countdownDate = self.convertLocalDate(dateString);
      var obj = self.getTimeTillEvent();
  
      var days =
        obj.days !== undefined
          ? obj.days >= 10
            ? obj.days
            : '0' + obj.days
          : '00';
      var hours =
        obj.hours !== undefined
          ? obj.hours >= 10
            ? obj.hours
            : '0' + obj.hours
          : '00';
      var mins =
        obj.minutes !== undefined
          ? obj.minutes >= 10
            ? obj.minutes
            : '0' + obj.minutes
          : '00';
      var secs =
        obj.seconds !== undefined
          ? obj.seconds >= 10
            ? obj.seconds
            : '0' + obj.seconds
          : '00';
  
      var html = "<div class='countdown'>";
  
      if (self.includeDays) {
        html +=
          '<span class="countdown__item"><span class="countdown__item__top">' +
          days +
          '</span><span class="js-auto-color countdown__item__bottom">Days</span></span>';
      }
  
      html +=
        '<span class="countdown__item"><span class="countdown__item__top">' +
        hours +
        '</span><span class="js-auto-color countdown__item__bottom">Hours</span></span><span class="countdown__item"><span class="countdown__item__top">' +
        mins +
        '</span><span class="js-auto-color countdown__item__bottom">Mins</span></span><span class="countdown__item"><span class="countdown__item__top">' +
        secs +
        '</span><span class="js-auto-color countdown__item__bottom">Secs</span></span></div>';
  
      $(self.elementSelector).html(html);
    };
  
    // get the time till the event start, taking into account current timezone offsets and adjusting where necessary
    self.getTimeTillEvent = function () {
      var dateNow = new Date();
      var UTCDateNow = Date.UTC(
        dateNow.getUTCFullYear(),
        dateNow.getUTCMonth(),
        dateNow.getUTCDate(),
        dateNow.getUTCHours(),
        dateNow.getUTCMinutes(),
        dateNow.getUTCSeconds(),
        0
      );
      var UTCEventDate = Date.UTC(
        self.countdownDate.getUTCFullYear(),
        self.countdownDate.getUTCMonth(),
        self.countdownDate.getUTCDate(),
        self.countdownDate.getUTCHours(),
        self.countdownDate.getUTCMinutes(),
        self.countdownDate.getUTCSeconds(),
        0
      );
  
      var date = new Date(UTCEventDate);
      var offset = date.getTimezoneOffset();
  
      UTCEventDate = UTCEventDate - offset * 60 * 1000;
      var UTCEventDate60 = UTCEventDate + 60 * 1000;
  
      // only redirect up to a minute after event start
      if (UTCDateNow > UTCEventDate) {
        if (self.urlToRedirect && UTCDateNow < UTCEventDate60) {
          window.location.href = self.urlToRedirect;
        }
  
        if (self.hideAtEnd) {
          $(self.elementSelector).hide();
        }
  
        return {
          days: 0,
          hours: 0,
          minutes: 0,
          seconds: 0,
        };
      }
  
      return self.duration(UTCDateNow, UTCEventDate);
    };
  
    // get the duration between two dates days, hours, minutes and seconds
    self.duration = function (t0, t1) {
      var d = new Date(t1) - new Date(t0);
      var days = Math.floor(d / 1000 / 60 / 60 / 24);
      var hours = Math.floor(d / 1000 / 60 / 60 - days * 24);
      var minutes = Math.floor(d / 1000 / 60 - days * 24 * 60 - hours * 60);
      var seconds = Math.floor(
        d / 1000 - days * 24 * 60 * 60 - hours * 60 * 60 - minutes * 60
      );
  
      var t = {};
      ['days', 'hours', 'minutes', 'seconds'].forEach(function (q) {
        if (eval(q) > 0) {
          t[q] = eval(q);
        }
      });
  
      return t;
    };
  
    // start the countdown timer
    self.countdownDate = self.convertLocalDate(dateString);
    self.setCountdown();
    window.setInterval(self.setCountdown, 1000);
  }
  