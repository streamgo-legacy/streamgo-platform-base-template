export const init = () => {
    const container = document.querySelector(`.agenda-list`);
    if (!container) {
        return
    };
	const items = container.querySelectorAll(`.agenda-item`);
	let allTags = [];

	items.forEach((x) => {
		const tags = JSON.parse(x.dataset.tags);
		tags.forEach((t) => {
			const idx = allTags.findIndex((a) => a === t);
			if (t !== 'no-ca' && t!== 'no-page' && idx < 0) {
				allTags.push(t);
			}
		});
	});

	console.log(`allTags`, allTags);

	if (allTags.length <= 0) {
		return;
	}

	const filterContainer = document.createElement("div");
	filterContainer.id = "agenda-filter-container";
	filterContainer.classList.add("agenda-filter-container");

	const tagsHtml = `
    <div class="tags">
        <h5 class='agenda-filter-title medium'>Filter By: </h5>
        ${allTags
					.map((x) => {
						return `
            <span class="tag is-normal is-rounded" data-tag="${x}">
                ${x}
                <span class="material-symbols-outlined checked-icon">done</span>
            </span>
        `;
					})
					.join(``)}
    </div>
    `;

	container.insertAdjacentElement("afterbegin", filterContainer);
	filterContainer.insertAdjacentHTML("afterbegin", tagsHtml);

	const tagsButtons = document.querySelectorAll(
		`.agenda-filter-container .tag`
	);
	tagsButtons.forEach((b) => {
		b.addEventListener("click", (e) => {
			const target = e.target;
			target.classList.toggle("active");
			reCheckFilter();
		});
	});
};

function reCheckFilter() {
	// const container = document.querySelector(`.agenda-list`);
    const agendaGroups = document.querySelectorAll(`.agenda-list`);
	const activeTags = Array.from(
		document.querySelectorAll(`.agenda-filter-container .tag.active`)
	).map((x) => x.dataset.tag);

	if (activeTags.length) {
        agendaGroups.forEach((x) => {
			const items = x.querySelectorAll(`.agenda-item`);
			items.forEach((p) => {
				const itemTags = JSON.parse(p.dataset.tags);
				const isActive = [...itemTags].filter((t) =>
					activeTags.includes(t)
				).length;
                if (isActive) {
                    p.classList.remove('filter-hide');
                } else {
                    p.classList.add('filter-hide');
                }
            });
		});
	} else {
		agendaGroups.forEach((x) => {
            const items = x.querySelectorAll(`.agenda-item`);
            x.classList.remove('filter-hide');
			items.forEach((p) => {
				p.classList.remove('filter-hide');
			});
		});
	}
}
