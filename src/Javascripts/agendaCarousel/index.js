/**
 * Agenda List Functions
 * @module AgendaCarousel
 */

import Glide, {
	Controls,
	Breakpoints,
	Swipe,
	Anchors,
} from "@glidejs/glide/dist/glide.modular.esm";
import "@glidejs/glide/dist/css/glide.core.min.css";
import "@glidejs/glide/dist/css/glide.theme.min.css";
import dayjs from "dayjs";

/**
 * Query all elements classed '.agenda-carousel' and transforms each into a Glide JS slider.
 * @memberof AgendaCarousel
 * @function
 * @returns {null} null
 */
export const init = () => {
	console.log("initCarousels");
	const cars = document.querySelectorAll(".agenda-carousel");
	if (cars.length <= 0) {
		return;
	}

	var glide = new Glide(".glide", {
		type: 'slider',
		startAt: 0,
		perView: 4,
		perTouch: 4,
		focusAt: 0,
		bound: true,
		breakpoints: {
			1280: {
				perView: 3,
				perTouch: 3,
			},
			800: {
				perView: 2,
				perTouch: 2,
			},
			600: {
				perView: 1,
				perTouch: 1,
			},
		},
	});

	// handler for further control on carousel
	const handler = (g, o, e) => {
		// console.log(g, o, e);
		return {
			mount() {
				console.log("Example component has been mounted.");
			},
		};
	};

	glide.mount({
		Controls,
		Breakpoints,
		Swipe,
		Anchors,
		// 'handler': handler
	});

	cars.forEach((c) => {
		const items = c.querySelectorAll(".agenda-item");
		items.forEach((x) => {
			const timeEl = x.querySelector(`.agenda-item-time__before`);
			if (!timeEl) {
				return
			}
			const startTime = dayjs(parseInt(x.dataset.startTime));
			const endTime = dayjs(parseInt(x.dataset.endTime));
			timeEl.innerHTML = `${startTime.format(`hh:mmA`)} - ${endTime.format(
				`hh:mmA`
			)}`;

			const tags = x.dataset.tags ? JSON.parse(x.dataset.tags) : [];

			if(tags.includes('no-page')){
				const titleDom = x.querySelector(`.agenda-item-title`);
				const title = x.dataset.title;
				titleDom.innerHTML = title;
			}

			const imgLink = x.querySelector(`.agenda-item-thumbnail`);
			if(imgLink){
				imgLink.href = '#'
			}
		});
	});
};
