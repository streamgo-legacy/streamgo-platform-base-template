export const init = () => {
  setTimeout(() => {
    const loadMoreContainer = document.querySelector(
      `.social-feed-container .load-more-container`
    )
    const messageContainer = document.querySelector(
      `.social-feed-container .social-feed__message-container`
    )

    if(!messageContainer){
      return
    }

    function addGifResutClose(gifResultContainer) {
      const gifResultCloseBtn = document.createElement(`div`);
      const icon = document.createElement('span');
      gifResultCloseBtn.classList.add(`gif-container__close`);
      icon.classList.add(`material-symbols-outlined`);
      icon.innerHTML = 'close';
      gifResultCloseBtn.addEventListener('click', () => {
        gifResultContainer.classList.remove(`gif-results-container--visible`);
      });
      gifResultCloseBtn.appendChild(icon);
      gifResultContainer.insertAdjacentElement(`afterend`, gifResultCloseBtn);
    }

    // adding close button for gif result
    const gifResultContainers = document.querySelectorAll(`.sfGifResults`);
    gifResultContainers.forEach(x => addGifResutClose(x));
    /* end of gif result */

    messageContainer.addEventListener('scroll', e => {
      if (e.target.scrollTop >= -20) {
        loadMoreContainer.style.display = 'block'
      } else {
        loadMoreContainer.style.display = 'none'
      }
    })

    messageContainer.scrollTop = -messageContainer.scrollHeight

    const addSideLikes = () => {
      const ratingsContainers = messageContainer.querySelectorAll(
        '.social-feed-ratings-container'
      )
      ratingsContainers.forEach(item => {
        const count = item.querySelector('.rating-count');
        if (!count) { return };
        const countHTML = count.innerHTML;
        // check if sideCount already exist
        const sideCountElement = item.querySelector('.side-count')
        if (!sideCountElement) {
          const newSideCountElement = document.createElement('span')
          newSideCountElement.classList.add('side-count')
          newSideCountElement.innerHTML = count
          item.appendChild(newSideCountElement)
        }
      })
    }
    addSideLikes()

    // // function to hide 0 replies
    const updateRepliesButtons = () => {
      const repliesBtns = messageContainer.querySelectorAll(
        `.social-post__meta__reply__count`
      )
      repliesBtns.forEach(item => {
        const nextDot = item.nextElementSibling
        const count = parseInt(item.innerHTML[0])
        if (isNaN(count)) {
          return
        }
        if (count <= 0) {
          item.style.display = 'none'
          nextDot.style.display = 'none'
        } else {
          item.style.display = 'initial'
          nextDot.style.display = 'initial'
        }
      })
    }

    // observe changes to update content
    function callback(mutationList) {
      mutationList.forEach(function (mutation) {
        //console.log(mutation.target.classList);
        const target = mutation.target
        const classes = target.classList
        if (classes.contains('rating-count')) {
          const targetElement = mutation.target
          const count = targetElement.innerHTML
          const sideCountElement = targetElement
            .closest('.social-feed-ratings-container')
            .querySelector('.side-count')
          sideCountElement.innerHTML = count
        }
        if (classes.contains(`social-post__meta__reply__count`)) {
          //console.log('reply updates', mutation);
          updateRepliesButtons()
        }
        if (classes.contains('social-feed__message-container')) {
          //new feeds loaded
          addSideLikes()
        }
        if (
          classes.contains('typing-indicator') ||
          classes.contains('social-feed__info-message')
        ) {
          const content = target.innerHTML
          if (content === '') {
            target.closest(`.social-feed-messages`).style.display = 'none'
          } else {
            target.closest(`.social-feed-messages`).style.display = 'flex'
          }
        }
      })
    }

    var observer = new MutationObserver(callback)
    observer.observe(document.querySelector('.social-feed-container'), {
      childList: true,
      subtree: true,
    })

    // setup event listener to have the emoji drawer goes under the button
    const moveEmojiDrawer = () => {
      const sfEmojiTrigger = document.querySelector('.sfEmojiTrigger');
      if (!sfEmojiTrigger) { return };
      sfEmojiTrigger.addEventListener(`click`, () => {
        const drawer = document.querySelector(`.emoji-picker__wrapper`);
        if (!drawer) { return };
        sfEmojiTrigger.appendChild(drawer);
      });
    }
    moveEmojiDrawer();
  }, 2000)
}
