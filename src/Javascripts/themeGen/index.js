import Color from 'color';

export const generateCssVars = () => {
    // setup for bg color and the whole page
    const bgColorValue = getComputedStyle(document.documentElement).getPropertyValue(`--bg-color`);
    if(typeof bgColorValue !== 'string'){ return };
    if(bgColorValue.trim() === ''){ return };

    const bgColor = Color(bgColorValue.trim());
    
    document.documentElement.style.setProperty(
        `--light`, 
        bgColor.isLight() ? bgColor.darken(0.1) : bgColor.lightness(30)
    );
    document.documentElement.style.setProperty(
        `--dark`, 
        bgColor.isLight() ? `#333` : `#ddd`
    );
    // document.documentElement.style.setProperty(
    //     `--font`, 
    //     bgColor.isLight() ? `#333` : `#eee`
    // );
    document.documentElement.style.setProperty(
        `--black`, 
        bgColor.isLight() ? `#000` : `#fff`
    );
    document.documentElement.style.setProperty(
        `--white`, 
        bgColor.isLight() ? `#fff` : `#000`
    );

    // setup varations for the primary colors
    const varNames = [
        `primary`,
        `secondary`,
        `alternate`,
        `success`,
        `warning`,
        `danger`,
        `light`,
        `dark`,
        `black`,
        `white`,
        `bg-color`,
        `font`
    ]
    varNames.forEach(name => {
        const color = getComputedStyle(document.documentElement).getPropertyValue(`--${name}`);
        if(typeof color === 'undefined'){ return };
        if(color.trim() === ''){ return };
        createVarations(name, color.trim());
    })
}

const createVarations = (name, color) => {
    const baseColor = Color(color);
    const varations = [
        {
            suffix: `contrast`,
            color: baseColor.isDark() ? `#eee` : `#333`
        },
        {
            suffix: `tint`,
            color: baseColor.lighten(0.3)
        },
        {
            suffix: `shade`,
            color: baseColor.darken(0.3)
        },
        {
            suffix: `light`,
            color: baseColor.isDark() ? `#111` : `#eee`
        },
        {
            suffix: `dark`,
            color: baseColor.isDark() ? `#eee` : `#111`
        },
    ]

    varations.forEach(v => {
        document.documentElement.style.setProperty(`--${name}-${v.suffix}`, v.color);
    })
}