

export const init = () => {
    const banner = document.querySelector(`.banner`);
    if (!banner || banner.classList.contains(`no-border`)) { return };
    window.addEventListener('scroll', () => parallelScrolling());
    parallelScrolling();
}

const parallelScrolling = () => {
    const scrollY = window.scrollY;
    const pageHeight = window.innerHeight / 2;
    const banner = document.querySelector(`.banner`);
    const bannerHeight = banner.clientHeight;
    const ratio = scrollY / bannerHeight;
    const paths = document.querySelectorAll(`.banner__border .nectar-shape-divider-wrap path`);
    paths.forEach(p => {
        const translateY = parseInt(p.dataset.translateY);
        if (!translateY) { return };
        p.style.transform = `translateY(${ratio < 1 ? translateY * ratio : translateY}px)`;
    })

    if (!banner.classList.contains('custom-banner') && !banner.classList.contains(`no-border`)) {
        const inners = banner.querySelector(`.inner`);
        inners.style.top = `${scrollY*0.3}px`   
    }
}