export const init = () => {
    window.addEventListener('click', (e) => {
        const classes = e.target.classList;
        if(classes.contains('close')){
            const parent = e.target.closest('.ws-alert');
            if(parent){
                parent.remove();
            }
        }
    })
}