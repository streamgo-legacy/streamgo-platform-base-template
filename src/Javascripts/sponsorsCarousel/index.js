/**
 * Agenda List Functions
 * @module AgendaCarousel
 */

 import Glide, {
	Controls,
	Breakpoints,
	Swipe,
	Anchors,
} from "@glidejs/glide/dist/glide.modular.esm";
import "@glidejs/glide/dist/css/glide.core.min.css";
import "@glidejs/glide/dist/css/glide.theme.min.css";
import dayjs from "dayjs";

/**
 * Query all elements classed '.agenda-carousel' and transforms each into a Glide JS slider.
 * @memberof AgendaCarousel
 * @function
 * @returns {null} null
 */
export const init = () => {
	const cars = document.querySelectorAll(".sponsors-carousel");
	if (cars.length <= 0) {
		return;
	}

	var glide = new Glide(".sponsors-carousel-glide", {
		type: 'slider',
		startAt: 0,
		perView: 2,
		perTouch: 2,
		focusAt: 0,
		bound: true,
		breakpoints: {
			1280: {
				perView: 2,
				perTouch: 2,
			},
			800: {
				perView: 2,
				perTouch: 2,
			},
			600: {
				perView: 1,
				perTouch: 1,
			},
		},
	});

	// handler for further control on carousel
	const handler = (g, o, e) => {
		return {
			mount() {
				console.log("Example component has been mounted.");
			},
		};
	};

	glide.mount({
		Controls,
		Breakpoints,
		Swipe,
		Anchors,
		// 'handler': handler
	});
};
