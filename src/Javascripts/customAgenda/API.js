// API

const callApi = async (method, data) => {
	const csrf = document.head.querySelector('meta[name="csrf-token"]').content;
	const projectUrl = document.head.querySelector(
		'meta[name="project-url"]'
	).content;
	const apiEndpoint = "/api/agenda";

	let url = `${projectUrl}${apiEndpoint}`;
	const headers = {
		"Content-Type": "application/json",
		"X-CSRF-TOKEN": csrf,
	};

	let payload = new FormData();
	if (typeof data != "undefined") {
		payload = JSON.stringify(data);
	}

	const options = {
		method: method,
		headers: headers,
	};

	if (method === "POST") {
		options.body = payload;
	}

	if (method === "DELETE") {
		let query = `/${data.id}?type=${data.type}`;
		url = `${url}${query}`;
	}

	// console.log(`url`, url);
	// console.log('options', options);

	try {
		const req = await fetch(url, options);
		const res = await req.json();
		// console.log(res);
		return res.agenda;
	} catch (error) {
		console.log(error);
		return;
	}
};

export default callApi;
