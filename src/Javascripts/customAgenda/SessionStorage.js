import callApi from "./API";

// Local Storage
const getSessionStorage = async () => {
    const projectId = document.querySelector(`meta[name='project-id']`).content;
    const existingItems = await callApi('GET');
    if (existingItems === undefined) {
        sessionStorage.setItem(`sg-ca-${projectId}`, []);
    } else {
        sessionStorage.setItem(`sg-ca-${projectId}`, JSON.stringify(existingItems));
    }
    var caLocal = JSON.parse(sessionStorage.getItem(`sg-ca-${projectId}`));
    return caLocal;
};

const addSessionStorageItem = async (localItem) => {
    const projectId = document.querySelector(`meta[name='project-id']`).content;
    var caLocal = await getSessionStorage();
    caLocal.push(localItem);
    sessionStorage.setItem(`sg-ca-${projectId}`, JSON.stringify(caLocal));
    await callApi('POST', { id: localItem.id, type: localItem.type });
    return
};

const removeSessionStorageItem = async (uid) => {
    const projectId = document.querySelector(`meta[name='project-id']`).content;
    var caLocal = await getSessionStorage();
    const targetItem = caLocal.filter(item => String(item.id) === uid)[0];
    const filtered = caLocal.filter(item => String(item.id) !== uid);
    sessionStorage.setItem(`sg-ca-${projectId}`, JSON.stringify(filtered));
    await callApi('DELETE', { id: targetItem.id, type: targetItem.type });
    return
};

export {
    getSessionStorage,
    addSessionStorageItem,
    removeSessionStorageItem
};