/**
 * Provides UI and Functions for users to add events to their personal agenda list
 * @module CustomAgenda
 */

import * as ShowHideSwitch from '../showHideSwitch';
import axios from 'axios';
import dayjs from 'dayjs';
import { getSessionStorage, addSessionStorageItem, removeSessionStorageItem } from './SessionStorage';

/*
data-ca-source: agenda source item that can be added/removed to custom agenda
data-ca-container: the DOM element to be serves as the wrapper of the custom agenda

Methods:
- sync local with server
- push or delete items on server
*/

const defaultSettings = {
    enable: true,
    accentColor: `#4466FF`,
    containerClasses: [],
    itemClasses: [],
    defaultMessage: `You do not have any events added`,
    excludedPages: [],
    excludedItemClasses: [],
    enableCalendar: true,
    enablePush: true,
    addButtonHtml: `<span class="material-symbols-outlined">add</span> Add to Agenda`,
    removeButtonHtml: `<span class="material-symbols-outlined">close</span> Remove from Agenda`,
    pushSettings: {
        title: 'StreamGo',
        pushAdvance: 15,
        icon: "https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/agenda-web-push/GO+square+green.png",
    },
    addedCallback: function () {
        console.log('Event Added to Custom Agenda!');
    },
    removedCallback: function () {
        console.log('Event Removed from Custom Agenda!');
    }
}

export const init = async (customSettings) => {
    let settings = {
        ...defaultSettings
    };;

    if (customSettings) {
        for (const [k, v] of Object.entries(customSettings)) {
            if (typeof settings[k] === typeof v) {
                settings[k] = v;
            }
        }
    }

    const container = createContainer(settings.defaultMessage, settings.containerClasses);
    if (!container) {
        return
    };

    // get local and create ca-clones
    // const locals = getLocal();
    const locals = await getSessionStorage();
    locals.forEach(l => addItem(l, settings, true));

    const sourceItemsQuery = document.querySelectorAll(`[data-ca-source]`);
    const sourceItems = Array.from(sourceItemsQuery);
    sourceItems.forEach((item) => {
        const tags = item.dataset.tags ? JSON.parse(item.dataset.tags) : [];
        !tags.includes('no-ca') && initSourceItem(item, settings);
    });
    const track = container.querySelector('.track');
    if (track !== null) {
        ShowHideSwitch.doSwitch(track);
    }
}

function createContainer(defaultMsg, classes) {
    const container = document.querySelector(`[data-ca-container]`);
    if (!container) {
        return null;
    };
    setClasses(container, ['ca-container', 'empty']);
    classes.forEach(className => {
        container.classList.add(className);
    });

    const itemTrack = document.createElement('div');
    setClasses(itemTrack, ['track']);

    itemTrack.dataset.switches = '';

    const emptyMsgEl = document.createElement('h3');
    setClasses(emptyMsgEl, ['empty-message', 'box']);
    emptyMsgEl.innerHTML = defaultMsg;

    container.appendChild(emptyMsgEl);
    container.appendChild(itemTrack);

    const mutationObsConfig = {
        attributes: false,
        childList: true,
        subtree: true
    }

    const mutationHandler = (mutationList, observer) => {
        for (const mutation of mutationList) {
            if (mutation.type === 'childList' && mutation.target.classList.contains('track')) {
                const itemsQuery = mutation.target.querySelectorAll('[data-ca-clone]');
                const items = Array.from(itemsQuery);
                if (items.length <= 0) {
                    container.classList.add('empty');
                } else {
                    container.classList.remove('empty');
                }
            }
        }
    }

    const caObserver = new MutationObserver(mutationHandler);
    caObserver.observe(container, mutationObsConfig);

    return container;
}

function getLocal() {
    const storageName = `streamgo-ca-local-${document.querySelector('meta[name="project-id"]')?.content}`;
    if (!localStorage.getItem(storageName)) {
        localStorage.setItem(storageName, JSON.stringify([]));
    }
    var caLocal = JSON.parse(localStorage.getItem(storageName));
    return caLocal;
}

function setLocal(value) {
    const storageName = `streamgo-ca-local-${document.querySelector('meta[name="project-id"]')?.content}`;
    localStorage.setItem(storageName, JSON.stringify(value));
}

function setClasses(item, classes) {
    classes.forEach(x => {
        item.classList.add(x);
    })
}

function removeClasses(item, classes) {
    classes.forEach(x => {
        item.classList.remove(x);
    })
}

async function initSourceItem(item, settings) {
    setClasses(item, [
        'ca-item',
        'ca-item-source'
    ])

    item.dataset.caId = item.id;

    // check if item is OD
    const isOd = item.hasAttribute('data-od');

    // find the data-ca-buttons element
    let buttons = item.querySelector(`[data-ca-buttons]`);
    if (!buttons) {
        buttons = document.createElement('div');
        item.appendChild(buttons);
    }

    buttons.classList.add('ca-buttons');
    buttons.classList.add('buttons');
    buttons.classList.add('are-small');

    // create add and remove buttons
    const addBtn = document.createElement('button');
    const removeBtn = document.createElement('button');

    // handle add button
    setClasses(addBtn, [
        'ca-button',
        'ca-button-add',
        'button',
        'primary',
        'widget-button'
    ]);

    addBtn.insertAdjacentHTML('afterbegin', settings.addButtonHtml);
    addBtn.dataset.trigger = 'customAgenda';
    addBtn.dataset.openOnly = '1';
    buttons.appendChild(addBtn);

    // handle remove button
    setClasses(removeBtn, [
        'ca-button',
        'ca-button-remove',
        'button',
        'is-danger'
    ]);
    removeBtn.insertAdjacentHTML('afterbegin', settings.removeButtonHtml);
    buttons.appendChild(removeBtn);

    const caLocal = await getSessionStorage();
    const targetItemData = caLocal.find(x => String(x.id) === String(item.id));
    if (targetItemData) {
        item.classList.toggle('added');
    }

    // create listeners for the buttons
    addBtn.addEventListener('click', () => {
        const sourceItems = document.querySelectorAll(`[data-ca-source][data-ca-id="${item.id}"]`);
        sourceItems.forEach(item => {
            item.classList.add(`added`);
        })
        const itemData = {
            id: String(item.id),
            start_time: item.dataset.startTime,
            end_time: item.dataset.endTime,
            name: item.dataset.title,
            url: item.dataset.url,
            isOd
        }
        addItem(itemData, settings);
    })
    removeBtn.addEventListener('click', () => {
        const sourceItems = document.querySelectorAll(`[data-ca-source][data-ca-id="${item.id}"]`);
        sourceItems.forEach(item => {
            item.classList.remove('added');
        })
        removeItem(item.id, settings);
    })
}

async function addItem(itemData, settings, isInit = false) {
    const itemHtml = `
    <li 
        class="agenda-item ca-item-clone box added"
        id="ca-item-clone-${itemData.id}"
        data-start-time="${itemData.start_time}"
        data-end-time="${itemData.end_time}"
        data-title="${itemData.name}"
        data-url="${itemData.url}"
        data-ca-id="${itemData.id}"
        data-switch="${itemData.start_time}"
        data-ca-clone
    >
        <div class="agenda-item-times">
            ${
                itemData.isOd || !itemData.end_time ? 
                `
                <span class="agenda-item-time agenda-item-time__after"
                data-show="${itemData.start_time}"
                style="display: none;">Available On Demand</span>
                ` 
                : 
                `
                <span class="agenda-item-time agenda-item-time__before"
                data-hide="${itemData.start_time}">${dayjs(parseInt(itemData.start_time)).format(`DD MMM YYYY, HH:mm`)}</span>
            <span class="agenda-item-time agenda-item-time__now"
                data-show="${itemData.start_time}"
                data-hide="${itemData.end_time}" 
                style="display: none;"
            >Live NOW</span>
            <span class="agenda-item-time agenda-item-time__after"
                data-show="${itemData.end_time}"
                style="display: none;">Available On Demand</span>
                `
            }
        </div>
        <a class="agenda-item-title" href="${itemData.url}">
            <h4 class="">${itemData.name}</h4>
        </a>
        <div data-ca-buttons></div>
    </li>
    `;

    const container = document.querySelector(`[data-ca-container]`);
    const itemTrack = container.querySelector('.track');
    itemTrack.insertAdjacentHTML('beforeend', itemHtml);
    // get the item back from document
    const caItem = document.querySelector(`[data-ca-clone][data-ca-id="${itemData.id}"]`);

    const close = document.createElement('button');
    setClasses(close, [`delete`, `close-btn`]);

    close.addEventListener('click', () => {
        const sourceItems = document.querySelectorAll(`[data-ca-source][data-ca-id="${itemData.id}"]`);
        sourceItems.forEach(item => {
            item.classList.remove('added');
        })
        removeItem(itemData.id, settings);
    })

    caItem.appendChild(close);

    // adding it to local if its not init
    if (!isInit) {
        // let local = getLocal();
        // local.push(itemData);
        // setLocal(local);
        await addSessionStorageItem(itemData);
        trackItem(itemData, 'add');
    }

    addCalButtons(caItem);

    // do callback
    if (settings.addedCallback && !isInit) {
        settings.addedCallback();
    }
    if (!isInit && settings.enablePush) {
        doWebPush(caItem, settings.pushSettings);
    }
    ShowHideSwitch.doSwitch(itemTrack);
}

async function removeItem(itemId, settings) {
    const clonedItem = document.querySelector(`[data-ca-clone][data-ca-id="${itemId}"]`);
    const sourceItems = document.querySelectorAll(`[data-ca-source][data-ca-id="${itemId}"]`);

    clonedItem.remove();

    // let local = getLocal();
    // setLocal(local.filter(x => x.id !== itemId));
    await removeSessionStorageItem(itemId);

    const container = document.querySelector(`[data-ca-container]`);
    const itemTrack = container.querySelector('.track');
    trackItem({
        id: itemId
    }, 'remove');
    ShowHideSwitch.doSwitch(itemTrack);
}

function addCalButtons(cloneItem) {
    if (typeof cloneItem.dataset.startTime === 'undefined' || dayjs().isAfter(dayjs(parseInt(cloneItem.dataset.startTime)))) {
        return
    };

    const dataSet = cloneItem.dataset;
    const data = {
        startTime: dayjs(dataSet.startTime).format('YYYY-MM-DD HH:mm:ss'),
        endTime: dayjs(dataSet.endTime).format('YYYY-MM-DD HH:mm:ss'),
        title: dataSet.title,
        url: dataSet.url
    }

    const aeCalHtml = `
    <div class='ca-ae'>
    <span class='ca-ae__desc'>Add To Calendar:</span>
    <a class='ca-ae__button' href='https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start=${data.startTime}&end=${data.endTime}&alarm=10&location=${data.url}&title=${data.title}&timezone=Europe%2FLondon&service=google' target='_blank' data-service='Google Calendar' ><i class="bi bi-google"></i></a>
    <a class='ca-ae__button' href='https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start=${data.startTime}&end=${data.endTime}&alarm=10&location=${data.url}&title=${data.title}&timezone=Europe%2FLondon&service=outlook' target='_blank' data-service='Outlook'><i class="bi bi-windows"></i></a>
    <a class='ca-ae__button' href='https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start=${data.startTime}&end=${data.endTime}&alarm=10&location=${data.url}&title=${data.title}&timezone=Europe%2FLondon&service=apple' target='_blank' data-service='iCal'><i class="bi bi-apple"></i></a>
    <a class='ca-ae__button' href='https://events.streamgo.live/calendar?client=apmlqQgPdzBqDoQwTmFW14244&start=${data.startTime}&end=${data.endTime}&alarm=10&location=${data.url}&title=${data.title}&timezone=Europe%2FLondon&service=yahoo' target='_blank' data-service='Yahoo! Calendar'><i class="lab la-yahoo"></i></a>
    </div>
    `;

    cloneItem.insertAdjacentHTML('beforeend', aeCalHtml);
}


function trackItem(item, action) {
    // if it is localhost, return
    if (window.location.hostname.includes('localhost')) {
        return
    };

    // API
    const csrf = document.head.querySelector('meta[name="csrf-token"]')?.content;
    const projectUrl = document.head.querySelector('meta[name="project-url"]')?.content;
    const apiEndpoint = `/api/agenda`;
    const trackingEndpoint = `/sessions/click`;
    const apiHeaders = {
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': csrf
    }

    const data = {
        project: projectUrl,
        module: 'custom-agenda',
        action: action,
        agenda: item.id
    };
    const url = `${projectUrl}${trackingEndpoint}`;
    axios({
        method: 'post',
        url,
        data,
        apiHeaders
    });
}

function doWebPush(item, pushSettings) {

    const {
        title,
        startTime,
        endTime
    } = item.dataset;
    const advance = pushSettings.pushAdvance * 60000;
    const pushTime = Date.now() > (Number(startTime) - advance) ? Date.now() + 20000 : (Number(startTime) - advance);

    const trackUrl = `${window.location.protocol}//${window.location.hostname}`;

    const payload = {
        uid: item.id,
        info: {
            title: pushSettings.title,
            eventTitle: title,
            icon: typeof pushSettings.icon !== 'undefined' ? pushSettings.icon : null,
            startTime: pushTime
        },
        trackUrl: trackUrl,
        permissionLBStyle: typeof pushSettings.permissionLBStyle !== 'undefined' ? pushSettings.permissionLBStyle : null
    }

    // if it is localhost, return
    if (window.location.hostname.includes('localhost')) {
        return
    };

    let AgendaWebPush = window.AgendaWebPush.default;
    let subscribeAgenda = new AgendaWebPush();
    subscribeAgenda.agenda_subscribe(payload);
}