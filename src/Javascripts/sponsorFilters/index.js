export const init = () => {
    const container = document.querySelector(`.content_sponsor_container`);
    if (!container) {
        return
    };
	const items = container.querySelectorAll(`.sponsor_item`);
	let allTags = [];

	items.forEach((x) => {
		const tags = JSON.parse(x.dataset.tags);
		tags.forEach((t) => {
			const idx = allTags.findIndex((a) => a === t);
			if (idx < 0) {
				allTags.push(t);
			}
		});
	});

	if (allTags.length <= 0) {
		return;
	}

	const filterContainer = document.createElement("div");
	filterContainer.id = "sponsor-filter-container";
	filterContainer.classList.add("sponsor-filter-container");

	const tagsHtml = `
    <div class="tags">
        <h5 class='sponsor-filter-title medium'>Filter By: </h5>
        ${allTags
					.map((x) => {
						return `
            <span class="tag is-normal is-rounded" data-tag="${x}">
                ${x}
                <span class="material-symbols-outlined checked-icon">done</span>
            </span>
        `;
					})
					.join(``)}
    </div>
    `;

	container.insertAdjacentElement("afterbegin", filterContainer);
	filterContainer.insertAdjacentHTML("afterbegin", tagsHtml);

	const tagsButtons = document.querySelectorAll(
		`.sponsor-filter-container .tag`
	);
	tagsButtons.forEach((b) => {
		b.addEventListener("click", (e) => {
			const target = e.target;
			target.classList.toggle("active");
			reCheckFilter();
		});
	});
};

function reCheckFilter() {
	const container = document.querySelector(`.content_sponsor_container`);
    const sponsorGroups = container.querySelectorAll(`.sponsors`);
	const activeTags = Array.from(
		document.querySelectorAll(`.sponsor-filter-container .tag.active`)
	).map((x) => x.dataset.tag);
	console.log(sponsorGroups, activeTags);
	if (activeTags.length) {
        sponsorGroups.forEach((x) => {
			const items = x.querySelectorAll(`.sponsor_item`);
			items.forEach((p) => {
				const itemTags = JSON.parse(p.dataset.tags);
				const isActive = [...itemTags].filter((t) =>
					activeTags.includes(t)
				).length;
                if (isActive) {
                    p.classList.remove('hide');
                } else {
                    p.classList.add('hide');
                }
            });
            const hiddenItems = x.querySelectorAll(`.sponsor_item.hide`);
            console.log(hiddenItems.length, items.length);
            if (items.length === hiddenItems.length) {
                x.classList.add('hide');
            } else {
                x.classList.remove('hide');
            }
		});
	} else {
		sponsorGroups.forEach((x) => {
            const items = x.querySelectorAll(`.sponsor_item`);
            x.classList.remove('hide');
			items.forEach((p) => {
				p.classList.remove('hide');
			});
		});
	}
}
