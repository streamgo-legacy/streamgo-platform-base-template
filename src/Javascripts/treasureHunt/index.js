export const th_init = (settings) => {
    // var th_css = document.createElement('link');
    // th_css.id = 'th_css';
    // th_css.rel = 'stylesheet';
    // th_css.href = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/th-style.css';
    // document.head.appendChild(th_css);

    const defaultLootImg = `https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/chest.png`;
    const defaultBlankImg = `https://sg-project-assets.s3.eu-west-2.amazonaws.com/100119/th-badges/blankBadge.svg`;

    var defaultSettings = {
        enable: true,
        blankLootSrc: defaultBlankImg,
        hideAllLoots: false,
        passportFloat: false,
        progressPassportId: '',
        progressPassportClass: '',
        completeModalClass: '',
        passportTitle: 'Treasures Collected',
        completeModalHtml: `<p>You've found all treasures!!</p>`,
        huntId: "sg-treasurehunt",
    }; 
    if(settings){
        for(const key in settings){
            if(typeof settings[key] != 'undefined' && settings[key] !== ''){
                defaultSettings[key] = settings[key]
            }
        }
    }
    
    // get loots ready
    var loots = [];
    settings.loots.forEach(loot => {
        const imgSrc = loot.src !== '' ? loot.src : defaultLootImg;
        const position = loot.position !== '' ? loot.position : 'top-right';
        var temp = {
            id: loot.id,
            path: loot.path,
            point: 1,
            description: loot.desc,
            imgSrc: imgSrc,
            videoGo: loot.videoGo,
            triggerId: loot.triggerId || null,
            item: {
                classes: ["th-item", position],
                html: `<img data-th-od="${loot.videoGo}"${loot.triggerId ? ` data-trigger="${loot.triggerId}"` : ``} src="${imgSrc}" data-th-id="${loot.id}" />`
            },
            modal: {
                classes: ["th-modal"],
                html: () => modalProgressHTML(true)
            }
        }
        loots.push(temp);
    });

    var blankLootSrc = defaultSettings.blankLootSrc;
    var progressPassportId = defaultSettings.progressPassportId;

    console.log('from th_init, settings', settings, loots);

    addScript('treasure-hunt-func', 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-new-platform.js')
    .then(() => {
        var trackingUrl = '/'+ window.location.pathname.split("/")[1];
        treasureHunt({
            huntId: defaultSettings.huntId,
            completedHash: "completed",
            removeHuntOnComplete: false,
            progress: {
              classes: ["th-modal"],
              html: ({ loots }) => loots.map(
                  ({ description, hunted }) => ``
                )
                .join("")+modalProgressHTML(true)
            },
            loots: loots,
            onComplete: () => treasureHuntOncomplete(defaultSettings.completeModalHtml),
            trackingUrl: trackingUrl
        });

        if(progressPassportId != ''){
            initThPassportTab();

            $('.th-item').on('click', function(){
                initThPassportTab();
            });
        };

        $(document).ready(function(){
            $('.th-item').each(function(){
                var item = $(this);
                var videoGoDuration = parseInt($(this).find('[data-th-id]').attr('data-th-od'));
                var trigger = $(this).find('[data-th-id]').attr('data-trigger');

                if(videoGoDuration != 0){
                    item.hide();
                    var hiddenItem = $(`<div class="th-item-unknown">
                    <img src="${blankLootSrc}" />
                    </div>`).appendTo($('body'));
                    videoGo.treasureHunt(videoGoDuration, showTreasure);
                };

                if (trigger) {
                    item.hide();
                    var hiddenItem = $(`<div class="th-item-unknown">
                    <img src="${blankLootSrc}" />
                    </div>`).appendTo($('body'));
                    const triggerElement = document.getElementById(trigger);
                    triggerElement && triggerElement.addEventListener('click', () => showTreasure());
                }

                if(defaultSettings.hideAllLoots){
                    item.hide();
                }
            });
        });

        function showTreasure(){
            $('.th-item-unknown').hide();
            var thItem = document.getElementsByClassName("th-item");
            var i;
            for (i = 0; i < thItem.length; i++) {
                thItem[i].style.display = 'block';
            }
        };
    });

    function addScript(scriptName, src){
        var jsFile = document.createElement('script');
        jsFile.id = scriptName;
        jsFile.src = src;
        document.head.appendChild(jsFile);
    
        return new Promise((res) =>{
            jsFile.onload = function() {
                res();
            }
        })
    }

    function modalProgressHTML(passportFloat) {
        var lootsProgress = JSON.parse(localStorage.getItem(`${defaultSettings.huntId}`));
        var huntedIds = lootsProgress.huntedIds;
        var badgeArray = [];
        for (var i = 0; i < loots.length; i++){
          var badgeSrc = huntedIds.includes(loots[i].id) ? loots[i].imgSrc : blankLootSrc;
          badgeArray.push(`<img src="${badgeSrc}" width="48px" />`);
        }
        var closeBtn = passportFloat ? `<div id="th-close-btn" class='th-close-btn material-icons-outlined'>close</div>` : ``;
        var badgeArray = badgeArray.join("");
        var temp = 
        `<div class="th-hunt-progress-content">
          ${closeBtn}
          <b>${defaultSettings.passportTitle}</b>
          <div class="badges-array">
            ${badgeArray}
          </div>
        </div>`;
        return temp;
    }

    function treasureHuntOncomplete(html){
        var modal = document.createElement("div");
        var overlay = document.createElement("div");
        var pp = document.createElement("div");
        modal.id = "th-complete-modal";
        modal.classList.add("th-modal");
        overlay.classList.add("th-modal-overlay");
        pp.innerHTML = html;
        pp.classList.add('th-modal-content');
        document.body.appendChild(modal);

        $("#th-complete-modal").append(overlay);
        $("#th-complete-modal").append(pp);
        $("#th-complete-modal img.complete-img").on('load', function(){
            $(this).css({
                'opacity':'1',
                'transform': 'scale(1)',
            });
        });
        
        overlay.onclick = function () {
            var modal = document.getElementById("th-complete-modal");
            modal.remove();
            $(this).remove();
        };
    };

    function initThPassportTab(){
        var lootsProgress = JSON.parse(localStorage.getItem(defaultSettings.huntId));
        var huntedIds = lootsProgress ? lootsProgress.huntedIds : [];
        var badgeArray = [];
        for(var i = 0; i < loots.length; i++){
          var blankBadge = blankLootSrc;
          var badgeSrc = huntedIds.includes(loots[i].id) ? loots[i].imgSrc : blankBadge;
          badgeArray.push(`<img src="${badgeSrc}" width="48px" />`);
        }
        var badgeArray = badgeArray.join("");
        var temp = 
        `<div class="th-hunt-progress-content passport">
          <div class="badges-array">
            ${badgeArray}
          </div>
        </div>`;
      $(`#${progressPassportId}`).html(temp);
    };
};