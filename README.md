# Base Template 2.0 Components
Component Codes are located in ./src/Components

Global Configs are located in ./src/Config

Global Settings (based on PROD platform settings) are located in ./src/Global Settings

# Template Assets
Codebase for globally centralised front-end template assets, such as JS or CSS/SCSS

## Get Started
Install all packages

`npm install`

Start Local Server (served in http://localhost:8080/)

`npm start`

Build locally (files will be output to ./dist folder)

`npm run build`

## Usage
To use the global script and css on your Template or Project, include below tags on the `<head>` of your HTML:

Staging:
```
<link href="http://cdn.staging.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/sgGlobal.css" rel="stylesheet"/>
<script src="http://cdn.staging.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/sgGlobal.js"></script>
```
Production:
```
<link href="http://cdn.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/sgGlobal.css" rel="stylesheet"/>
<script src="http://cdn.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/sgGlobal.js"></script>
```

### Call Functions
To call functions exposed from the plugin, use the global js variable `sgGlobal`.

For example, to call the `testPing` function:
```
<script>
    window.addEventListener('load', () => {
        sgGlobal.testPing()
    })
    // expected log: 'Ping! Sent from sgGlobal.testPing()'
</script>
```

## Demo & Docs
### Staging
Demo: http://cdn.staging.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/

Docs: http://cdn.staging.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/docs/
### Production
Demo: http://cdn.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/

Docs: http://cdn.sg-template-assets-global.s3-website.eu-west-2.amazonaws.com/docs/

## Deployment
Deployment is automated upon merging occurred in 'develop' and 'main' branches, respectively to staging and production S3 buckets

Staging: https://s3.console.aws.amazon.com/s3/buckets/cdn.staging.sg-template-assets-global?region=eu-west-2&tab=objects

Production: https://s3.console.aws.amazon.com/s3/buckets/cdn.sg-template-assets-global?region=eu-west-2&tab=objects

## Automatic Documentation
This repository is installed with 'Documentation' plugin which provides a fast way to build references for classes and functions.

#### Usage & Syntax
Add this special comment before the class/function (Make sure the open comment tag is started with 2* [/**]).

Example:
```
/**
* Description of your function/class
* @param {number} userId the id of the user 
* @returns {string} email the email of the user
*/

const getUserEmail = (userId) => {
    //do sth
    const email = user.email;
    return email;
}
```
For more details please see https://github.com/documentationjs/documentation#readme

#### Build updated Docs locally
The latest doc will be built upon the auto deployment to staging or production, but you can run `npm run docs` to build the updated docs locally.<br/>
Docs will be built and located in `./public/docs`
